<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Administrator',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

    	DB::table('client')->insert([
    		'type' => 'corporate',
    		'role_id' => 1,
            'name' => 'Layana Web Admin',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'name' => 'Admin Kartu Virtual',
            'email' => 'layanacomputindo@gmail.com',
            'password' => bcrypt('xcafexcafe'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'client_id' => 1
        ]);

        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);
    }
}
