<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepositeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        	Schema::create('deposit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('price');
            $table->integer('bank_id');
            $table->string('account_number');
            $table->string('account_name');
            $table->string('type');
            $table->string('transfer_total')->nullable();
            $table->string('transfer_date')->nullable();
            $table->string('transfer_letter')->nullable();
            $table->string('transfer_photo')->nullable();
            $table->string('param')->nullable();
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post');
    }
}
