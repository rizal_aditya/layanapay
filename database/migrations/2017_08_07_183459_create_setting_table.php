<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');   
            $table->string('password');
            $table->string('host');
            $table->string('port');
            $table->string('encryption');
	    $table->string('logo');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('mindepn');
            $table->integer('maxdepn');
            $table->integer('mintrn');
            $table->integer('maxtrn');
            $table->integer('minwdn');
            $table->integer('maxwdn');
            $table->integer('mindepk');
            $table->integer('maxdepk');
            $table->integer('mintrk');
            $table->integer('maxtrk');    
            $table->integer('minwdk');
            $table->integer('maxwdk');
            $table->integer('admin');
	    $table->integer('transaksi');
            $table->integer('transfer');
            $table->integer('saldo');
            $table->string('bank');
            $table->integer('count');
            $table->string('account_name');
            $table->string('account_number');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting');
    }
}
