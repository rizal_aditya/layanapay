<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qrcode', 100);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('last_balance');
            $table->integer('packet_id')->unsigned();
            $table->integer('costumer_id')->unsigned();
            $table->integer('bank_id')->unsigned();
            $table->datetime('tanggal');
            $table->integer('bank_send')->unsigned();
            $table->integer('price');
            $table->integer('denom');
            $table->string('type', 100);
            $table->integer('notif');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction');
    }
}
