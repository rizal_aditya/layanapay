<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('users', function($table)
         {   
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('client')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('saldo');
            $table->string('pob');
            $table->date('dob');
            $table->string('gender');
            $table->string('national');
            $table->string('city');
            $table->string('postal_code');
            $table->string('education');
            $table->string('job');
            $table->string('qrcode');
            $table->string('pin');
            $table->string('photo');
            $table->string('phone');
            $table->string('type_id');
            $table->string('identity_number');
            $table->string('identity_photo');
            $table->string('img_qrcode');
            $table->string('img_sb');
            $table->string('img_location');
            $table->text('address');
            $table->string('type');
            $table->string('code_reveral');
            $table->string('img_reveral');
            $table->string('type');
            $table->integer('notif');
            $table->string('status');
            $table->string('fcm');
            $table->integer('kyc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
