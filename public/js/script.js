$(document).ready(function() {
   
   // JmlDeposit();
   // JmlWithdraw();
   // JmlUser(); 

  var status = "10";
  var search = $('#search').val();
  var pages = "";
  var costumer_id = "kosong";

  var url = window.location.toString();
  var type = url.split('=');
   var halaman = url.split('/');
 
  if(type[1] =="deposite")
  {
     //JmlDeposit();
     //ListUserDeposite();
     ListDepositeTable(status,search,pages);

  }else if(type[1] =="withdraw"){
    JmlWithdraw();
    //ListUserWithdraw();
    ListWithdrawTable(status,search,pages);
  }else if(type[1] =="costumer"){
     JmlUser(); 
     JmlDeposit();
     JmlWithdraw();
     gettopup();
     //ListUserKyc();
     ListUserTable(status,search,pages);
  }else if(type[1] =="co-branding"){
    JmlDeposit();
     JmlWithdraw();
     JmlUser(); 
     gettopup();
    ListCobrandTable(status,search,pages);
  }else if(type[1] =="merchant"){
    JmlDeposit();
   JmlWithdraw();
   JmlUser(); 
   gettopup();
    ListMerchantTable(status,search,pages);
  }
  else if(type[1] == 'issuer')
  {
    JmlDeposit();
   JmlWithdraw();
   JmlUser(); 
   gettopup();
    ListIssuerTable(status,search,pages);


 }else if(halaman[3] == "order"){

  ListorderTable(status,search,pages);


 }



  //ListUserKyc();

$('#StVerDep').on('change', function() {
  var status = this.value;
  ListDepositeTable(status,search,pages);

});


$('#issuer').on('change', function() {
  var status = this.value;
  //ListDepositeTable(status,search,pages);

});



$('#StVerDraw').on('change', function() {
  var status = this.value;
  ListWithdrawTable(status,search,pages);

});

$('#StVerCos').on('change', function() {
  var status = this.value;
  ListUserTable(status,search,pages);

});

$('#StVerCo').on('change', function() {
  var status = this.value;
  ListCobrandTable(status,search,pages);
});

$('#StVerMer').on('change', function() {
  var status = this.value;
  ListMerchantTable(status,search,pages);
});


$('#searchDep').keypress(function(e) {
    var search = $("#searchDep").val();
    if(search !=null)
    {
      ListDepositeTable(status,search,pages);
    }
}); 

$('#searchDraw').keypress(function(e) {
    var search = $("#searchDraw").val();
    if(search !=null)
    {
      ListWithdrawTable(status,search,pages);
    }
}); 

$('#searchOrder').keypress(function(e) {
    var search = $("#searchOrder").val();
    if(search !=null)
    {
      datatableorder(status,search,pages);
    }
}); 




$('#searchCos').keypress(function(e) {
    var search = $("#searchCos").val();
    if(search !=null)
    {
      ListUserTable(status,search,pages);
    }
}); 

$('#searchMer').keypress(function(e) {
    var search = $("#searchMer").val();
    if(search !=null)
    {
      ListMerchantTable(status,search,pages);
    }
}); 

$('#searchCob').keypress(function(e) {
    var search = $("#searchCob").val();
    if(search !=null)
    {
      ListCobrandTable(status,search,pages);
    }
}); 


$('#Bank').on('change', function() {
  $(".tes").html(this.value);
})



$('#Notifdeposit').click(function(){
    
    UpdateNotifDeposite();
    ListUserDeposite(); 
});

$('#Notifwithdraw').click(function(){
    UpdateNotifWithdraw(); 
    ListUserWithdraw(); 
});

$('#Notifuser').click(function(){
   UpdateNotifUser();
   ListUserKyc(); 
});


  
});



// this will run after every 3 seconds
// setInterval(function(){

//    //JmlDeposit();
//    //JmlWithdraw();
//     //JmlUser();
// }, 3000); 





function gettopup(){
  $.ajax({
      type:"GET",
      url: "dashboard/jmldana",
      dataType: "json",
      cache: false,
      success: function(data){        
       gettopupBaru(data);
      },
      error: function (data) {
        //alert("Gagal jumlah Deposite");
        
      }
    });


}


function gettopupBaru(jml){
  $.ajax({
      type:"GET",
      url: "dashboard/jmldana",
      dataType: "json",
      cache: false,
      success: function(data){  

       
          Topup ="";
          if(data == "0")
          {
             Topup += "<div class='topup'>Saldo : 0 </div>";
          }else{
             Topup += "<div class='topup'>Saldo : "+ toRp(data[0]["dana"]) +"</div>";
          }

         

          $(".topsaldo").html(Topup);

      },
      error: function (data) {
        //alert("Gagal jumlah Deposite");
        
      }
    });


}




  
function JmlDeposit(){
	$.ajax({
      type:"GET",
      url: "deposite/jmldeposite",
      dataType: "json",
      cache: false,
      success: function(data){   
          JmlDepositBaru(data);
      },
      error: function (data) {
        //alert("Gagal jumlah Deposite");
        
      }
    });


}


function JmlDepositBaru(jml){
  $.ajax({
      type:"GET",
      url: "deposite/jmldeposite",
      dataType: "json",
      cache: false,
      success: function(data){   

          if(jml != data)
          {

            var status = "10";
            var search = $('#search').val();
            var pages = "";  

            //ListUserDeposite();
            ListDepositeTable(status,search,pages);

          } 
              buatNotiv ="";
              if(data == "")
              {
              
              buatNotiv += "<i class='fa fa-arrow-down'></i><span class='label label-success'></span>";
              }else{
              
               buatNotiv += "<i class='fa fa-arrow-down'></i><span class='label label-success'>"+ data +"</span>";
              }
      
            $("#Notifdeposit").html(buatNotiv);
      },
      error: function (data) {
        //alert("Gagal jumlah Deposite");
        
      }
    });

}


function JmlWithdraw(){
  $.ajax({
      type:"GET",
      url: "deposite/jmlwithdraw",
      dataType: "json",
      cache: false,
      success: function(data){        
          JmlWithdrawBaru(data);
      },
      error: function (data) {
        //alert("Gagal jumlah withdraw");
        
      }
    });


}


function JmlWithdrawBaru(jml){
  $.ajax({
      type:"GET",
      url: "deposite/jmlwithdraw",
      dataType: "json",
      cache: false,
      success: function(data){        
       

          if(jml != data)
          {

            var status = "10";
            var search = $('#search').val();
            var pages = "";  

            //ListUserWithdraw();
            ListWithdrawTable(status,search,pages);

          } 

          buatNotiv ="";

          if(data == "0")
          {
            
             buatNotiv += "<i class='fa fa-arrow-up'></i><span class='label label-success'></span>";

          }else{
    
             buatNotiv += "<i class='fa fa-arrow-up'></i><span class='label label-success'>"+ data +"</span>";

          }
         
          $("#Notifwithdraw").html(buatNotiv);

      },
      error: function (data) {
        //alert("Gagal jumlah withdraw");
        
      }
    });


}



function JmlUser(){
  $.ajax({
      type:"GET",
      url: "deposite/jmluser",
      dataType: "json",
      cache: false,
      success: function(data){        
       JmlUserBaru(data);
      },
      error: function (data) {
        //alert("Gagal jumlah Deposite");
        
      }
    });


}


function JmlUserBaru(jml){
  $.ajax({
      type:"GET",
      url: "deposite/jmluser",
      dataType: "json",
      cache: false,
      success: function(data){  


          if(jml != data)
          {

            var status = "10";
            var search = $('#search').val();
            var pages = "";  

            //ListUserKyc();
            //ListWithdrawTable(status,search,pages);

          } 

       
          buatNotiv ="";
          if(data == "0")
          {
             buatNotiv += "<i class='fa fa-user'></i><span class='label label-success'></span>";
          }else{
             buatNotiv += "<i class='fa fa-user'></i><span class='label label-success'>"+ data +"</span>";
          }

         

          $("#Notifuser").html(buatNotiv);

      },
      error: function (data) {
        //alert("Gagal jumlah Deposite");
        
      }
    });


}

function ListUserDeposite(){
 $(".listuser").html('<li><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></li>');
	$.ajax({
      type:"GET",
      url: 'deposite/listuserdeposite',
      dataType: "json",
      cache: false,
      success: function(respons){        
   			

         jmlData = respons['data'].length;
         //buatTabel = "";
         buatList = "";

                        
                         
                        for(a = 0; a < jmlData; a++)
                        {
                           var url = window.location.toString();
                           var img = "/img/avatar.png";
                           var photo =  url.img;
                           var uimg = "50";
                           if(photo != null || photo !='')
                           {
                              photo = respons['data'][a]["photo"];
                           } 

                            if(respons['data'][a]["status"] =="0")
                            {
                                $status = "Proses";
                            }else{
                                $status = "Sukses";
                            }   


                        buatList  += "<li>"
                                        +"<a href='#'>"
                                        + "<div class='pull-left'><img width='50' height='50' src='"+ photo +"' class='img-circle' alt='User Image'></div>"
                                        + "<h4>"+ respons['data'][a]["name"] +" <small> <i class='fa fa-clock-o'></i> " + time_ago(respons['data'][a]["created_at"]) + "</small></h4>"
                                        + "<p>" + $status + " </p>"
                                        + "</a>"
                                      +"</li>";
                        }  
                      
                
        $(".listdeposit").html(buatList);
      },
      error: function (respons) {
        //alert("Gagal list user Deposite ");
        
      }
    });

}


function ListDepositeTable(status,search,page){
$("#DepTable").html('<tr><td colspan="11"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
	$.ajax({
      type:"GET",
      url: "deposite/listtabeldeposite",
      dataType: "json",
      data: {status:status,search:search,page:page},
      success: function(respons){        
   		   
        
         jmlData = respons['data'].length;

              if(jmlData =="0"){

                     buatTabel = "<tr><td colspan='11'>Data Kosong</td></tr>";
                     buatPage = "";
                     buatFirst = "";
                     buatLast = "";
               }else{

                  buatTabel = "";
                  buatPage = "";
                  buatFirst = "";
                  buatLast = "";
               }



        
         
          for(a = 0; a < jmlData; a++)
          {

              
                
               var url = window.location.toString();
               var img = "/img/avatar.png";
               var photo =  url.img;
               var uimg = "50";
               if(photo != null || photo !='')
               {
                  photo = respons['data'][a]["photo"];
               } 

               var st = respons['data'][a]["status"];
               if(st =="0")
               {
                var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Proses</div>";
                var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKirim(" + respons['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Terima</a>";
               }else{
                var status = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Sukses</div>";
                var konfirmasi = "<a id='Block' style='cursor: pointer;'' data-toggle='modal' onclick='GetBatal(" + respons['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-user-times'></i> Batal</a>";
               }

             

            //mencetak baris baru
            buatTabel += "<tr>"
            
                        //membuat penomoran
                        + "<td><img width='" + uimg + "' height='" + uimg + "' src='" + photo + "'></td>"
                        + "<td><div class='nametbl'> " + respons['data'][a]["name"] + "</div></td>"
                        + "<td>" + respons['data'][a]["packet"] + "</td>"
                        + "<td>" + toRp(respons['data'][a]["saldo"]) + "</td>"
                        + "<td> <div class='banktbl'>" + respons['data'][a]["bank_costumer"] + " | " + respons['data'][a]["account_costumer"] + "</div></td>"
                        + "<td> <div class='banktbl'>" + respons['data'][a]["bank_admin"] + " | " + respons['data'][a]["account_admin"] + "</div></td>"
                        + "<td>" + toRp(respons['data'][a]["price"]) + "</td>"
                        + "<td>" + datebln(respons['data'][a]["tanggal"])  + "</td>"
                        + "<td>" + time_ago(respons['data'][a]["created_at"]) + "</td>"
                        + "<td>" + status + "</td>"
                        + "<td><div class='btn-group'><a class='btn btn-default btn-xs'  style='cursor: pointer;' data-toggle='modal' onclick='GetDetail(" + respons['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-eye'></i> Detail</a><a class='btn btn-default btn-small ismal dropdown-toggle' data-toggle='dropdown' href='#''><span class='caret'></span></a><ul class='dropdown-menu drop'> <li>" + konfirmasi + "</li> <li> <a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetDestroy(" + respons['data'][a]["id"] + ");'' data-target='#myModal'><i class='fa fa-trash'></i> Hapus</a></li></ul> </div></td>"
            //tutup baris baru
                + "<tr/>";

          
               
        }

      $("#DepTable").html(buatTabel);

        
        

        if(respons['from'] != null){
           var last_page = respons['last_page']; 
           
              buatFirst += ""
                   + "<li><button  class='btn btn-default' onClick='GetpageDep(1);'>«</button></li>"
                    + ""; 

             for(i = 1; i < last_page+1; i++)
             {
                    
                  if(respons['from'] ==1)
                  {
                    if([i] ==1)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    } 

                  }else if(page == respons['current_page']){

                    if([i] == page)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    }  
                 
                  }else{
                    var aktif = "";
                  }

                  

                     buatPage += ""
                   + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageDep(" + [i] + ");'>" + [i] + "</button></li>"
                    + "";    
             } 

              buatLast += ""
             + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageDep(" + respons['last_page']+");''>»</button></li>"
             + ""; 
            
         }

              

         $(".firstDep").html(buatFirst);
         $(".navigationDep").html(buatPage);
         $(".lastDep").html(buatLast);

      
      },
      error: function (respons) {
        alert("Gagal list Table Deposite ");
        
      }
    });

 

}


function ListUserWithdraw(){
 $(".listwithdraw").html('<li><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></li>');
  $.ajax({
      type:"GET",
      url: 'deposite/listuserwithdraw',
      dataType: "json",
      cache: false,
      success: function(respons){        
        
        jmlData = respons['data'].length;
         //buatTabel = "";
         buatList = "";

         if(jmlData == "0")
          {
            var jml = "";
          }else{
            var jml = jmlData;
          }

         //mencetak baris baru
            //buatTabel  += "<i class='fa fa-arrow-down'></i><span class='label label-success'>"+ jml +"</span>";


                        // //membuat penomoran
                        
                         
                        for(a = 0; a < jmlData; a++)
                        {
                           var url = window.location.toString();
                           var img = "/img/avatar.png";
                           var photo =  url.img;
                           var uimg = "50";
                           if(photo != null || photo !='')
                           {
                              photo = respons['data'][a]["photo"];
                           } 

                            if(respons['data'][a]["status"] =="0")
                            {
                                $status = "Proses";
                            }else{
                                $status = "Sukses";
                            }   


                        buatList  += "<li>"
                                        +"<a href='#'>"
                                        + "<div class='pull-left'><img width='50' height='50' src='"+ photo +"' class='img-circle' alt='User Image'></div>"
                                        + "<h4>"+ respons['data'][a]["name"] +" <small> <i class='fa fa-clock-o'></i> " + time_ago(respons['data'][a]["created_at"]) + "</small></h4>"
                                        + "<p>" + $status + " </p>"
                                        + "</a>"
                                      +"</li>";
                        }  
                      
            //tutup baris baru
         



        //$("#Notifwithdraw").html(buatTabel);
        $(".listwithdraw").html(buatList);
      },
      error: function (respons) {
        //alert("Gagal list user Deposite ");
        
      }
    });

}




function ListWithdrawTable(status,search,page){
$("#DepTable").html('<tr><td colspan="11"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
  $.ajax({
      type:"GET",
      url: "deposite/listtabelwithdraw",
      dataType: "json",
      data: {status:status,search:search,page:page},
      success: function(respons){        
         
        
         jmlData = respons['data'].length;

              if(jmlData =="0"){

                     buatTabel = "<tr><td colspan='11'>Data Kosong</td></tr>";
                     buatPage = "";
                     buatFirst = "";
                     buatLast = "";
               }else{

                  buatTabel = "";
                  buatPage = "";
                  buatFirst = "";
                  buatLast = "";
               }



        
         
          for(a = 0; a < jmlData; a++)
          {

              
                
               var url = window.location.toString();
               var img = "/img/avatar.png";
               var photo =  url.img;
               var uimg = "50";
               if(photo != null || photo !='')
               {
                  photo = respons['data'][a]["photo"];
               } 

               var st = respons['data'][a]["status"];
               if(st =="0")
               {
                var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Proses</div>";
                var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKirim(" + respons['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Kirim</a>";
               }else{
                var status = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Sukses</div>";
                var konfirmasi = "<a id='Block' style='cursor: pointer;'' data-toggle='modal' onclick='GetBatal(" + respons['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-user-times'></i> Batal</a>";
               }

             

            //mencetak baris baru
            buatTabel += "<tr>"
            
                        //membuat penomoran
                        + "<td><img width='" + uimg + "' height='" + uimg + "' src='" + photo + "'></td>"
                        + "<td><div class='nametbl'> " + respons['data'][a]["name"] + "</div></td>"
                        + "<td>" + respons['data'][a]["packet"] + "</td>"
                        + "<td>" + toRp(respons['data'][a]["saldo"]) + "</td>"
                        + "<td> <div class='banktbl'>" + respons['data'][a]["bank_costumer"] + " | " + respons['data'][a]["account_costumer"] + "</div></td>"
            
                        + "<td>" + toRp(respons['data'][a]["price"]) + "</td>"
                        + "<td>" + datebln(respons['data'][a]["tanggal"])  + "</td>"
                        + "<td>" + time_ago(respons['data'][a]["created_at"]) + "</td>"
                        + "<td>" + status + "</td>"
                        + "<td>"
                        + "<div class='btn-group'>"
                        + "<a class='btn btn-default btn-xs'  style='cursor: pointer;' data-toggle='modal' onclick='GetDetail(" + respons['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-eye'></i> Detail</a>"
                        + "<a class='btn btn-default btn-small ismal dropdown-toggle' data-toggle='dropdown' href='#'>"
                        + "<span class='caret'></span></a>"
                          + "<ul class='dropdown-menu drop'>" 
                            + "<li>" + konfirmasi + "</li>"
                            + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetDestroy(" + respons['data'][a]["id"] + ");'' data-target='#myModal'><i class='fa fa-trash'></i> Hapus</a></li>"
                          + "</ul>" 
                        + "</div>"
                        + "</td>"
            //tutup baris baru
                + "<tr/>";

          
               
        }

      $("#DrawTable").html(buatTabel);

        
        

        if(respons['from'] != null){
           var last_page = respons['last_page']; 
           
              buatFirst += ""
                   + "<li><button  class='btn btn-default' onClick='GetpageDraw(1);'>«</button></li>"
                    + ""; 

             for(i = 1; i < last_page+1; i++)
             {
                    
                  if(respons['from'] ==1)
                  {
                    if([i] ==1)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    } 

                  }else if(page == respons['current_page']){

                    if([i] == page)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    }  
                 
                  }else{
                    var aktif = "";
                  }

                  

                     buatPage += ""
                   + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageDraw(" + [i] + ");'>" + [i] + "</button></li>"
                    + "";    
             } 

              buatLast += ""
             + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageDraw(" + respons['last_page']+");''>»</button></li>"
             + ""; 
            
         }

              

         $(".firstDraw").html(buatFirst);
         $(".navigationDraw").html(buatPage);
         $(".lastDraw").html(buatLast);

      
      },
      error: function (respons) {
        alert("Gagal list Table Deposite ");
        
      }
    });

 

}

function ListUserKyc(){
 
  $.ajax({
      type:"GET",
      url: 'deposite/listuserkyc',
      dataType: "json",
      cache: false,
      success: function(respons){        
        

         jmlData = respons['data'].length;
         //buatTabel = "";
         buatList = "";
  
                         
                        for(a = 0; a < jmlData; a++)
                        {
                           var url = window.location.toString();
                           var img = "/img/avatar.png";
                           var photo =  url.img;
                           var uimg = "50";
                           if(photo != null || photo !='')
                           {
                              photo = respons['data'][a]["photo"];
                           } 

                            if(respons['data'][a]["kyc"] =="0")
                            {
                                $status = "Proses";
                            }else{
                                $status = "Verified";
                            }   


                        buatList  += "<li>"
                                        +"<a href='#'>"
                                        + "<div class='pull-left'><img width='50' height='50' src='"+ photo +"' class='img-circle' alt='User Image'></div>"
                                        + "<h4>"+ respons['data'][a]["name"] +" <small> <i class='fa fa-clock-o'></i> " + time_ago(respons['data'][a]["created_at"]) + "</small></h4>"
                                        + "<p>" + $status + " </p>"
                                        + "</a>"
                                      +"</li>";
                        }  
                      
            //tutup baris baru
                



        //$("#Notifdeposit").html(buatTabel);
        $(".listuser").html(buatList);
      },
      error: function (respons) {
        alert("Gagal list user kyc ");
        
      }
    });

}


function ListUserTable(status,search,page){
$("#UserTable").html('<tr><td colspan="11"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
  $.ajax({
      type:"GET",
      url: "deposite/listtabeluser",
      dataType: "json",
      data: {status:status,search:search,page:page},
      success: function(data){        
         
        
         jmlData = data['data'].length;

              if(jmlData =="0"){

                     buatTabel = "<tr><td colspan='10'>Data Kosong</td></tr>";
                     buatPage = "";
                     buatFirst = "";
                     buatLast = "";
               }else{

                  buatTabel = "";
                  buatPage = "";
                  buatFirst = "";
                  buatLast = "";
               }



        
         
          for(a = 0; a < jmlData; a++)
          {

              
                
               var url = window.location.toString();
               var img = "/img/avatar.png";
               var photo =  url.img;
               var uimg = "50";
               if(photo != null || photo !='')
               {
                  photo = data['data'][a]["photo"];
               } 

               var st = data['data'][a]["status"];
               if(st =="0")
               {
                var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Proses</div>";
                var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Terima</a>";
               }else if(st =="2"){
                var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Blokir</div>";
                var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetBataluser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-recycle' aria-hidden='true'></i> Batalkan</a>";
               }else{
                var status = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Sukses</div>";
                var konfirmasi = "<a id='Block' style='cursor: pointer;'' data-toggle='modal' onclick='GetBlokirUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-user-times'></i> Blokir</a>";
               }

             

            //mencetak baris baru
            buatTabel += "<tr>"
            
                        //membuat penomoran
                        + "<td><img width='" + uimg + "' height='" + uimg + "' src='" + photo + "'></td>"
                        + "<td><div class='nametbl'> " + data['data'][a]["costumer"] + "</div></td>"
                        + "<td>" + data['data'][a]["email"] + "</td>"
                        + "<td>" + data['data'][a]["phone"] + "</td>"
                        + "<td>" + toRp(data['data'][a]["saldo"]) + "</td>"
                        //+ "<td>" + data['data'][a]["account_number"] + "</td>"
                        //+ "<td>" + data['data'][a]["bank"] + "</td>"
                        //+ "<td>" + data['data'][a]["account_name"] + "</td>"
                        + "<td>" + status + "</td>"
                        + "<td>"
                        + "<div class='btn-group'>"
                        + "<a class='btn btn-default btn-xs' onclick='GetDetailuser(" + data['data'][a]["id"] + ");'  style='cursor: pointer;' data-toggle='modal'  data-target='#myModal'><i class='fa fa-eye'></i> Detail</a>"
                        + "<a class='btn btn-default btn-small ismal dropdown-toggle' data-toggle='dropdown' href='#'>"
                        + "<span class='caret'></span></a>"
                          + "<ul class='dropdown-menu drop'>" 
                            + "<li>" + konfirmasi + "</li>"
                           
                          + "</ul>" 
                        + "</div>"
                        + "</td>"

            //tutup baris baru
                + "<tr/>";

          
               
        }

      $("#UserTable").html(buatTabel);

        
        

        if(data['from'] != null){
           var last_page = data['last_page']; 
           
              buatFirst += ""
                   + "<li><button  class='btn btn-default' onClick='GetpageUser(1);'>«</button></li>"
                    + ""; 

             for(i = 1; i < last_page+1; i++)
             {
                    
                  if(data['from'] ==1)
                  {
                    if([i] ==1)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    } 

                  }else if(page == data['current_page']){

                    if([i] == page)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    }  
                 
                  }else{
                    var aktif = "";
                  }

                  

                     buatPage += ""
                   + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageUser(" + [i] + ");'>" + [i] + "</button></li>"
                    + "";    
             } 

              buatLast += ""
             + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageUser(" + data['last_page']+");''>»</button></li>"
             + ""; 
            
         }

              

         $(".firstDep").html(buatFirst);
         $(".navigationDep").html(buatPage);
         $(".lastDep").html(buatLast);

      
      },
      error: function (data) {
        alert("Gagal list Table User");
        
      }
    });

 

}

/* Populate table Merchant */
function ListMerchantTable(status,search,page) {
  $("#MerchantTable").html('<tr><td colspan="11"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
  $.ajax({
    type:"GET",
    url: "deposite/listtabelmerchant",
    dataType: "json",
    data: {status:status,search:search,page:page},
    success: function(data){        
       
      
       jmlData = data['data'].length;

            if(jmlData =="0"){

                   buatTabel = "<tr><td colspan='10'>Data Kosong</td></tr>";
                   buatPage = "";
                   buatFirst = "";
                   buatLast = "";
             }else{

                buatTabel = "";
                buatPage = "";
                buatFirst = "";
                buatLast = "";
             }



      
       
        for(a = 0; a < jmlData; a++)
        {    
           var url = window.location.toString();
           var img = "/img/avatar.png";
           var photo =  url.img;
           var uimg = "50";
           if(photo != null || photo !='')
           {
              photo = data['data'][a]["photo"];
           } 

           var st = data['data'][a]["status"];
           if(st =="0")
           {
            var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Proses</div>";
            var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Terima</a>";
           }else if(st =="2"){
            var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Blokir</div>";
            var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetBataluser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-recycle' aria-hidden='true'></i> Batalkan</a>";
           }else{
            var status = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Sukses</div>";
            var konfirmasi = "<a id='Block' style='cursor: pointer;'' data-toggle='modal' onclick='GetBlokirUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-user-times'></i> Blokir</a>";
           }
            //mencetak baris baru
            buatTabel += "<tr>"
        
                    //membuat penomoran
                    + "<td><img width='" + uimg + "' height='" + uimg + "' src='" + photo + "'></td>"
                    + "<td><div class='nametbl'> " + data['data'][a]["costumer"] + "</div></td>"
                    + "<td>" + data['data'][a]["email"] + "</td>"
                    + "<td>" + data['data'][a]["phone"] + "</td>"
                    + "<td>" + toRp(data['data'][a]["saldo"]) + "</td>"
                    //+ "<td>" + data['data'][a]["account_number"] + "</td>"
                    //+ "<td>" + data['data'][a]["bank"] + "</td>"
                    //+ "<td>" + data['data'][a]["account_name"] + "</td>"
                    + "<td>" + status + "</td>"
                    + "<td>"
                    + "<div class='btn-group'>"
                    + "<a class='btn btn-default btn-xs' onclick='GetDetailuser(" + data['data'][a]["id"] + ");'  style='cursor: pointer;' data-toggle='modal'  data-target='#myModal'><i class='fa fa-eye'></i> Detail</a>"
                    + "<a class='btn btn-default btn-small ismal dropdown-toggle' data-toggle='dropdown' href='#'>"
                    + "<span class='caret'></span></a>"
                      + "<ul class='dropdown-menu drop'>" 
                        // + "<li>" + konfirmasi + "</li>"
                        + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetDetailmerchant(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit</a></li>"
                        + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiMerchant(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-times' aria-hidden='true'></i> Nonaktifkan</a></li>"
                      + "</ul>" 
                    + "</div>"
                    + "</td>"

        //tutup baris baru
            + "<tr/>";         
      }

      $("#MerchantTable").html(buatTabel);
      if(data['from'] != null){
         var last_page = data['last_page']; 
         
            buatFirst += ""
                 + "<li><button  class='btn btn-default' onClick='GetpageUser(1);'>«</button></li>"
                  + ""; 

           for(i = 1; i < last_page+1; i++)
           {
                  
                if(data['from'] ==1)
                {
                  if([i] ==1)
                  {
                    var aktif = "active";
                  }else{
                    var aktif = "";
                  } 

                }else if(page == data['current_page']){

                  if([i] == page)
                  {
                    var aktif = "active";
                  }else{
                    var aktif = "";
                  }  
               
                }else{
                  var aktif = "";
                }
                   buatPage += ""
                 + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageUser(" + [i] + ");'>" + [i] + "</button></li>"
                  + "";    
           } 
            buatLast += ""
           + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageUser(" + data['last_page']+");''>»</button></li>"
           + "";   
       }
       $(".firstDep").html(buatFirst);
       $(".navigationDep").html(buatPage);
       $(".lastDep").html(buatLast);     
    },
    error: function (data) {
      alert("Gagal list Table User");   
    }
  });
}

/* populate field modal add Merchant */
function GetFieldAddMerchant(){
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
  var token = $('meta[name="csrf-token"]').attr('content');
  var temp = $(location).attr('href');
  var tempurl = temp.split('?type');
  // console.log();
  buatList = "";
  buatList  += "<div class='modal-content'>"
                  +"<div class='modal-header'>"
                      +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                  +"<h4 class='modal-title'>Add Merchant</h4>"
                  +"</div>"
                  +"<form method='POST' enctype='multipart/form-data' action='"+tempurl[0]+"/postmerchant'>"
                  +'<input type="hidden" name="_token" value="'+token+'">'
                    +"<div class='modal-body'>"
                       +"<p><strong>Name : </strong></p><input value='' type='text' class='form-control' name='mer_name' placeholder='Name'>"
                       +"<p><strong>Email : </strong></p><input type='text' class='form-control' name='mer_email' placeholder='Email'>"
                       +"<p><strong>Phone : </strong></p><input type='text' class='form-control' name='mer_phone' placeholder='Phone Number'>"
                       +"<p><strong>City : </strong></p><input type='text' class='form-control' name='mer_city' placeholder='City'>"
                       +"<p><strong>Postal Code : </strong></p><input type='text' class='form-control' name='mer_postal' placeholder='Postal Code'>"
                       +"<p><strong>Address : </strong></p><input type='text' class='form-control' name='mer_address' placeholder='Address'>"
                       +"<p><strong>Photo : </strong></p><input type='file' class='form-control' accept='image/*' name='mer_photo' placeholder='Photo'>"
                    +"</div>"

                    +"<div class='modal-footer'>"
                      +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                      +"<button type='submit' class='btn btn-default'>Ya</button>"
                    +"</div>"
                  +"</form>"
              +"</div>";

  $("#Modals").html(buatList); 
}

function GetDetailmerchant(id)
{
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
  var token = $('meta[name="csrf-token"]').attr('content');
  var temp = $(location).attr('href');
  var tempurl = temp.split('?type');
  // console.log();

  $.ajax({
    type:"GET",
    url: "user/detailmerchant/"+id,
    data: {},
    dataType: "json",
    cache: false,
    success: function(respons){
      console.log(respons);
       buatList = "";
        buatList  += "<div class='modal-content'>"
                        +"<div class='modal-header'>"
                            +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                        +"<h4 class='modal-title'>Edit Co-Branding</h4>"
                        +"</div>"
                        +"<form method='POST' enctype='multipart/form-data' action='"+tempurl[0]+"/postEditmerchant'>"
                        +'<input type="hidden" name="_token" value="'+token+'">'
                        +'<input type="hidden" name="_id" value="'+respons[0].id+'">'
                          +"<div class='modal-body'>"
                             +"<p><strong>Name : </strong></p><input value='"+respons[0].name+"' type='text' class='form-control' name='mer_name' placeholder='Name'>"
                             +"<p><strong>Email : </strong></p><input value='"+respons[0].email+"' type='text' class='form-control' name='mer_email' placeholder='Email'>"
                             +"<p><strong>Phone : </strong></p><input value='"+respons[0].phone+"' type='text' class='form-control' name='mer_phone' placeholder='Phone Number'>"
                             +"<p><strong>City : </strong></p><input value='"+respons[0].city+"' type='text' class='form-control' name='mer_city' placeholder='City'>"
                             +"<p><strong>Postal Code : </strong></p><input value='"+respons[0].postal_code+"' type='text' class='form-control' name='mer_postal' placeholder='Postal Code'>"
                             +"<p><strong>Address : </strong></p><input value='"+respons[0].address+"' type='text' class='form-control' name='mer_address' placeholder='Address'>"
                             +"<p><strong>Photo : </strong></p><input type='file' class='form-control' accept='image/*' name='mer_photo' placeholder='Photo'>"
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button type='submit' class='btn btn-default'>Ya</button>"
                          +"</div>"
                        +"</form>"
                    +"</div>";

        $("#Modals").html(buatList);
    },
    error: function (respons) {
      alert("Gagal memuat data.");
      
    }
  });
}

function GetKonfirmasiMerchant(id){
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

  $.ajax({
    type:"GET",
    url: "user/detailcobrand/"+id,
    data: {},
    dataType: "json",
    cache: false,
    success: function(respons){
      console.log(respons);
       buatList = "";
       buatList  += "<div class='modal-content'>"
                      +"<div class='modal-header'>"
                          +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                      +"<h4 class='modal-title'>Konfirmasi Terima</h4>"
                      +"</div>"

                      +"<div class='modal-body'>"
                               +"<p>Apakah anda yakin akan menonaktifkan co-branding <b>"+ respons[0].name +"</b> ?</p>"    
                      +"</div>"

                      +"<div class='modal-footer'>"
                        +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                        +"<button onclick='HapusCobrand("+ respons[0].id +");' type='button' class='btn btn-default'>Ya</button>"
     
                      +"</div>"
                  +"</div>";

      $("#Modals").html(buatList); 

    },
    error: function (respons) {
      alert("Gagal memuat data.");
      
    }
  });
}

function HapusMerchant(id)
{
  var token = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
    type:"POST",
    url: "user/postHapusCo",
    data:{id:id, _token:token},
    cache: false,
    success: function(respons){
      location.reload();
    },
    error: function(respons){
      alert('Gagal menghapus data');
    }
  });
}

/* populate data table Co-Branding */
function ListCobrandTable(status,search,page) {
  $("#CobrandTable").html('<tr><td colspan="11"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
  $.ajax({
      type:"GET",
      url: "deposite/listtabelcobrand",
      dataType: "json",
      data: {status:status,search:search,page:page},
      success: function(data){        
         
        
         jmlData = data['data'].length;

              if(jmlData =="0"){

                     buatTabel = "<tr><td colspan='10'>Data Kosong</td></tr>";
                     buatPage = "";
                     buatFirst = "";
                     buatLast = "";
               }else{

                  buatTabel = "";
                  buatPage = "";
                  buatFirst = "";
                  buatLast = "";
               }       
         
          for(a = 0; a < jmlData; a++)
          {    
             var url = window.location.toString();
             var img = "/img/avatar.png";
             var photo =  url.img;
             var uimg = "50";
             if(photo != null || photo !='')
             {
                photo = data['data'][a]["photo"];
             } 

	    	
		

             var st = data['data'][a]["status"];
             if(st =="0")
             {
              var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Proses</div>";
              var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Terima</a>";
             }else if(st =="2"){
              var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Blokir</div>";
              var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetBataluser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-recycle' aria-hidden='true'></i> Batalkan</a>";
             }else{
              var status = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Sukses</div>";
              var konfirmasi = "<a id='Block' style='cursor: pointer;'' data-toggle='modal' onclick='GetBlokirUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-user-times'></i> Blokir</a>";
             }
              //mencetak baris baru
              buatTabel += "<tr>"
          
                      //membuat penomoran
                      + "<td><img width='" + uimg + "' height='" + uimg + "' src='" + photo + "'></td>"
                      + "<td><div class='nametbl'> " + data['data'][a]["costumer"] + "</div></td>"
		   		
                      + "<td>" + data['data'][a]["email"] + "</td>"
                      + "<td>" + data['data'][a]["phone"] + "</td>"
                      + "<td>" + toRp(data['data'][a]["saldo"]) + "</td>"
                      + "<td>" + status + "</td>"
                      + "<td>"
                      + "<div class='btn-group'>"
                      + "<a class='btn btn-default btn-xs' onclick='GetDetailuser(" + data['data'][a]["id"] + ");'  style='cursor: pointer;' data-toggle='modal'  data-target='#myModal'><i class='fa fa-eye'></i> Detail</a>"
                      + "<a class='btn btn-default btn-small ismal dropdown-toggle' data-toggle='dropdown' href='#'>"
                      + "<span class='caret'></span></a>"
                        + "<ul class='dropdown-menu drop'>" 
                          // + "<li>" + konfirmasi + "</li>"
                          + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetDetailcobrand(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit</a></li>"
                          + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiCoBrand(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-times' aria-hidden='true'></i> Nonaktifkan</a></li>"
                        + "</ul>" 
                      + "</div>"
                      + "</td>"

          //tutup baris baru
              + "<tr/>";         
        }

        $("#CobrandTable").html(buatTabel);
        if(data['from'] != null){
           var last_page = data['last_page']; 
           
              buatFirst += ""
                   + "<li><button  class='btn btn-default' onClick='GetpageUser(1);'>«</button></li>"
                    + ""; 

             for(i = 1; i < last_page+1; i++)
             {
                    
                  if(data['from'] ==1)
                  {
                    if([i] ==1)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    } 

                  }else if(page == data['current_page']){

                    if([i] == page)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    }  
                 
                  }else{
                    var aktif = "";
                  }
                     buatPage += ""
                   + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageUser(" + [i] + ");'>" + [i] + "</button></li>"
                    + "";    
             } 
              buatLast += ""
             + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageUser(" + data['last_page']+");''>»</button></li>"
             + "";   
         }
         $(".firstDep").html(buatFirst);
         $(".navigationDep").html(buatPage);
         $(".lastDep").html(buatLast);     
      },
      error: function (data) {
        alert("Gagal list Table User");   
      }
    });
}

/* populate field modal add Co-Branding */
function GetFieldAddCoBranding(){
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
  var token = $('meta[name="csrf-token"]').attr('content');
  var temp = $(location).attr('href');
  var tempurl = temp.split('?type');
  // console.log();
  buatList = "";
  buatList  += "<div class='modal-content'>"
                  +"<div class='modal-header'>"
                      +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                  +"<h4 class='modal-title'>Add Co-Branding</h4>"
                  +"</div>"
                  +"<form method='POST' enctype='multipart/form-data' action='"+tempurl[0]+"/postCobrand'>"
                  +'<input type="hidden" name="_token" value="'+token+'">'
                    +"<div class='modal-body'>"
                       +"<p><strong>Name : </strong></p><input value='' type='text' class='form-control' name='co_name' placeholder='Name'>"
                       +"<p><strong>Email : </strong></p><input type='text' class='form-control' name='co_email' placeholder='Email'>"
                       +"<p><strong>Phone : </strong></p><input type='text' class='form-control' name='co_phone' placeholder='Phone Number'>"
                       +"<p><strong>City : </strong></p><input type='text' class='form-control' name='co_city' placeholder='City'>"
                       +"<p><strong>Postal Code : </strong></p><input type='text' class='form-control' name='co_postal' placeholder='Postal Code'>"
                       +"<p><strong>Address : </strong></p><input type='text' class='form-control' name='co_address' placeholder='Address'>"
                       +"<p><strong>Photo : </strong></p><input type='file' class='form-control' accept='image/*' name='co_photo' placeholder='Photo'>"
                    +"</div>"

                    +"<div class='modal-footer'>"
                      +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                      +"<button type='submit' class='btn btn-default'>Ya</button>"
                    +"</div>"
                  +"</form>"
              +"</div>";

  $("#Modals").html(buatList); 
}

function GetDetailcobrand(id)
{
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
  var token = $('meta[name="csrf-token"]').attr('content');
  var temp = $(location).attr('href');
  var tempurl = temp.split('?type');
  // console.log();

  $.ajax({
    type:"GET",
    url: "user/detailcobrand/"+id,
    data: {},
    dataType: "json",
    cache: false,
    success: function(respons){
      console.log(respons);
       buatList = "";
        buatList  += "<div class='modal-content'>"
                        +"<div class='modal-header'>"
                            +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                        +"<h4 class='modal-title'>Edit Co-Branding</h4>"
                        +"</div>"
                        +"<form method='POST' enctype='multipart/form-data' action='"+tempurl[0]+"/postEditCo'>"
                        +'<input type="hidden" name="_token" value="'+token+'">'
                        +'<input type="hidden" name="_id" value="'+respons[0].id+'">'
                          +"<div class='modal-body'>"
                             +"<p><strong>Name : </strong></p><input value='"+respons[0].name+"' type='text' class='form-control' name='co_name' placeholder='Name'>"
                             +"<p><strong>Email : </strong></p><input value='"+respons[0].email+"' type='text' class='form-control' name='co_email' placeholder='Email'>"
                             +"<p><strong>Phone : </strong></p><input value='"+respons[0].phone+"' type='text' class='form-control' name='co_phone' placeholder='Phone Number'>"
                             +"<p><strong>City : </strong></p><input value='"+respons[0].city+"' type='text' class='form-control' name='co_city' placeholder='City'>"
                             +"<p><strong>Postal Code : </strong></p><input value='"+respons[0].postal_code+"' type='text' class='form-control' name='co_postal' placeholder='Postal Code'>"
                             +"<p><strong>Address : </strong></p><input value='"+respons[0].address+"' type='text' class='form-control' name='co_address' placeholder='Address'>"
                             +"<p><strong>Photo : </strong></p><input type='file' class='form-control' accept='image/*' name='co_photo' placeholder='Photo'>"
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button type='submit' class='btn btn-default'>Ya</button>"
                          +"</div>"
                        +"</form>"
                    +"</div>";

        $("#Modals").html(buatList);
    },
    error: function (respons) {
      alert("Gagal memuat data.");
      
    }
  });
}

function GetKonfirmasiCoBrand(id){
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

  $.ajax({
    type:"GET",
    url: "user/detailcobrand/"+id,
    data: {},
    dataType: "json",
    cache: false,
    success: function(respons){
      console.log(respons);
       buatList = "";
       buatList  += "<div class='modal-content'>"
                      +"<div class='modal-header'>"
                          +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                      +"<h4 class='modal-title'>Konfirmasi Terima</h4>"
                      +"</div>"

                      +"<div class='modal-body'>"
                               +"<p>Apakah anda yakin akan menonaktifkan co-branding <b>"+ respons[0].name +"</b> ?</p>"    
                      +"</div>"

                      +"<div class='modal-footer'>"
                        +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                        +"<button onclick='HapusCobrand("+ respons[0].id +");' type='button' class='btn btn-default'>Ya</button>"
     
                      +"</div>"
                  +"</div>";

      $("#Modals").html(buatList); 

    },
    error: function (respons) {
      alert("Gagal memuat data.");
      
    }
  });
}

function HapusCobrand(id)
{
  var token = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
    type:"POST",
    url: "user/postHapusCo",
    data:{id:id, _token:token},
    cache: false,
    success: function(respons){
      location.reload();
    },
    error: function(respons){
      alert('Gagal menghapus data');
    }
  });
}


function GetDetailuser(id)
{
    var url = window.location.toString();
   var type = url.split('=');

   $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

    $.ajax({
      type:"GET",
      url: "deposite/detailuser",
      data: {id:id, type:type[1]},
      cache: false,
      success: function(respons){   
              console.log(respons);
               var url = window.location.toString();
               var img = "/img/avatar.png";
               var photo =  url+''+img;
               var uimg = "50";
               // if(photo != null || photo !='')
               // {
               //    photo = respons[0]["identity_photo"];
               // }

               if(respons[0]["identity_photo"] != ''){
                  photo = respons[0]["identity_photo"];
               } 

               if(respons[0]["kyc"] =="0")
               {
                 var verified = "<div class='njh'>"+ respons[0]['name'] +"</div> <div  class='rdef' ><i class='fa fa-user-times' aria-hidden='true'></i> Belum Verified</div>"; 
               }else if(respons[0]["kyc"] =="2"){
                var verified = "<div class='njh'>"+ respons[0]['name'] +"</div> <div  class='rdef' ><i class='fa fa-user-times' aria-hidden='true'></i> User diblokir</div> "; 
               }else{

                 var verified = "<div class='njh'>"+ respons[0]['name'] +"</div> <div  class='rde' ><i class='fa fa-check' aria-hidden='true'></i> Verified</div>";
               } 
               var bank = acc = acc_name = '';
               if (typeof respons[0]['bank'] == 'undefined') { bank = '-'; } else { bank = respons[0]['bank']; }
               if(typeof respons[0]['account_number'] == 'undefined') { acc = '-'; } else { acc = respons[0]['account_number']; }
               if(typeof respons[0]['account_name'] == 'undefined') { acc_name =  '-'; } else { acc_name = respons[0]['account_name']; }

        buatList = "";
           buatList  += "<div class='pull-left modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Detail "+ type[1] +"</h4>"
                          +"</div>"

                          +"<div class='pull-left modal-body'>"
                                   +"<div class='form-group col-sm-6'><img width='100' height='100' src='"+ photo +"'></div>"  
                                  +"<div class='form-group col-sm-6'><label for='email'>No Identitas:</label><p>"+ respons[0]['identity_number'] +"</p></div>"
                                   +"<div class='form-group col-sm-6 '><label class='pull-left vfgr' for='name'>Nama:</label> "+ verified +"</div>"  
                                     +"<div class='form-group col-sm-6'><label for='email'>Email:</label><p>"+ respons[0]['email'] +"</p></div>"
                                    +"<div class='form-group col-sm-6'><label for='bank'>Bank:</label><p>"+ bank +"</p></div>"
                                   +"<div class='form-group col-sm-6'><label for='saldo'>Saldo:</label><p>"+ toRp(respons[0]['saldo']) +"</p></div>" 
                                   +"<div class='form-group col-sm-6'><label for='no_rekening'>No Rekening:</label><p>"+ acc +"</p></div>" 
                                   +"<div class='form-group col-sm-6'><label for='atas_nama'>Atas Nama:</label><p>"+ acc_name +"</p></div>"
                                   
                          +"</div>"

                          +"<div class='pull-left modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                           
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 
       

      },
      error: function (respons) {
        alert("Gagal Detail Deposite ");
        
      }
    });
}

function GetKonfirmasiUser(id){
    var url = window.location.toString();
    var type = url.split('=');
    $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

    $.ajax({
      type:"GET",
      url: "deposite/detailuser",
      data: {id:id, type:type[1]},
      cache: false,
      success: function(respons){   
        
           buatList = "";
           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi Terima</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p>Apakah anda yakin data atas nama <b>"+ respons[0]['name'] +"</b> sudah lengkap?</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button onclick='KonfirmasiUser("+ respons[0]['id'] +");' type='button' class='btn btn-default'>Ya</button>"
         
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

      },
      error: function (respons) {
        alert("Gagal blokir user ");
        
      }
    });


}


function KonfirmasiUser(id){
$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');   
var url = window.location.toString();
 var type = url.split('=');
$.ajax({
      type:"GET",
      url: "deposite/terimauser",
      data: {id:id,type:type[1]},
      cache: false,
      success: function(respons){  
       
        var status = "10";
        var search = $('#search').val();


          buatList = "";

           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi "+ type[1] +" </h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p><i class='fa fa-check' aria-hidden='true'></i>sukses atas nama <b>"+ respons[0]['name'] +"</b> berhasil diverifikasi.</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

        var status = "10";
        var search = $('#search').val();
        var pages = "";

       if(type[1] =="costumer")
       { 

         ListUserTable(status,search,pages);
       }else if(type[1] =="issuer"){
        ListIssuerTable(status,search,pages);
       } 

      },
      error: function (respons) {
        alert("Gagal Kirim Deposite ");
        
      }
    });


}



function GetBlokirUser(id){
$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

    $.ajax({
      type:"GET",
      url: "deposite/detailuser",
      data: {id:id},
      cache: false,
      success: function(respons){   
        
           buatList = "";
           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi Blokir</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p>Apakah anda yakin  atas nama <b>"+ respons[0]['name'] +"</b> akan diblokir?</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button onclick='BlokirUser("+ respons[0]['id'] +");' type='button' class='btn btn-default'>Ya</button>"
         
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

      },
      error: function (respons) {
        alert("Gagal blokir user ");
        
      }
    });


}

function BlokirUser(id){
$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');   
var url = window.location.toString();
 var type = url.split('=');
$.ajax({
      type:"GET",
      url: "deposite/blokiruser",
      data: {id:id,type:type[1]},
      cache: false,
      success: function(respons){  
       
        var status = "10";
        var search = $('#search').val();


          buatList = "";

           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi "+ type[1] +" </h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p><i class='fa fa-check' aria-hidden='true'></i> Atas nama <b>"+ respons[0]['name'] +"</b> sukses diblokir.</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

        var status = "10";
        var search = $('#search').val();
        var pages = "";

       if(type[1] =="costumer")
       { 

         ListUserTable(status,search,pages);
       }else{

        

       } 

      },
      error: function (respons) {
        alert("Gagal Kirim Deposite ");
        
      }
    });


}


function GetBataluser(id){
$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

    $.ajax({
      type:"GET",
      url: "deposite/detailuser",
      data: {id:id},
      cache: false,
      success: function(respons){   
        
           buatList = "";
           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi Batal</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p>Apakah anda yakin  atas nama <b>"+ respons[0]['name'] +"</b> akan dibatalkan?</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button onclick='BatalUser("+ respons[0]['id'] +");' type='button' class='btn btn-default'>Ya</button>"
         
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

      },
      error: function (respons) {
        alert("Gagal blokir user ");
        
      }
    });


}


function BatalUser(id){
$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');   
var url = window.location.toString();
 var type = url.split('=');
$.ajax({
      type:"GET",
      url: "deposite/bataluser",
      data: {id:id,type:type[1]},
      cache: false,
      success: function(respons){  
       
        var status = "10";
        var search = $('#search').val();


          buatList = "";

           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi "+ type[1] +"</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p><i class='fa fa-check' aria-hidden='true'></i> Atas nama <b>"+ respons[0]['name'] +"</b> sukses dibatalkan.</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

        var status = "10";
        var search = $('#search').val();
        var pages = "";

       if(type[1] =="costumer")
       { 
        
        ListUserTable(status,search,pages);
       }else{

        

       } 

      },
      error: function (respons) {
        alert("Gagal batal user ");
        
      }
    });


}


function getbank()
{

$.ajax({
      type:"GET",
      url: "deposite/bankAdmin",
      dataType: "json",
      data: {},
      success: function(data){        
         
        
         jmlData = data['data'].length;
         bankID = "";
         Listoptions = "";

         options = "<option>Pilih Bank</option>";
          for(a = 0; a < jmlData; a++)
          {
             
              if(jmlData =="1")
               { 

            //mencetak baris baru
              
            bankID += ""+ options +" <option value='" + data['data'][a]["id"] + "'>"
                        + "" + data['data'][a]["name"] + ""
            //tutup baris baru
                + "</option>";

                }else{


    

               bankID  += " "+ options +"  <option value='" + data['data'][a]["id"] + "'>"
                        + "" + data['data'][a]["name"] + ""
            //tutup baris baru
                + "</option>"; 



                }
               
           }

      $("#Bank").html(bankID);

      
      },
      error: function (data) {
        alert("Gagal list Table Deposite ");
        
      }
    });

}

function getsetuju(sel,id)
{
    var idtr   = id;
    var idbank = sel.value;

    //alert(sel.value);
    
     btn = "<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'> <button onclick='Kirim("+ idtr +","+ idbank +");' type='button' class='btn btn-default'>Ya</button>";

     $(".modal-footer").html(btn);
}





function GetKirim(id){
    var url = window.location.toString();
    var type = url.split('=');
   
   $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
$.ajax({
      type:"GET",
      url: "deposite/konfirmasi",
      data: {id:id},
      cache: false,
      success: function(respons){  

              Listbank = "";
             if(type[1] =="withdraw")
             {

                getbank();
                Listbank = "<table><tr><td ><p>Apakah anda yakin menyetujui ?</p></td></tr>  <tr><td>Nama</td> <td> <b>: "+ respons[0]['name'] +"</b></td></tr><tr> <td>Bank</td><td><b > : "+ respons[0]['bank_costumer'] +" | "+ respons[0]['no_account_costumer'] +"</b></td></tr><tr> <td>Tarik Saldo</td><td> <b>:" + toRp(respons[0]['price']) + "</b></td></tr><tr> <td>Rekening Layana</td> <td><b>:</b> <select id='Bank' onchange='getsetuju(this,"+ respons[0]['transaction_id'] +");'></select></td> </tr></table>";

             }else if(type[1] =="deposite"){
               
                getbank();
                Listbank = "<table><tr><td ><p>Apakah anda yakin menyetujui ?</p></td></tr>  <tr><td>Nama</td> <td> <b>: "+ respons[0]['name'] +"</b></td></tr><tr> <td>Bank</td><td><b > : "+ respons[0]['bank_costumer'] +" | "+ respons[0]['no_account_costumer'] +"</b></td></tr><tr> <td>Tambah Saldo</td><td> <b>:" + toRp(respons[0]['price']) + "</b></td></tr><tr> <td>Rekening Layana</td> <td><b>:</b> <select id='Bank' onchange='getsetuju(this,"+ respons[0]['transaction_id'] +");'></select></td> </tr></table>";
             } 

           buatList = "";
           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi " + type[1] +"</h4>"
                          +"</div>"

                           

                          +"<div class='modal-body'>"
                                   +"  "+ Listbank +"   <div class='tes'></div>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +""  
                            +""
         
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 
       
      },
      error: function (respons) {
        alert("Gagal Modal Deposite ");
        
      }
    });

}



function GetBatal(id){
   $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

    $.ajax({
      type:"GET",
      url: "deposite/konfirmasi",
      data: {id:id},
      cache: false,
      success: function(respons){   
        
           buatList = "";
           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi Pembatalan</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p>Apakah anda yakin ,  saldo <b>" + toRp(respons[0]['price']) + "</b> atas nama <b>"+ respons[0]['name'] +"</b> akan dibatalkan?</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button onclick='Batal("+ respons[0]['transaction_id'] +");' type='button' class='btn btn-default'>Ya</button>"
         
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

      },
      error: function (respons) {
        alert("Gagal batalkan Deposite ");
        
      }
    });

}





function GetDestroy(id){
    var url = window.location.toString();
  var type = url.split('=');
   $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
$.ajax({
      type:"GET",
      url: "deposite/konfirmasi",
      data: {id:id},
      cache: false,
      success: function(respons){        
        

        buatList = "";
           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi Hapus</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p>Apakah anda yakin, "+ type[1] +" saldo atas nama <b>"+ respons[0]['costumer'] +"</b> sebesar <b>" + toRp(respons[0]['price']) + "</b> akan dihapus?</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button onclick='Hapus("+ respons[0]['transaction_id'] +");' type='button' class='btn btn-default'>Ya</button>"
         
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

      },
      error: function (respons) {
        alert("Gagal delete Deposite ");
        
      }
    });

}





function Kirim(id,bank){
$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');   
var url = window.location.toString();
 var type = url.split('=');
$.ajax({
      type:"GET",
      url: "deposite/kirim",
      data: {id:id,type:type[1],bank_send:bank},
      cache: false,
      success: function(respons){  
       
        var status = "10";
        var search = $('#search').val();


          buatList = "";

           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi "+ type[1] +" Saldo</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p><i class='fa fa-check' aria-hidden='true'></i> Sukses Menambahkan saldo <b>"+ respons[0]['name'] +"</b> sebesar <b>" + toRp(respons[0]['price']) + "</b></p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 


       if(type[1] =="deposite")
       { 

        ListDepositeTable(status,search);
        ListUserDeposite();

       }else{

          ListWithdrawTable(status,search);
          ListUserWithdraw();

       } 

      },
      error: function (respons) {
        alert("Gagal Kirim Deposite ");
        
      }
    });

}


function Batal(id){

$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');   
var url = window.location.toString();
 var type = url.split('=');

$.ajax({
      type:"GET",
      url: "deposite/batal",
      data: {id:id,type:type[1]},
      cache: false,
      success: function(respons){  
       
        var status = "10";
        var search = $('#search').val();
       
          buatList = "";

           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi Pembatalan</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p><i class='fa fa-check' aria-hidden='true'></i> Sukses Membatalkan <b>"+ type[1] +"</b> atas nama <b>"+ respons[0]['name'] +"</b> Sebesar <b>" + toRp(respons[0]['price']) + "</b> </p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 

        
        if(type[1] =="deposite")
        {

           ListDepositeTable(status,search);
           ListUserDeposite();

         }else{

          ListWithdrawTable(status,search);
          ListUserWithdraw();

         } 

        
        
      },
      error: function (respons) {
        alert("Gagal Blokir Deposite ");
        
      }
    });

}


function Hapus(id){
  var url = window.location.toString();
 var type = url.split('=');
$("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');   
$.ajax({
      type:"GET",
      url: "deposite/delete",
      data: {id:id},
      cache: false,
      success: function(respons){  
       
        var status = "10";
        var search = $('#search').val();
        
        buatList = "";

           buatList  += "<div class='modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Konfirmasi Hapus</h4>"
                          +"</div>"

                          +"<div class='modal-body'>"
                                   +"<p><i class='fa fa-check' aria-hidden='true'></i> Sukses, "+ type[1] +" berhasil dihapus</p>"    
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Keluar' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button id='SendDeposit' alt='' type='button' class='btn btn-default'>Ya</button>"
         
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 
        $('#SendDeposit').hide();


          if(type[1] =="deposite")
          {  
         
          ListDepositeTable(status,search);
          ListUserDeposite();

          }else{

           ListWithdrawTable(status,search);
           ListUserWithdraw();
          }
        
      },
      error: function (respons) {
        alert("Gagal Hapus Deposite ");
        
      }
    });

}




function GetDetail(id){
   var url = window.location.toString();
   var type = url.split('=');

   $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

    $.ajax({
      type:"GET",
      url: "deposite/konfirmasi",
      data: {id:id},
      cache: false,
      success: function(respons){   

               var url = window.location.toString();
               var img = "/img/avatar.png";
               var photo =  url.img;
               var uimg = "50";
               if(photo != null || photo !='')
               {
                  photo = respons[0]["photo"];
               } 

               if(respons[0]["kyc"] =="0")
               {
                 var verified = "<i class='fa fa-user-times' aria-hidden='true'></i> Belum Verified"; 
               }else{

                 var verified = "<div class='njh'> <b>"+ respons[0]['name'] +"</b></div> <div  class='rde' ><i class='fa fa-check' aria-hidden='true'></i> Verified</div>";
               } 

               
     
        buatList = "";
           buatList  += "<div class='pull-left modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Detail "+ type[1] +"</h4>"
                          +"</div>"

                          +"<div class='pull-left modal-body'>"
                                   +"<div class='form-group col-sm-6'><img width='100' height='100' src='"+ photo +"'></div>"  
                                   +"<div class='form-group col-sm-6 '><label class='pull-left vfgr' for='name'>Nama:</label> "+ verified +"</div>"  
                                     +"<div class='form-group col-sm-6'><label for='email'>Email:</label><p>"+ respons[0]['email'] +"</p></div>"
                                    +"<div class='form-group col-sm-6'><label for='bank'>Bank:</label><p>"+ respons[0]['bank_costumer'] +"</p></div>"
                                   +"<div class='form-group col-sm-6'><label for='saldo'>Saldo:</label><p>"+ toRp(respons[0]['saldo']) +"</p></div>" 
                                   
                                   +"<div class='form-group col-sm-6'><label for='type'>"+ type[1] +":</label><p>"+ toRp(respons[0]['price']) +"</p></div>"
                                   
                                   +"<div class='form-group col-sm-6'><label for='no_rekening'>No Rekening:</label><p>"+ respons[0]['no_account_costumer'] +"</p></div>" 
                                   +"<div class='form-group col-sm-6'><label for='atas_nama'>Atas Nama:</label><p>"+ respons[0]['account_costumer'] +"</p></div>"
                                   +"<div class='form-group col-sm-6'><label for='tanggal'>Tanggal Transfer:</label><p>"+ datebln(respons[0]['tanggal']) +"</p></div>"
                          +"</div>"

                          +"<div class='pull-left modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                           
                          +"</div>"
                      +"</div>";

        $("#Modals").html(buatList); 
       

      },
      error: function (respons) {
        alert("Gagal Detail Deposite ");
        
      }
    });

}


function UpdateNotifDeposite(){
$.ajax({
      type:"GET",
      url: "deposite/updatenotivdep",
      cache: false,
      success: function(data){  
        
        
         

      },
      error: function (data) {
        //alert("Gagal Update ");
        
      }
    });

}

function UpdateNotifWithdraw(){
$.ajax({
      type:"GET",
      url: "deposite/updatenotivdraw",
      cache: false,
      success: function(data){  
        
         //JmlWithdraw();

      },
      error: function (data) {
        //alert("Gagal Update ");
        
      }
    });

}

function UpdateNotifUser(){
$.ajax({
      type:"GET",
      url: "deposite/updatenotivuser",
      cache: false,
      success: function(data){  
      

      },
      error: function (data) {
        //alert("Gagal Update ");
        
      }
    });

}




function GetpageUser(page){
  var pages = page;
  var status = $('#StVerCos').val();
  var search = $('#searchCos').val();
 
  if(pages ==null | pages ==""){
    
   
  }else{
      //alert(status);
    ListUserTable(status,search,page);
   
  }
}



function GetpageDep(page){
  var pages = page;
  var status = $('#StVerDep').val();
  var search = $('#searchDep').val();
 
  if(pages ==null | pages ==""){
    
   
  }else{
      
    ListDepositeTable(status,search,page);
   
  }
}

function GetpageDraw(page){
  var pages = page;
  var status = $('#StVerDraw').val();
  var search = $('#searchDraw').val();
 
  if(pages ==null | pages ==""){
    
   
  }else{
    
    ListWithdrawTable(status,search,page);
   
  }
}




function GetpageOrder(page){
  var pages = page;
  var status = $('#StVerDep').val();
  var search = $('#searchDep').val();
 
  if(pages ==null | pages ==""){
    
   
  }else{
      
    datatableorder(status,search,page);
   
  }
}





function SimpanProfile(){

$.ajax({
      type:"GET",
      url: "admin/update",
      data: {name:name,email:email,phone:phone,address:address},
      cache: false,
      success: function(respons){  
       
        

      },
      error: function (respons) {
        alert("Gagal update profile ");
        
      }
    });

}



function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
}

function datebln(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

function toDate(dateStr) {
    const [day, month, year] = dateStr.split("-")
    return new Date(year, month - 1, day)
}


function time_ago(time) {

  switch (typeof time) {
    case 'number':
      break;
    case 'string':
      time = +new Date(time);
      break;
    case 'object':
      if (time.constructor === Date) time = time.getTime();
      break;
    default:
      time = +new Date();
  }
  var time_formats = [
    [60, 'seconds', 1], // 60
    [120, '1 minute ago', '1 minute from now'], // 60*2
    [3600, 'minutes', 60], // 60*60, 60
    [7200, '1 hour ago', '1 hour from now'], // 60*60*2
    [86400, 'hours', 3600], // 60*60*24, 60*60
    [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
    [604800, 'days', 86400], // 60*60*24*7, 60*60*24
    [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
    [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
    [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
    [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
    [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
    [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
    [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
    [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
  ];
  var seconds = (+new Date() - time) / 1000,
    token = 'ago',
    list_choice = 1;

  if (seconds == 0) {
    return 'Just now'
  }
  if (seconds < 0) {
    seconds = Math.abs(seconds);
    token = 'from now';
    list_choice = 2;
  }
  var i = 0,
    format;
  while (format = time_formats[i++])
    if (seconds < format[0]) {
      if (typeof format[2] == 'string')
        return format[list_choice];
      else
        return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
    }
  return time;
}

function ListIssuerTable(status,search,page) {
  $("#body_table_issuer").html('<tr><td colspan="11"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
  $.ajax({
      type:"GET",
      url: "issuer/dataissuer",
      dataType: "json",
      data: {status:status,search:search,page:page},
      success: function(data){
        // console.log(data);    
        jmlData = data['data'].length;
            if(jmlData =="0"){
                buatTabel = "<tr><td colspan='10'>Data Kosong</td></tr>";
                buatPage = "";
                buatFirst = "";
                buatLast = "";
            }else{
                buatTabel = "";
                buatPage = "";
                buatFirst = "";
                buatLast = "";
            }       
           
          for(a = 0; a < jmlData; a++)
            {
              var url = window.location.toString();
              var img = "/img/avatar.png";
              var photo =  url.img;
              var uimg = "50";
              if(photo != null || photo !='')
              {
                  photo = data['data'][a]["photo"];
              } 

              var st = data['data'][a]["status"];
              if(st =="0")
              {
                  var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Proses</div>";
                  var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-check' aria-hidden='true'></i> Konfirmasi</a>";
              }else if(st =="2"){
                  var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Blokir</div>";var konfirmasi = "";
                  // var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetBataluser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-recycle' aria-hidden='true'></i> Batalkan</a>";
              }else{
                  var status = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Sukses</div>";var konfirmasi = "";
                  // var konfirmasi = "<a id='Block' style='cursor: pointer;'' data-toggle='modal' onclick='GetBlokirUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-user-times'></i> Blokir</a>";
              }
              var kyc = data['data'][a]["kyc"];
              if(kyc == 0)
              {
                var status_kyc = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Belum Lengkap</div>";
              }
              else
              {
                var status_kyc = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Data Lengkap</div>";
              }
              //mencetak baris baru
              buatTabel += "<tr>"
                        //membuat penomoran
                        + "<td><img width='" + uimg + "' height='" + uimg + "' src='" + photo + "'></td>"
                        + "<td><div class='nametbl'> " + data['data'][a]["name"] + "</div></td>"
                        + "<td>" + toRp(data['data'][a]["bank_saldo"]) + "</td>"
                        + "<td>" + status + "</td>"
                        + "<td>" + status_kyc + "</td>"
                        + "<td>"
                        + "<div class='btn-group'>"
                        + "<a class='btn btn-default btn-xs' onclick='GetDetailuser(" + data['data'][a]["id"] + ");'  style='cursor: pointer;' data-toggle='modal'  data-target='#myModal'><i class='fa fa-eye'></i> Detail</a>"
                        + "<a class='btn btn-default btn-small ismal dropdown-toggle' data-toggle='dropdown' href='#'>"
                        + "<span class='caret'></span></a>"
                          + "<ul class='dropdown-menu drop'>" 
                            + "<li>" + konfirmasi + "</li>"
                            + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetDetailissuer(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit</a></li>"
                            + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiIssuer(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-times' aria-hidden='true'></i> Nonaktifkan</a></li>"
                          + "</ul>" 
                        + "</div>"
                        + "</td>"
                //tutup baris baru
                + "<tr/>";         
          }

          $("#body_table_issuer").html(buatTabel);
          if(data['from'] != null){
             var last_page = data['last_page']; 
             
                buatFirst += ""
                     + "<li><button  class='btn btn-default' onClick='GetpageUser(1);'>«</button></li>"
                      + ""; 

               for(i = 1; i < last_page+1; i++)
               {
                      
                    if(data['from'] ==1)
                    {
                      if([i] ==1)
                      {
                        var aktif = "active";
                      }else{
                        var aktif = "";
                      } 

                    }else if(page == data['current_page']){

                      if([i] == page)
                      {
                        var aktif = "active";
                      }else{
                        var aktif = "";
                      }  
                   
                    }else{
                      var aktif = "";
                    }
                       buatPage += ""
                     + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageUser(" + [i] + ");'>" + [i] + "</button></li>"
                      + "";    
               } 
                buatLast += ""
               + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageUser(" + data['last_page']+");''>»</button></li>"
               + "";   
          }
          $(".firstDep").html(buatFirst);
          $(".navigationDep").html(buatPage);
          $(".lastDep").html(buatLast);     
      },
      error: function (data) {
        // console.log(data);
          alert("Gagal list Table User");   
      }
    });
}

function GetFieldAddIssuer(){
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
  var token = $('meta[name="csrf-token"]').attr('content');
  var temp = $(location).attr('href');
  var tempurl = temp.split('?type');
  // console.log();
  buatList = "";
  buatList  += "<div class='modal-content'>"
                  +"<div class='modal-header'>"
                      +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                  +"<h4 class='modal-title'>Add Issuer</h4>"
                  +"</div>"
                  +"<form method='POST' enctype='multipart/form-data' action='"+tempurl[0]+"/postissuer'>"
                  +'<input type="hidden" name="_token" value="'+token+'">'
                    +"<div class='modal-body'>"
                       +"<p><strong>Name : </strong></p><input value='' type='text' class='form-control' name='co_name' placeholder='Name'>"
                       +"<p><strong>Email : </strong></p><input type='text' class='form-control' name='co_email' placeholder='Email'>"
                       +"<p><strong>Phone : </strong></p><input type='text' class='form-control' name='co_phone' placeholder='Phone Number'>"
                       +"<p><strong>City : </strong></p><input type='text' class='form-control' name='co_city' placeholder='City'>"
                       +"<p><strong>Postal Code : </strong></p><input type='text' class='form-control' name='co_postal' placeholder='Postal Code'>"
                       +"<p><strong>Address : </strong></p><input type='text' class='form-control' name='co_address' placeholder='Address'>"
                       +"<p><strong>Photo : </strong></p><input type='file' class='form-control' accept='image/*' name='co_photo' placeholder='Photo'>"
                    +"</div>"

                    +"<div class='modal-footer'>"
                      +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                      +"<button type='submit' class='btn btn-default'>Ya</button>"
                    +"</div>"
                  +"</form>"
              +"</div>";

  $("#Modals").html(buatList); 
}

function GetKonfirmasiIssuer(id){
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');

  $.ajax({
    type:"GET",
    url: "user/detailcobrand/"+id,
    data: {},
    dataType: "json",
    cache: false,
    success: function(respons){
      console.log(respons);
       buatList = "";
       buatList  += "<div class='modal-content'>"
                      +"<div class='modal-header'>"
                          +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                      +"<h4 class='modal-title'>Konfirmasi Issuer</h4>"
                      +"</div>"

                      +"<div class='modal-body'>"
                               +"<p>Apakah anda yakin akan menonaktifkan Issuer <b>"+ respons[0].name +"</b> ?</p>"    
                      +"</div>"

                      +"<div class='modal-footer'>"
                        +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                        +"<button onclick='HapusIssuer("+ respons[0].id +");' type='button' class='btn btn-default'>Ya</button>"
     
                      +"</div>"
                  +"</div>";

      $("#Modals").html(buatList); 

    },
    error: function (respons) {
      alert("Gagal memuat data.");
      
    }
  });
}

function HapusIssuer(id)
{
  var token = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
    type:"POST",
    url: "user/postHapusissuer",
    data:{id:id, _token:token},
    cache: false,
    success: function(respons){
      location.reload();
    },
    error: function(respons){
      alert('Gagal menghapus data');
    }
  });
}

function GetDetailissuer(id)
{
  $("#Modals").html('<div class="modal-body"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></div>');
  var token = $('meta[name="csrf-token"]').attr('content');
  var temp = $(location).attr('href');
  var tempurl = temp.split('?type');
  // console.log();

  $.ajax({
    type:"GET",
    url: "user/detailcobrand/"+id,
    data: {},
    dataType: "json",
    cache: false,
    success: function(respons){
      console.log(respons);
       buatList = "";
        buatList  += "<div class='modal-content'>"
                        +"<div class='modal-header'>"
                            +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                        +"<h4 class='modal-title'>Edit Co-Branding</h4>"
                        +"</div>"
                        +"<form method='POST' enctype='multipart/form-data' action='"+tempurl[0]+"/postEditIssuer'>"
                        +'<input type="hidden" name="_token" value="'+token+'">'
                        +'<input type="hidden" name="_id" value="'+respons[0].id+'">'
                        
                          +"<div class='modal-body'>"
                             +"<p><strong>Name : </strong></p><input value='"+respons[0].name+"' type='text' class='form-control' name='co_name' placeholder='Name'>"
                             +"<p><strong>Email : </strong></p><input value='"+respons[0].email+"' type='text' class='form-control' name='co_email' placeholder='Email'>"
                             +"<p><strong>Phone : </strong></p><input value='"+respons[0].phone+"' type='text' class='form-control' name='co_phone' placeholder='Phone Number'>"
                             +"<p><strong>City : </strong></p><input value='"+respons[0].city+"' type='text' class='form-control' name='co_city' placeholder='City'>"
                             +"<p><strong>Postal Code : </strong></p><input value='"+respons[0].postal_code+"' type='text' class='form-control' name='co_postal' placeholder='Postal Code'>"
                             +"<p><strong>Address : </strong></p><input value='"+respons[0].address+"' type='text' class='form-control' name='co_address' placeholder='Address'>"
                             +"<p><strong>Photo : </strong></p><input type='file' class='form-control' accept='image/*' name='co_photo' placeholder='Photo'>"
                          +"</div>"

                          +"<div class='modal-footer'>"
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"  
                            +"<button type='submit' class='btn btn-default'>Ya</button>"
                          +"</div>"
                        +"</form>"
                    +"</div>";

        $("#Modals").html(buatList);
    },
    error: function (respons) {
      alert("Gagal memuat data.");
      
    }
  });
}





function ListorderTable(costumer_id,search,page){
 var url = window.location.toString();
        var halaman = url.split('/');
$.ajax({
    type:"GET",
    url: "order/checkstatus",
    dataType: "json",
    cache: false,
    success: function(respons){

     head = "";
     if(respons['data'][0]["type"] == "admin")
     {

        head +='<select id="issuer"  class=" selectpicker" >';
        head +='<option value="10" selected>Semua</option>';
        head +='</select>';
        head +='<form class="navbar-form navbar-right" method="GET" >';
        head +='<div class="box-tools">';
        head +='<div class="input-group input-group-sm" style="width: 150px;">';
        head +='<input id="search" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >';
        head +='</div>';
        head +='</form>';

    }else{


        head +='<a target="_blank" href="' + halaman[1] +'/order/printorder" class="pull-left ptr btn btn-danger">Print Riwayat</a>';

        head +='<form class="navbar-form navbar-right" method="GET" >';
        head +='<div class="box-tools">';
        head +='<div class="input-group input-group-sm" style="width: 150px;">';
        head +='<input id="searchOrder" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >';
        head +='</div>';
        head +='</form>';

         gettableorder(costumer_id,search,page);

    }
       


        $(".fheader").html(head); 
        
    },
    error: function (respons) {
      alert("Gagal memuat data.");
      
    }
  });

}


function gettableorder(costumer_id,search,page){

var history ="";
  history +='<thead>';
  history +='<th>Costumer</th>';
  history +='<th>Type</th>';
  history +='<th>Mer / Cob / Cos </th>';
  history +='<th>Bayar</th>';
  history +='<th>Fee</th>';
  history +='<th>Admin</th>';
  history +='<th>Total</th>';

  history +='</thead>';
    history +='<tbody id="DepOder">';
    
    history +='</tbody>';
    datatableorder();
    pagetabelorder();


$("#tableorder").html(history);


}



function datatableorder(costumer_id,search,page){

$("#DepOder").html('<tr><td colspan="11"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
  $.ajax({
      type:"GET",
      url: "order/history",
      dataType: "json",
      data: {costumer_id:costumer_id,search:search,page:page},
      success: function(respons){        
         
        
         jmlData = respons['data'].length;

              if(jmlData =="0"){

                     buatTabel = "<tr><td colspan='11'>Data Kosong</td></tr>";
                     buatPage = "";
                     buatFirst = "";
                     buatLast = "";
               }else{

                  buatTabel = "";
                  buatPage = "";
                  buatFirst = "";
                  buatLast = "";
               }



        
         
          for(a = 0; a < jmlData; a++)
          {

              
                
              
               var typemerchant = respons['data'][a]["type_merchant"];
               if(typemerchant =="merchant")
               {
                var status_merchant = "Transaksi";
                var fee = respons['data'][a]["issuertrx"];
                var feeadmin = respons['data'][a]["admintrx"];
                var jumlah = (fee - feeadmin);
                var Total = (respons['data'][a]["price"]-jumlah);
                 // var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Export</a>";
               }else{

                var status_merchant = "Transfer";
                var fee = respons['data'][a]["issuertrf"];
                var feeadmin = respons['data'][a]["admintrf"];
                var jumlah = (fee - feeadmin);
                var Total = (respons['data'][a]["price"]-jumlah);
                // var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Export</a>";
               }
              
            
             

            //mencetak baris baru
            buatTabel += "<tr>"
            
                        //membuat penomoran
                      
                        + "<td>" + respons['data'][a]["pengikut"] + " | " + respons['data'][a]["costumer"] + "  </td>"
                        + "<td>" + status_merchant + "</td>"
                        + "<td>" + respons['data'][a]["issuer"] + " |  "+ respons['data'][a]["merchant"] + "</td>"
                        + "<td>" + toRp(respons['data'][a]["price"]) + "</td>"
                         + "<td>" + toRp(fee) + "</td>"
                          + "<td>" + toRp(feeadmin) + "</td>"
                        + "<td>" + toRp(Total) + "</td>"
                     
            //tutup baris baru
                + "<tr/>";

          
               
        }

      $("#DepOder").html(buatTabel);

        
        

        if(respons['from'] != null){
           var last_page = respons['last_page']; 
           
              buatFirst += ""
                   + "<li><button  class='btn btn-default' onClick='GetpageOrder(1);'>«</button></li>"
                    + ""; 

             for(i = 1; i < last_page+1; i++)
             {
                    
                  if(respons['from'] ==1)
                  {
                    if([i] ==1)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    } 

                  }else if(page == respons['current_page']){

                    if([i] == page)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    }  
                 
                  }else{
                    var aktif = "";
                  }

                  

                     buatPage += ""
                   + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageOrder(" + [i] + ");'>" + [i] + "</button></li>"
                    + "";    
             } 

              buatLast += ""
             + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageOrder(" + respons['last_page']+");''>»</button></li>"
             + ""; 
            
         }

              

         $(".firstDep").html(buatFirst);
         $(".navigationDep").html(buatPage);
         $(".lastDep").html(buatLast);

      
      },
      error: function (respons) {
        alert("Gagal list Table Deposite ");
        
      }
    });



}


function pagetabelorder(){

   var page = "";
   page +='<li>';
     page +='<div class="firstDep"></div>';  
     page +='<div class="navigationDep"></div>';
     page +='<div class="lastDep"></div>';
    page +='</li>';


    $(".pagination").html(page);


}











	
 



function getprint(){
// $.ajax({
//     type:"GET",
//     url: "order/printorder",
//     dataType: "json",
//     cache: false,
//     success: function(respons){

// var hitungan= document.getElementById('DepOder');

//   var newWin=window.open('','Print-Window');

//   newWin.document.open();


//   print = "";
//   print  += '<div id="bungkus">';
//   print  += '<div class="headerprint">';
//   print  += '<div class="textdata">Alive Resto</div>';
//   print  += '<div class="textdata">Fusion Dining</div>';
//     print  += '<div class="textdata">Jalan Timoho No 49a </div>';
//     print  += '<div class="textdata">Yogyakarta (0274) </div>';
//     print  += '<div class="textdata">***** COPY RECEIPT ***** </div>';

//     // print  += '<div>Table  60 </div>';
//     // print  += '<div>Guest Ani </div>';
//     print  += '</div>';
//     print  += '</div>';

//   newWin.document.write('<html><body onload="window.print()">'+ print +'</body></html>');

//   newWin.document.close();

//   setTimeout(function(){newWin.close();},10);
     
        
//     },
//     error: function (respons) {
//       alert("Gagal memuat data.");
      
//     }
//   });





}