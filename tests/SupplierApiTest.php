<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SupplierApiTest extends TestCase
{
    use MakeSupplierTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSupplier()
    {
        $supplier = $this->fakeSupplierData();
        $this->json('POST', '/api/v1/supplier', $supplier);

        $this->assertApiResponse($supplier);
    }

    /**
     * @test
     */
    public function testReadSupplier()
    {
        $supplier = $this->makeSupplier();
        $this->json('GET', '/api/v1/supplier/'.$supplier->id);

        $this->assertApiResponse($supplier->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSupplier()
    {
        $supplier = $this->makeSupplier();
        $editedSupplier = $this->fakeSupplierData();

        $this->json('PUT', '/api/v1/supplier/'.$supplier->id, $editedSupplier);

        $this->assertApiResponse($editedSupplier);
    }

    /**
     * @test
     */
    public function testDeleteSupplier()
    {
        $supplier = $this->makeSupplier();
        $this->json('DELETE', '/api/v1/supplier/'.$supplier->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/supplier/'.$supplier->id);

        $this->assertResponseStatus(404);
    }
}
