<?php

use App\Models\SurveyDetail;
use App\Repositories\SurveyDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SurveyDetailRepositoryTest extends TestCase
{
    use MakeSurveyDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SurveyDetailRepository
     */
    protected $surveyDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->surveyDetailRepo = App::make(SurveyDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSurveyDetail()
    {
        $surveyDetail = $this->fakeSurveyDetailData();
        $createdSurveyDetail = $this->surveyDetailRepo->create($surveyDetail);
        $createdSurveyDetail = $createdSurveyDetail->toArray();
        $this->assertArrayHasKey('id', $createdSurveyDetail);
        $this->assertNotNull($createdSurveyDetail['id'], 'Created SurveyDetail must have id specified');
        $this->assertNotNull(SurveyDetail::find($createdSurveyDetail['id']), 'SurveyDetail with given id must be in DB');
        $this->assertModelData($surveyDetail, $createdSurveyDetail);
    }

    /**
     * @test read
     */
    public function testReadSurveyDetail()
    {
        $surveyDetail = $this->makeSurveyDetail();
        $dbSurveyDetail = $this->surveyDetailRepo->find($surveyDetail->id);
        $dbSurveyDetail = $dbSurveyDetail->toArray();
        $this->assertModelData($surveyDetail->toArray(), $dbSurveyDetail);
    }

    /**
     * @test update
     */
    public function testUpdateSurveyDetail()
    {
        $surveyDetail = $this->makeSurveyDetail();
        $fakeSurveyDetail = $this->fakeSurveyDetailData();
        $updatedSurveyDetail = $this->surveyDetailRepo->update($fakeSurveyDetail, $surveyDetail->id);
        $this->assertModelData($fakeSurveyDetail, $updatedSurveyDetail->toArray());
        $dbSurveyDetail = $this->surveyDetailRepo->find($surveyDetail->id);
        $this->assertModelData($fakeSurveyDetail, $dbSurveyDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSurveyDetail()
    {
        $surveyDetail = $this->makeSurveyDetail();
        $resp = $this->surveyDetailRepo->delete($surveyDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(SurveyDetail::find($surveyDetail->id), 'SurveyDetail should not exist in DB');
    }
}
