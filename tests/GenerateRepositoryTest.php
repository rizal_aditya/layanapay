<?php

use App\Models\Generate;
use App\Repositories\GenerateRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GenerateRepositoryTest extends TestCase
{
    use MakeGenerateTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GenerateRepository
     */
    protected $generateRepo;

    public function setUp()
    {
        parent::setUp();
        $this->generateRepo = App::make(GenerateRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGenerate()
    {
        $generate = $this->fakeGenerateData();
        $createdGenerate = $this->generateRepo->create($generate);
        $createdGenerate = $createdGenerate->toArray();
        $this->assertArrayHasKey('id', $createdGenerate);
        $this->assertNotNull($createdGenerate['id'], 'Created Generate must have id specified');
        $this->assertNotNull(Generate::find($createdGenerate['id']), 'Generate with given id must be in DB');
        $this->assertModelData($generate, $createdGenerate);
    }

    /**
     * @test read
     */
    public function testReadGenerate()
    {
        $generate = $this->makeGenerate();
        $dbGenerate = $this->generateRepo->find($generate->id);
        $dbGenerate = $dbGenerate->toArray();
        $this->assertModelData($generate->toArray(), $dbGenerate);
    }

    /**
     * @test update
     */
    public function testUpdateGenerate()
    {
        $generate = $this->makeGenerate();
        $fakeGenerate = $this->fakeGenerateData();
        $updatedGenerate = $this->generateRepo->update($fakeGenerate, $generate->id);
        $this->assertModelData($fakeGenerate, $updatedGenerate->toArray());
        $dbGenerate = $this->generateRepo->find($generate->id);
        $this->assertModelData($fakeGenerate, $dbGenerate->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGenerate()
    {
        $generate = $this->makeGenerate();
        $resp = $this->generateRepo->delete($generate->id);
        $this->assertTrue($resp);
        $this->assertNull(Generate::find($generate->id), 'Generate should not exist in DB');
    }
}
