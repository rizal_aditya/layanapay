<?php

use Faker\Factory as Faker;
use App\Models\Generate;
use App\Repositories\GenerateRepository;

trait MakeGenerateTrait
{
    /**
     * Create fake instance of Generate and save it in database
     *
     * @param array $generateFields
     * @return Generate
     */
    public function makeGenerate($generateFields = [])
    {
        /** @var GenerateRepository $generateRepo */
        $generateRepo = App::make(GenerateRepository::class);
        $theme = $this->fakeGenerateData($generateFields);
        return $generateRepo->create($theme);
    }

    /**
     * Get fake instance of Generate
     *
     * @param array $generateFields
     * @return Generate
     */
    public function fakeGenerate($generateFields = [])
    {
        return new Generate($this->fakeGenerateData($generateFields));
    }

    /**
     * Get fake data of Generate
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGenerateData($generateFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'transaction_id' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $generateFields);
    }
}
