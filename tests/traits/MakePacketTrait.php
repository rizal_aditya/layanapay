<?php

use Faker\Factory as Faker;
use App\Models\Packet;
use App\Repositories\PacketRepository;

trait MakePacketTrait
{
    /**
     * Create fake instance of Packet and save it in database
     *
     * @param array $packetFields
     * @return Packet
     */
    public function makePacket($packetFields = [])
    {
        /** @var PacketRepository $packetRepo */
        $packetRepo = App::make(PacketRepository::class);
        $theme = $this->fakePacketData($packetFields);
        return $packetRepo->create($theme);
    }

    /**
     * Get fake instance of Packet
     *
     * @param array $packetFields
     * @return Packet
     */
    public function fakePacket($packetFields = [])
    {
        return new Packet($this->fakePacketData($packetFields));
    }

    /**
     * Get fake data of Packet
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacketData($packetFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'denom' => $fake->randomDigitNotNull,
            'price' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $packetFields);
    }
}
