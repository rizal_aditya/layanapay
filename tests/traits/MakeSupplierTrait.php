<?php

use Faker\Factory as Faker;
use App\Models\Supplier;
use App\Repositories\SupplierRepository;

trait MakeSupplierTrait
{
    /**
     * Create fake instance of Supplier and save it in database
     *
     * @param array $supplierFields
     * @return Supplier
     */
    public function makeSupplier($supplierFields = [])
    {
        /** @var SupplierRepository $supplierRepo */
        $supplierRepo = App::make(SupplierRepository::class);
        $theme = $this->fakeSupplierData($supplierFields);
        return $supplierRepo->create($theme);
    }

    /**
     * Get fake instance of Supplier
     *
     * @param array $supplierFields
     * @return Supplier
     */
    public function fakeSupplier($supplierFields = [])
    {
        return new Supplier($this->fakeSupplierData($supplierFields));
    }

    /**
     * Get fake data of Supplier
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSupplierData($supplierFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'phone' => $fake->word,
            'address' => $fake->text,
            'description' => $fake->text,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $supplierFields);
    }
}
