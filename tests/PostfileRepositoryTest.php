<?php

use App\Models\Postfile;
use App\Repositories\PostfileRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostfileRepositoryTest extends TestCase
{
    use MakePostfileTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PostfileRepository
     */
    protected $postfileRepo;

    public function setUp()
    {
        parent::setUp();
        $this->postfileRepo = App::make(PostfileRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePostfile()
    {
        $postfile = $this->fakePostfileData();
        $createdPostfile = $this->postfileRepo->create($postfile);
        $createdPostfile = $createdPostfile->toArray();
        $this->assertArrayHasKey('id', $createdPostfile);
        $this->assertNotNull($createdPostfile['id'], 'Created Postfile must have id specified');
        $this->assertNotNull(Postfile::find($createdPostfile['id']), 'Postfile with given id must be in DB');
        $this->assertModelData($postfile, $createdPostfile);
    }

    /**
     * @test read
     */
    public function testReadPostfile()
    {
        $postfile = $this->makePostfile();
        $dbPostfile = $this->postfileRepo->find($postfile->id);
        $dbPostfile = $dbPostfile->toArray();
        $this->assertModelData($postfile->toArray(), $dbPostfile);
    }

    /**
     * @test update
     */
    public function testUpdatePostfile()
    {
        $postfile = $this->makePostfile();
        $fakePostfile = $this->fakePostfileData();
        $updatedPostfile = $this->postfileRepo->update($fakePostfile, $postfile->id);
        $this->assertModelData($fakePostfile, $updatedPostfile->toArray());
        $dbPostfile = $this->postfileRepo->find($postfile->id);
        $this->assertModelData($fakePostfile, $dbPostfile->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePostfile()
    {
        $postfile = $this->makePostfile();
        $resp = $this->postfileRepo->delete($postfile->id);
        $this->assertTrue($resp);
        $this->assertNull(Postfile::find($postfile->id), 'Postfile should not exist in DB');
    }
}
