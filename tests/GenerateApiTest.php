<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GenerateApiTest extends TestCase
{
    use MakeGenerateTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateGenerate()
    {
        $generate = $this->fakeGenerateData();
        $this->json('POST', '/api/v1/generate', $generate);

        $this->assertApiResponse($generate);
    }

    /**
     * @test
     */
    public function testReadGenerate()
    {
        $generate = $this->makeGenerate();
        $this->json('GET', '/api/v1/generate/'.$generate->id);

        $this->assertApiResponse($generate->toArray());
    }

    /**
     * @test
     */
    public function testUpdateGenerate()
    {
        $generate = $this->makeGenerate();
        $editedGenerate = $this->fakeGenerateData();

        $this->json('PUT', '/api/v1/generate/'.$generate->id, $editedGenerate);

        $this->assertApiResponse($editedGenerate);
    }

    /**
     * @test
     */
    public function testDeleteGenerate()
    {
        $generate = $this->makeGenerate();
        $this->json('DELETE', '/api/v1/generate/'.$generate->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/generate/'.$generate->id);

        $this->assertResponseStatus(404);
    }
}
