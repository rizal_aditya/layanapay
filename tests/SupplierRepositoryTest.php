<?php

use App\Models\Supplier;
use App\Repositories\SupplierRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SupplierRepositoryTest extends TestCase
{
    use MakeSupplierTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SupplierRepository
     */
    protected $supplierRepo;

    public function setUp()
    {
        parent::setUp();
        $this->supplierRepo = App::make(SupplierRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSupplier()
    {
        $supplier = $this->fakeSupplierData();
        $createdSupplier = $this->supplierRepo->create($supplier);
        $createdSupplier = $createdSupplier->toArray();
        $this->assertArrayHasKey('id', $createdSupplier);
        $this->assertNotNull($createdSupplier['id'], 'Created Supplier must have id specified');
        $this->assertNotNull(Supplier::find($createdSupplier['id']), 'Supplier with given id must be in DB');
        $this->assertModelData($supplier, $createdSupplier);
    }

    /**
     * @test read
     */
    public function testReadSupplier()
    {
        $supplier = $this->makeSupplier();
        $dbSupplier = $this->supplierRepo->find($supplier->id);
        $dbSupplier = $dbSupplier->toArray();
        $this->assertModelData($supplier->toArray(), $dbSupplier);
    }

    /**
     * @test update
     */
    public function testUpdateSupplier()
    {
        $supplier = $this->makeSupplier();
        $fakeSupplier = $this->fakeSupplierData();
        $updatedSupplier = $this->supplierRepo->update($fakeSupplier, $supplier->id);
        $this->assertModelData($fakeSupplier, $updatedSupplier->toArray());
        $dbSupplier = $this->supplierRepo->find($supplier->id);
        $this->assertModelData($fakeSupplier, $dbSupplier->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSupplier()
    {
        $supplier = $this->makeSupplier();
        $resp = $this->supplierRepo->delete($supplier->id);
        $this->assertTrue($resp);
        $this->assertNull(Supplier::find($supplier->id), 'Supplier should not exist in DB');
    }
}
