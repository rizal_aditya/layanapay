<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostfileApiTest extends TestCase
{
    use MakePostfileTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePostfile()
    {
        $postfile = $this->fakePostfileData();
        $this->json('POST', '/api/v1/postfile', $postfile);

        $this->assertApiResponse($postfile);
    }

    /**
     * @test
     */
    public function testReadPostfile()
    {
        $postfile = $this->makePostfile();
        $this->json('GET', '/api/v1/postfile/'.$postfile->id);

        $this->assertApiResponse($postfile->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePostfile()
    {
        $postfile = $this->makePostfile();
        $editedPostfile = $this->fakePostfileData();

        $this->json('PUT', '/api/v1/postfile/'.$postfile->id, $editedPostfile);

        $this->assertApiResponse($editedPostfile);
    }

    /**
     * @test
     */
    public function testDeletePostfile()
    {
        $postfile = $this->makePostfile();
        $this->json('DELETE', '/api/v1/postfile/'.$postfile->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/postfile/'.$postfile->id);

        $this->assertResponseStatus(404);
    }
}
