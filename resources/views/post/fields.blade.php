<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
</div>

@if(!empty($post->photo))
<div class="form-group col-sm-12 col-lg-12">
<img width="100" height="100" src="{!! $post->photo !!}">
</div>
@endif

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('publish', 'Publish:') !!}<br/>
    {{ Form::radio('publish', 1, isset($post) ? $post->publish == 1 : false) }} Yes<br>
    {{ Form::radio('publish', 0, isset($post) ? $post->publish == 0 : true) }} No
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('post.index') !!}" class="btn btn-default">Cancel</a>
</div>
