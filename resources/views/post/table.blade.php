<table class="table table-responsive" id="post-table">
    <thead>
        <th>Photo</th>
        <th>Title</th>
        <th>Description</th>
        <th>Publish</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($post as $post)
        <tr>
            <td><img width="50" height="50" src="{!! $post->photo !!}"></td>
            <td>{!! $post->title !!}</td>
            <td>{!! $post->description !!}</td>
            <td>@if($post->publish =="1") Publish  @else Proses Publish @endif</td>
            <td>@if($post->status =="1") Aktif  @else Proses Aktif @endif</td>
            <td>
                {!! Form::open(['route' => ['post.destroy', $post->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('post.show', [$post->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('post.edit', [$post->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>