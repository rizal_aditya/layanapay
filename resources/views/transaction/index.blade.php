@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Transaction <small>{{ $type }}</small></h1>
        <h1 class="pull-right">
           <!-- <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('transaction.create') !!}">Add New</a> -->
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                 <div class="box-header">
              <h3 class="box-title">Data {{ $type }} 

              @if($type =="deposite")

              <select id="StVerDep"  class=" selectpicker" >
                <option value="10" selected>Semua</option>
                <option value="0">Proses</option>
                <option value="1">Verifikasi</option>
              </select>

              @else

               <select id="StVerDraw"  class=" selectpicker" >
                <option value="10" selected>Semua</option>
                <option value="0">Proses</option>
                <option value="1">Verifikasi</option>
              </select>

              @endif

              </h3>



             <form class="navbar-form navbar-right" method="GET" >
  <div class="box-tools">
    <div class="input-group input-group-sm" style="width: 150px;">
         @if($type =="deposite")
              <input id="searchDep" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
         @else
              <input id="searchDraw" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
         @endif 
    </div>
  </div>
  </form>
        </div>          


               
                     @include('transaction.table')

              
                     

                    
            </div>
        </div>
    </div>
@endsection



<div class="modal fade" id="myModal" role="dialog" style="display: none;" >
     <div class="modal-dialog">  
        <div id="Modals" class="pull-left"></div>
      </div>
   
  </div>



 



