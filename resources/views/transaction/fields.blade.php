<!-- Qrcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qrcode', 'Qrcode:') !!}
    {!! Form::text('qrcode', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Costumer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('costumer_id', 'Costumer Id:') !!}
    {!! Form::number('costumer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transaction.index') !!}" class="btn btn-default">Cancel</a>
</div>
