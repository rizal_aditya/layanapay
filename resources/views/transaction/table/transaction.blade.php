<table class="table table-responsive" id="transaction-table">
    <thead>
        <th>Qrcode</th>
        <th>User Id</th>
        <th>Costumer Id</th>
        <th>Price</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($transaction as $transaction)
        <tr>
            <td>{!! $transaction->qrcode !!}</td>
            <td>{!! $transaction->user_id !!}</td>
            <td>{!! $transaction->costumer_id !!}</td>
            <td>{!! $transaction->price !!}</td>
            <td>{!! $transaction->status !!}</td>
            <td>
                {!! Form::open(['route' => ['transaction.destroy', $transaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transaction.show', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('transaction.edit', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>