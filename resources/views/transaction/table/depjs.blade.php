
 @if(empty($transaction))
                    
<tr>
<td colspan="11">
<b>Data Masih Kosong.</b>
</td>
</tr>

@else
                    


    @foreach($transaction as $transaction)

            <?php   $bank_costumer = DB::table('bank')->where('id',$transaction->bank_id)->first();  ?>
             <?php   $bank_admin = DB::table('bank')->where('id',$transaction->bank_send)->first();  ?>
        <tr>
            <td><img width="50" height="50" src="{!! empty($transaction->user->photo) ? config('app.photo_default') : url($transaction->user->photo)  !!} "></td>
            <td>{!! $transaction->name !!}</td>
            <td>{!! $transaction->packet !!}</td>
           
            <td>Rp {!! number_format($transaction->saldo,0,'','.') !!},00</td>
            <td>{!! $bank_costumer->name !!} | {!! $bank_costumer->account_name !!}</td>

            <td>{!! $bank_admin->name !!} | {!! $bank_admin->account_name !!}</td>
            
            <td>Rp {!! number_format($transaction->price,0,'','.') !!},00</td>
           <td>

            <?php 
              $date = $transaction->created_at;
              $mydate = date("Y-m-d H:i:s");
              $datetime1 = date_create($date);
              $datetime2 = date_create($mydate);
              $interval = date_diff($datetime1, $datetime2);
               $min=$interval->format('%i');
              $sec=$interval->format('%s');
              $hour=$interval->format('%h');
              $mon=$interval->format('%m');
              $day=$interval->format('%d');
              $week = date("Y-m-d");
              $year=$interval->format('%y');
              if($interval->format('%i%h%d%m%y')=="00000")
              {
                echo $sec." detik yang lalu";
              }else if($interval->format('%h%d%m%y')=="0000"){
                echo $min." menit yang lalu";
              }else if($interval->format('%d%m%y')=="000"){
                echo $hour." jam yang lalu";   
              }else if($interval->format('%m%y')=="00"){
                echo $day." hari yang lalu";
              }else if($interval->format('%y')=="0"){
                echo $mon." bulan yang lalu";
              }else{
                echo $year." tahun yang lalu";
              }

             ?></td>
           <td>

           <?php 

           $tgl = $transaction->tanggal;

            $bulan  = array("Jan"
            , "Feb"
            , "Mar"
            , "Apr"
            , "Mei"
            , "Jun"
            , "Jul"
            , "Agu"
            , "Sep"
            , "Okt"
            , "Nov"
            , "Des");

            $hari = array("Senin"
            , "Selasa"
            , "Rabu"
            , "Kamis"
            , "Jum'at"
            , "Sabtu"
            , "Minggu");

    $tahun_split  = substr($tgl, 0, 4);
    $bulan_split  = substr($tgl, 5, 2);
    $hari_split   = substr($tgl, 8, 2);
    $tmpstamp   = mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
    $bulan_jadi   = $bulan[date("n", $tmpstamp)-1];
    echo $hari_split." ".$bulan_jadi." ".$tahun_split;

           ?>
            </td>

            
             <td>
                @if($transaction->status>0)
                    
                 <div  class="skus" ><i class="fa fa-check" aria-hidden="true"></i> Sukses</div>
                    
                @else
                  <div class="blok" ><i class="fa fa-bookmark" aria-hidden="true"></i> Proses </div>    
                @endif
            </td>
            <td>

            <div class="btn-group">
           
              <a class="btn btn-default btn-xs"  style="cursor: pointer;" data-toggle="modal" onclick="GetDetail({!! $transaction->id !!});" data-target="#myModal"><i class="fa fa-eye"></i> Detail</a>


              <a class="btn btn-default btn-small ismal dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>

              <ul class="dropdown-menu">
                 @if($transaction->status>0)
                        <li><a id="Block" style="cursor: pointer;" data-toggle="modal" onclick="GetBlokir({!! $transaction->id !!});" data-target="#myModal"><i class="fa fa-user-times"></i>   Blokir</a></li>
                  @else   

                    <li> <a id="Devp"  style="cursor: pointer;" data-toggle="modal" onclick="GetName({!! $transaction->id !!});" data-target="#myModal"><i class="fa fa-money" aria-hidden="true"></i> Terima</a></li>

                @endif

                <li> <a id="Devp"  style="cursor: pointer;" data-toggle="modal" onclick="GetDestroy({!! $transaction->id !!});" data-target="#myModal"><i class="fa fa-trash"></i> Hapus</a></li>

              
              </ul>
            </div><!-- /btn-group --> 


            

               
            </td>
        </tr>


 

        
    @endforeach
    <tr>
     <td colspan="11">
      <ul class="navigation"> 
        
  
        </ul>
        </td> 
      </tr>
  @endif
  






