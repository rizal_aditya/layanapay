@if($type == "transaction")
    @include('transaction.table.transaction')
@elseif($type =="deposite")
    @include('transaction.table.deposite')
@elseif($type =="withdraw")
    @include('transaction.table.withdraw')
@endif

