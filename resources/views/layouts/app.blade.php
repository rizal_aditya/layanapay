<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{!! config('app.title') !!}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
   <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/4.5.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/font-awesome.css') }}" media="screen">
    <link rel="stylesheet" href="{{ asset('/ionicons/2.0.1/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/timepicker/bootstrap-timepicker.css') }}">
    <link href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/colorbox.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('/css/ui.tabs.css') }}" rel="stylesheet"> 

     <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/js/jQuery-2.2.0.min.js') }}"></script>
     <script src="{{ asset('/js/script.js') }}"></script>
    

    
    <!-- <script src="{{ asset('/js/ui.core.js') }}" type="text/javascript"></script>
   <script src="{{ asset('/js/ui.tabs.js') }}" type="text/javascript"></script> -->
   
   
  
    
</head>

<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">
       
            <!-- Logo -->
            <a href="{{ url('home') }}" class="logo">
                
                <span class="logo-mini"> <!-- <img width="50" height="50"  src="{!! Auth::user()->photo == '' ? config('app.photo_default') : url(Auth::user()->photo)  !!}"
                                     class="user-image"  alt="User Image"/></span> -->
                <span class="logo-lg"></span>
            </a>



            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li  class="topsaldo dropdown messages-menu">
                       
                         
                        
                        </li> 

                    <!-- DEPOSIT -->
                        <!-- Messages: style can be found in dropdown.less-->

                 <!--   <li class="dropdown messages-menu">
                        <a id="Notifdeposit" class="dropdown-toggle" data-toggle="dropdown"></a>
                        <ul class="dropdown-menu">
                                <li  class="header">Pemberitahuan Tambah Saldo</li>
                                <li>
                                    <ul  class="listdeposit menu">
                                    </ul>
                                </li>
                                 <li class="footer"><a href="{!! url('transaction?type=deposite') !!}" target="_top">Lihat Data </a></li>
                        </ul>
                   </li>  -->


                       <!-- withdraw -->
                        <!-- Messages: style can be found in dropdown.less-->
                       <!--  <li class='dropdown messages-menu'>
                        <a id='Notifwithdraw' class='dropdown-toggle' data-toggle='dropdown'></a>
                            <ul class="dropdown-menu">
                                    <li  class="header">Pemberitahuan Tarik Saldo</li>
                                    <li>
                                        <ul  class="listwithdraw menu">
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="{!! url('transaction?type=withdraw') !!}" target="_top">Lihat Data </a></li>
                            </ul>
                         </li>  -->



                          <!-- user -->
                        <!-- Messages: style can be found in dropdown.less-->
                      <!--   <li class='dropdown messages-menu'>
                        <a id='Notifuser' class='dropdown-toggle' data-toggle='dropdown'></a>
                            <ul class="dropdown-menu">
                                    <li  class="header">Pemberitahuan Verifikasi </li>
                                    <li>
                                        <ul  class="listuser menu">
                                        </ul>
                                    </li>
                                     <li class="footer"><a href="{!! url('user?type=costumer') !!}" target="_top">Lihat Data </a></li>
                            </ul>
                         </li>  -->

                    <!-- deposite -->

                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{!! Auth::user()->photo == '' ? config('app.photo_default') : url(Auth::user()->photo)  !!}"
                                     class="user-image"  alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">
                                      @if (Auth::guest())
                                        {!! config('app.title') !!}
                                    @else
                                    {!! Auth::user()->name !!}
                                    @endif
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="{!! Auth::user()->photo == '' ? config('app.photo_default') : url(Auth::user()->photo)  !!}" 
                                    class="img-header" alt="User Image"/>
                                    <p>
                                        @if (Auth::guest())
                                            {!! config('app.title') !!}
                                        @else
                                            {!! Auth::user()->name !!}
                                        @endif
                                        <small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>

                                            <?php $id = Auth::user()->id;?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{!! url('admin') !!}" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © {!! config('app.copyright_date') !!} <a href="{!! config('app.company_link') !!}" target="_blank">{!! config('app.company_name') !!}</a>.</strong> All rights reserved.
        </footer>

    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{!! url('/') !!}">
                    {!! config('app.title') !!}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{!! url('/home') !!}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{!! url('/login') !!}">Login</a></li>
                        <li><a href="{!! url('/register') !!}">Register</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @endif
    
     <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('/js/icheck.min.js') }}"></script>
   
    <!-- AdminLTE App -->
    <script src="{{ asset('/js/app.min.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.min.js') }}"></script>
    <script  src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript">
        $('.date').datepicker({
          autoclose: true
        });
        $("textarea").wysihtml5({
            toolbar: {
                "link": false,
                "image": false
            }
        });
    </script>

        <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
  });


</script>
<script type="text/javascript">
$(function () {   
    //Timepicker
    $(".timepicker").timepicker({
        showMeridian:false,
      showInputs: false
    });
   
  });
</script>

    @yield('scripts')
</body>
</html>