<table class="table table-responsive" id="postfile-table">
    <thead>
        <th>Post Id</th>
        <th>Branch Id</th>
        <th>Type</th>
        <th>Name</th>
        <th>Content</th>
        <th>File</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($postfile as $postfile)
        <tr>
            <td>{!! $postfile->post_id !!}</td>
            <td>{!! $postfile->branch_id !!}</td>
            <td>{!! $postfile->type !!}</td>
            <td>{!! $postfile->name !!}</td>
            <td>{!! $postfile->content !!}</td>
            <td>{!! $postfile->file !!}</td>
            <td>{!! $postfile->status !!}</td>
            <td>
                {!! Form::open(['route' => ['postfile.destroy', $postfile->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('postfile.show', [$postfile->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('postfile.edit', [$postfile->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>