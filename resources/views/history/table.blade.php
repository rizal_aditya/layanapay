<table class="table table-responsive" id="history-table">
    <thead>
       
        <th>Kode Transaksi</th>
        <th>Nama</th>
        <th>Bank</th>
        <th>Jumlah</th>
        <th>Keterangan</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($history as $history)
        <tr>
            <?php  
                $bank = DB::table('bank')->where('id',$history->bank_id)->first()->name;
                if(!empty($bank))
                {
                    $data = $bank;
                }else{
                    $data = "";
                }

             ?>
            <td>{!! $history->transaction_id !!}</td>
            <td>{!! $history->user->name !!}</td>
            <td>{!! $data !!}</td>
            <td>Rp  {!!  number_format($history->price,0,',','.')  !!}</td>
            <td>{!! $history->description !!}</td>
            <td>
                  @if($history->status =="0")
                    Proses
                  @else
                    Sukses
                  @endif  
            </td>
            <td>
                {!! Form::open(['route' => ['history.destroy', $history->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('history.show', [$history->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('history.edit', [$history->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>