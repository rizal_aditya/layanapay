@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Riwayat Transaksi</h1>
       
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @if($history->isEmpty())
                    <div class="text-center">No Artis found.</div>
                    @else
                      @include('history.table')

                    {{ $history->links() }}
                
                    @endif

                   
            </div>
        </div>
    </div>
@endsection

