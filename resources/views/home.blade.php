@extends('layouts.app')

@section('content')

<section class="content">
      <!-- Info boxes -->
      <div id="bankSaldo" class="row">
      </div>
      <!-- /.row -->

      

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          
  

          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Terima & Tarik Saldo</h3>

              <div class="box-tools pull-right">
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Type</th>
                    <th>Transaksi</th>
                    <th>Jumlah</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody id="lates">
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
             
            </div>
            <!-- /.box-footer -->
          </div>


          <!-- /.box -->
        </div>
        <!-- /.col -->

            <div class="col-md-4">
              <div id="dealadmin" class="info-box bg-red"></div>
              <!-- Info Boxes Style 2 -->
              <div id='provite' class="info-box bg-yellow"></div>
              <!-- /.info-box -->
              <div id='totmember' class="info-box bg-green"></div>
              <!-- /.info-box -->
              
              <!-- /.info-box -->
              <div id="totmerchant" class="info-box bg-aqua"></div>

         

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>	





<div class="modal fade" id="myModal" role="dialog" style="display: none;" >
     <div class="modal-dialog">  
        <div id="Modals" class="pull-left"></div>
      </div>
   
  </div>

<script src="{{ asset('/js/dasboard.js') }}"></script>

@endsection
