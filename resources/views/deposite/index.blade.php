@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Issuer
                <small>{{ $type }}</small>
        </h1>

        <h1 class="pull-right">
         
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
              <div class="box-header">
                <h3 class="box-title">Data {{ $type }} 

                  <select id="Stdeposit"  class=" selectpicker" >
                    <option value="all" selected>Semua</option>
                    <option value="not">Belum Konfirmasi</option>
                    <option value="suc">Sudah Konfirmasi</option>
                  </select>

                  </h3>
              <form class="navbar-form navbar-right" method="GET" >
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  
                  <input id="searchCDepIs" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
                 
                </div>
              </div>
              </form>

            </div>              
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dynamic-table" >
                      <thead>
                          <th>Issuer</th>
                          <th>Nominal</th>
                          <th>Methode</th>
                          <th>Tanggal</th>
                          <th>Konfirmasi</th>
                          <th>Status</th>
                          <th colspan="3">Action</th>
                      </thead>
                       <tbody id="DepisTable">
                      
                      </tbody>
                  </table>

                  <ul class="pagination">
                       <div class="firstDep"></div>   
                       <div class="navigationDep"></div>
                       <div class="lastDep"></div>
                      
                      </li>
                  </ul>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
  @include('deposite.script')
@endsection


<div class="modal fade" id="myModal" role="dialog"  >
     <div class="modal-dialog">  
        <div id="Modals" class="pull-left"></div>
      </div>

  </div>
