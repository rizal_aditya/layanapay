<script>
	$(document).ready(function(){
		var st = 'all';
		var sea = '';
		var page2 = '';

		var type = '{{ $type }}';
		console.log(type);

		if(type == 'History')
		{
			ListTableHistoryTrans(st, sea, page2);
		}
		else if(type == 'Deposit')
		{
			ListTableDeposit(st, sea, page2);
		}

		$("#Stdeposit").change(function(){
			var data = $(this).val();
			// alert(data);
			ListTableDeposit(data, sea, page2);
		});

		$("#searchCDepIs").on('keyup keypress', function(){
			var data = $("#Stdeposit").val();
			var search = $(this).val();
			var page = '';

			ListTableDeposit(data, search, page);
		});

	});

	function ListTableDeposit(status2, search2, page2)
	{
		$("#DepisTable").html('<tr><td colspan="7"><div class="loading"><img src="{{ url("css/images/loading.gif") }}" /><br />Loading...</div></td></tr>');
		$.ajax({
			type:"GET",
		    url: "{{ url('issuer/lisdeposit') }}",
		    data: {status:status2,search:search2,page:page2},
		    dataType: "json",
		    cache: false,
		    success: function(data){
		    	jmlData = data['data'].length;

              	if(jmlData =="0"){
                    buatTabel = "<tr><td colspan='10'>Data Kosong</td></tr>";
                    buatPage = "";
                    buatFirst = "";
                    buatLast = "";
               	}else{
                  	buatTabel = "";
                  	buatPage = "";
                  	buatFirst = "";
                  	buatLast = "";
               	}       
		         
	          	for(a = 0; a < jmlData; a++)
	          	{    
		            var url = window.location.toString();
		            var img = "/img/avatar.png";
		            var photo =  url.img;
		            var uimg = "50";
		            if(photo != null || photo !='')
		            {
		                photo = data['data'][a]["transfer_photo"];
		            } 

		            var st = data['data'][a]["status"];
		            if(st =="0")
		            {
		              var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Proses</div>";
		              var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-money' aria-hidden='true'></i> Terima</a>";
		            }else if(st =="2"){
		              var status = "<div class='blok' ><i class='fa fa-bookmark' aria-hidden='true'></i> Blokir</div>";
		              var konfirmasi ="<a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetBataluser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-recycle' aria-hidden='true'></i> Batalkan</a>";
		            }else{
		              var status = "<div  class='skus' ><i class='fa fa-check' aria-hidden='true'></i> Sukses</div>";
		              var konfirmasi = "<a id='Block' style='cursor: pointer;'' data-toggle='modal' onclick='GetBlokirUser(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-user-times'></i> Blokir</a>";
		            }
		              //mencetak baris baru
		            buatTabel += "<tr>"
		          
		                      //membuat penomoran
		                      + "<td><div class='nametbl'> " + data['data'][a]["name"] + "</div></td>"
		                      + "<td>" + toRp(data['data'][a]["price"]) + "</td>"
		                      + "<td>" + data['data'][a]["type"] + "</td>"
		                      + "<td>" + time_ago(data['data'][a]["created_at"]) + "</td>"
		                      + "<td><img width='" + uimg + "' height='" + uimg + "' src='" + photo + "'></td>"
		                      + "<td>" + status + "</td>"
		                      + "<td>"
		                      + "<div class='btn-group'>"
		                      + "<a class='btn btn-default btn-xs' onclick='GetDetailDep(" + data['data'][a]["id"] + ");'  style='cursor: pointer;' data-toggle='modal'  data-target='#myModal'><i class='fa fa-check'></i> Konfirmasi</a>"
		                      // + "<a class='btn btn-default btn-small ismal dropdown-toggle' data-toggle='dropdown' href='#'>"
		                      // + "<span class='caret'></span></a>"
		                        // + "<ul class='dropdown-menu drop'>" 
		                        //   // + "<li>" + konfirmasi + "</li>"
		                        //   + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetDetailcobrand(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit</a></li>"
		                        //   + "<li><a id='Devp'  style='cursor: pointer;'' data-toggle='modal' onclick='GetKonfirmasiCoBrand(" + data['data'][a]["id"] + ");' data-target='#myModal'><i class='fa fa-times' aria-hidden='true'></i> Nonaktifkan</a></li>"
		                        // + "</ul>" 
		                      + "</div>"
		                      + "</td>"
		          	//tutup baris baru
		              + "<tr/>";         
		        }
		     	$("#DepisTable").html(buatTabel);
		      	console.log(data);  

		      	if(data['from'] != null){
		           var last_page = data['last_page']; 
		           
		              buatFirst += ""
		                   + "<li><button  class='btn btn-default' onClick='GetpageDep(1);'>«</button></li>"
		                    + ""; 

		             for(i = 1; i < last_page+1; i++)
		             {
		                    
		                  if(data['from'] ==1)
		                  {
		                    if([i] ==1)
		                    {
		                      var aktif = "active";
		                    }else{
		                      var aktif = "";
		                    } 

		                  }else if(page == data['current_page']){

		                    if([i] == page)
		                    {
		                      var aktif = "active";
		                    }else{
		                      var aktif = "";
		                    }  
		                 
		                  }else{
		                    var aktif = "";
		                  }
		                     buatPage += ""
		                   + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageDep(" + [i] + ");'>" + [i] + "</button></li>"
		                    + "";    
		             } 
		              buatLast += ""
		             + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageDep(" + data['last_page']+");''>»</button></li>"
		             + "";   
		         }
		         $(".firstDep").html(buatFirst);
		         $(".navigationDep").html(buatPage);
		         $(".lastDep").html(buatLast);  
		    },
		    error:function (data) {
		    	alert("Gagal memuat data.");
		    }
		});
	}

	function ListTableHistoryTrans(status2, search2, page2)
	{
		$("#HistTable").html('<tr><td colspan="7"><div class="loading"><img src="{{ url("css/images/loading.gif") }}" /><br />Loading...</div></td></tr>');
		$.ajax({
			type:"GET",
		    url: "{{ url('issuer/listhistorytrans') }}",
		    data: {status:status2,search:search2,page:page2},
		    dataType: "json",
		    cache: false,
		    success: function(data){
		    	jmlData = data['data'].length;

              	if(jmlData =="0"){
                    buatTabel = "<tr><td colspan='10'>Data Kosong</td></tr>";
                    buatPage = "";
                    buatFirst = "";
                    buatLast = "";
               	}else{
                  	buatTabel = "";
                  	buatPage = "";
                  	buatFirst = "";
                  	buatLast = "";
               	}       
		         
	          	for(a = 0; a < jmlData; a++)
	          	{    
		            var url = window.location.toString();
		            var img = "/img/avatar.png";
		            var photo =  url.img;
		            var uimg = "50";
		            if(photo != null || photo !='')
		            {
		                photo = data['data'][a]["transfer_photo"];
		            } 

		              //mencetak baris baru
		            buatTabel += "<tr>"
		                      //membuat penomoran
		                      + "<td><div class='nametbl'> " + data['data'][a]["pengirim"] + "</div></td>"
		                      + "<td>" + data['data'][a]["tujuan"] + "</td>"
		                      + "<td>" + toRp(data['data'][a]["price"]) + "</td>"
		                      + "<td>" + time_ago(data['data'][a]["tanggal"]) + "</td>"
		                      + "<td>" + data['data'][a]['type'] + "</td>"
		                      + "<td><a class='btn btn-default btn-xs' onclick='GetDetailTrans(" + data['data'][a]["id"] + ");'  style='cursor: pointer;' data-toggle='modal'  data-target='#myModal'><i class='fa fa-search-plus'></i> </a></td>"
		          	//tutup baris baru
		              + "<tr/>";         
		        }
		     	$("#HistTable").html(buatTabel);
		      	console.log(data);  

		      	if(data['from'] != null){
		           var last_page = data['last_page']; 
		           
		              buatFirst += ""
		                   + "<li><button  class='btn btn-default' onClick='GetpageHis(1);'>«</button></li>"
		                    + ""; 

		             for(i = 1; i < last_page+1; i++)
		             {
		                    
		                  if(data['from'] ==1)
		                  {
		                    if([i] ==1)
		                    {
		                      var aktif = "active";
		                    }else{
		                      var aktif = "";
		                    } 

		                  }else if(page == data['current_page']){

		                    if([i] == page)
		                    {
		                      var aktif = "active";
		                    }else{
		                      var aktif = "";
		                    }  
		                 
		                  }else{
		                    var aktif = "";
		                  }
		                     buatPage += ""
		                   + "<li><button  class='"+ aktif + " btn btn-default' onClick='GetpageHis(" + [i] + ");'>" + [i] + "</button></li>"
		                    + "";    
		             } 
		              buatLast += ""
		             + "<li><button id='nextPage' class='btn btn-default' onClick='GetpageHis(" + data['last_page']+");''>»</button></li>"
		             + "";   
		         }
		         $(".firstDep").html(buatFirst);
		         $(".navigationDep").html(buatPage);
		         $(".lastDep").html(buatLast);  
		    },
		    error:function (data) {
		    	alert("Gagal memuat data.");
		    }
		});
	}

	function GetpageDep(page){
	  	var pages = page;
	  	var status = $('#Stdeposit').val();
	  	var search = $('#searchCDepIs').val();
	 
	  	if(pages ==null | pages ==""){
	   
	  	}else{
	      	//alert(status);
	    	ListTableDeposit(status, search, pages)
	   
	  	}
	}

	function GetpageHis(page){
	  	var pages = page;
	  	var status = $('#Sthistory').val();
	  	var search = $('#searchCHist').val();
	 
	  	if(pages ==null | pages ==""){
	   
	  	}else{
	      	//alert(status);
	    	ListTableHistoryTrans(status, search, pages)
	   
	  	}
	}

	function konfirm_dep(id, saldo, user)
	{
		var token = $('meta[name="csrf-token"]').attr('content');

		var status = $('#Stdeposit').val();
	  	var search = $('#searchCDepIs').val();

		$.ajax({
			type:"POST",
		    url: "{{ url('issuer/konfirDep') }}",
		    data: {_token:token, id:id, saldo:saldo, user_id:user},
		    cache: false,
		    success: function(respons){
		    	ListTableDeposit(status, search, '');
		    },
		    error:function (respons) {}
		});
	}

	function GetDetailDep(id)
	{
		$("#Modals").html('<div class="modal-body"><div class="loading"><img src="{{ url("css/images/loading.gif") }}" /><br />Loading...</div></div>');
		$.ajax({
			type:"GET",
		    url: "{{ url('issuer/detailDep') }}",
		    data: {id:id},
		    dataType: "json",
		    cache: false,
		    success: function(respons){
		    	var url = window.location.toString();
               	var img = "/img/avatar.png";
               	var photo =  url+''+img;
               	var uimg = "150";

               	if(respons[0]["transfer_photo"] != ''){
                  	photo = respons[0]["transfer_photo"];
               	} 

               	if(respons[0]["status"] =="0")
               	{
                	var verified = "<div class='njh'>"+ respons[0]['name'] +"</div> <div  class='rdef' ><i class='fa fa-user-times' aria-hidden='true'></i> Belum dikonfirmasi</div>"; 
               	}else{
                	var verified = "<div class='njh'>"+ respons[0]['name'] +"</div> <div  class='rde' ><i class='fa fa-check' aria-hidden='true'></i> Terkonfirmasi</div>";
               	} 
               	if(respons[0]['transfer_photo'] != null){
               		var konfirmasi = "<input id='konfirm_dep' type='button' class='btn btn-default' value='Konfirmasi' onclick='konfirm_dep("+respons[0]['id']+","+respons[0]['price']+","+respons[0]['user_id']+")' data-dismiss='modal'>";
               	}else{
               		var konfirmasi = "";
               	}
               	var bank = acc = acc_name = '';
               	if(typeof respons[0]['bank'] == 'undefined') { bank = '-'; } else { bank = respons[0]['bank']; }
               	if(typeof respons[0]['account_number'] == 'undefined') { acc = '-'; } else { acc = respons[0]['account_number']; }
               	if(typeof respons[0]['account_name'] == 'undefined') { acc_name =  '-'; } else { acc_name = respons[0]['account_name']; }

        		buatList = "";
           		buatList  += "<div class='pull-left modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Detail Deposit</h4>"
                          +"</div>"

                          +"<div class='pull-left modal-body'>"
                               +"<div class='form-group col-sm-6'><img style='max-width: 250;' src='"+ photo +"'></div>"
                               +"<div class='form-group col-sm-6 '><label class='pull-left vfgr' for='name'>Nama:</label> "+ verified +"</div>"  
                                 +"<div class='form-group col-sm-6'><label for='email'>Method:</label><p>"+ respons[0]['type'] +"</p></div>"
                               +"<div class='form-group col-sm-6'><label for='saldo'>Deposit:</label><p>"+ toRp(respons[0]['price']) +"</p></div>" 
                               +"<div class='form-group col-sm-6'><label for='saldo'>Total Transfer:</label><p>"+ toRp(respons[0]['transfer_total']) +"</p></div>" 
                               +"<div class='form-group col-sm-6'><label for='no_rekening'>No Rekening:</label><p>"+ acc +"</p></div>" 
                               +"<div class='form-group col-sm-6'><label for='atas_nama'>Atas Nama:</label><p>"+ acc_name +"</p></div>"       
                          +"</div>"

                          +"<div class='pull-left modal-footer'>"
                          	+konfirmasi
                            +"<input id='cancely' type='button' value='Batal' class='btn btn-default' data-dismiss='modal'>"
                          +"</div>"
                      +"</div>";

        		$("#Modals").html(buatList); 
		    },
		    error:function (respons) {}
		});
	}

	function GetDetailTrans(id)
	{
		$.ajax({
			type:"GET",
		    url: "{{ url('issuer/detailhistorytrans') }}",
		    data: {id:id},
		    dataType: "json",
		    cache: false,
		    success: function(respons){
		    	var verified = '-';
		    	var photo = '';
		    	buatList = "";
           		buatList  += "<div class='pull-left modal-content'>"
                          +"<div class='modal-header'>"
                              +"<button type='button' class='close' data-dismiss='modal'>&times;</button>"
                          +"<h4 class='modal-title'>Detail </h4>"
                          +"</div>"

                          +"<div class='pull-left modal-body'>"
                               +"<div class='form-group col-sm-6'><img width='100' height='100' src=''></div>"  
                              +"<div class='form-group col-sm-6'><label for='email'>No Identitas:</label><p></p></div>"
                               +"<div class='form-group col-sm-6 '><label class='pull-left vfgr' for='name'>Nama:</label></div>"  
                                 +"<div class='form-group col-sm-6'><label for='email'>Email:</label><p></p></div>"
                                +"<div class='form-group col-sm-6'><label for='bank'>Bank:</label><p></p></div>"
                               +"<div class='form-group col-sm-6'><label for='saldo'>Saldo:</label><p></p></div>" 
                               +"<div class='form-group col-sm-6'><label for='no_rekening'>No Rekening:</label><p></p></div>" 
                               +"<div class='form-group col-sm-6'><label for='atas_nama'>Atas Nama:</label><p></p></div>"    
                          +"</div>"

                          +"<div class='pull-left modal-footer'>"
                            +"<input id='cancely' type='button' value='Keluar' class='btn btn-default' data-dismiss='modal'>"  
                           
                          +"</div>"
                      +"</div>";

        		$("#Modals").html(buatList); 
		    },
		    error:function (respons) {

		    }
		});
	}

</script>