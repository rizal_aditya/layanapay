<table class="table table-responsive" id="packet-table">
    <thead>
        <th>Name</th>
        <th>Denom</th>
        <th>Price</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($packet as $packet)
        <tr>
            <td>{!! $packet->name !!}</td>
            <td>{!! $packet->denom !!}</td>
            <td>{!! $packet->price !!}</td>
            <td>{!! $packet->status !!}</td>
            <td>
                {!! Form::open(['route' => ['packet.destroy', $packet->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('packet.show', [$packet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('packet.edit', [$packet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>