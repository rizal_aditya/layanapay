<table class="table table-responsive" id="generate-table">
    <thead>
        <th>Code</th>
        <th>User Id</th>
        <th>Transaction Id</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($generate as $generate)
        <tr>
            <td>{!! $generate->code !!}</td>
            <td>{!! $generate->user_id !!}</td>
            <td>{!! $generate->transaction_id !!}</td>
            <td>{!! $generate->status !!}</td>
            <td>
                {!! Form::open(['route' => ['generate.destroy', $generate->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('generate.show', [$generate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('generate.edit', [$generate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>