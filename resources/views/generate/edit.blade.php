@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Generate
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($generate, ['route' => ['generate.update', $generate->id], 'method' => 'patch']) !!}

                        @include('generate.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection