
<div class="pull-left form-group col-sm-4">
    <h3>{!! Form::label('deposite', 'Deposite') !!}</h3>
        <h4><b>Non KYC</b></h4>
        <p>Minimal  : {!! Form::text('mindepn', null, ['class' => 'form-control','placeholder' => 'Minimal']) !!} </p>
        <p>Maksimal : {!! Form::text('maxdepn', null, ['class' => 'form-control','placeholder' => 'Maksimal']) !!} </p>


        <h4><b>KYC</b></h4>
        <p>Minimal : {!! Form::text('mindepk', null, ['class' => 'form-control','placeholder' => 'Minimal']) !!}</p>
        <p>Maksimal : {!! Form::text('maxdepk', null, ['class' => 'form-control','placeholder' => 'Maksimal']) !!}</p>
</div>

<div class="pull-left form-group col-sm-4">
    <h3>{!! Form::label('transaksi', 'Transaksi') !!}</h3>
        <h4><b>Non KYC</b></h4>
        <p>Minimal : {!! Form::text('mintrn', null, ['class' => 'form-control','placeholder' => 'Minimal']) !!} </p>
        <p>Maksimal : {!! Form::text('maxtrn', null, ['class' => 'form-control','placeholder' => 'Maksimal']) !!} </p>


        <h4><b>KYC</b></h4>
        <p>Minimal :{!! Form::text('mintrk', null, ['class' => 'form-control','placeholder' => 'Minimal']) !!}</p>
        <p>Maksimal : {!! Form::text('maxtrk', null, ['class' => 'form-control','placeholder' => 'Maksimal']) !!}</p>
</div>


<div class="pull-left form-group col-sm-4">
    <h3>{!! Form::label('withdraw', 'Withdraw') !!}</h3>
        <h4><b>Non KYC</b></h4>
        <p>Minimal : {!! Form::text('minwdn', null, ['class' => 'form-control','placeholder' => 'Minimal']) !!} </p>
        <p>Maksimal : {!! Form::text('maxwdn', null, ['class' => 'form-control','placeholder' => 'Maksimal']) !!} </p>


        <h4><b>KYC</b></h4>
        <p>Minimal : {!! Form::text('minwdk', null, ['class' => 'form-control','placeholder' => 'Minimal']) !!}</p>
        <p>Maksimal : {!! Form::text('maxwdk', null, ['class' => 'form-control','placeholder' => 'Maksimal']) !!}</p>
</div>



<div class="pull-left form-group col-sm-12">
 {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
  <a href="{!! url('setting?type=nominal') !!}" class="btn btn-default">Back</a>
</div>
