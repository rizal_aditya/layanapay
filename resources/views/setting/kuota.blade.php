@extends('layouts.app')
@section('content')
<?php $method_bayar = array('atm' => 'ATM', 'mobile' => 'Mobile Banking', 'internet' => 'Internet Banking'); ?>
<section class="content-header">
  <h1>
    Setting
  </h1>
</section>
<div class="content">
  @include('adminlte-templates::common.errors')
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <!-- <div class="pull-left form-group col-sm-6"> -->
        <div class="pull-left form-group col-sm-12">
          <ul  class="nav nav-pills">
            <li class="active">
              <a  href="#1a" data-toggle="tab">Tambah Saldo</a>
            </li>
            <li><a href="#2a" data-toggle="tab">Konfirmasi Pembayaran</a>
            </li>
            <li><a href="#3a" data-toggle="tab">Riwayat</a>
            </li>
          </ul>
          <div class="tab-content clearfix">
            <div class="tab-pane active col-sm-6" id="1a">
              <!-- {!! Form::model($setting, ['action' => 'SettingController@postDeposit', 'method' => 'patch']) !!} -->
              <form method="POST" action="{{ url('/setting/postSaldo') }}">
                {!! csrf_field() !!}
              <h3>Lengkapi informasi deposit di bawah:</h3>
              </br>
              {!! Form::label('price', 'Jumlah Deposit:') !!}
              {!! Form::text('price', null, ['class' => 'form-control']) !!}
              {!! Form::label('account_number', 'No Rekening:') !!}
              {!! Form::text('account_number', $setting->account_number, ['class' => 'form-control','placeholder' => 'No Rekening','readonly'=>'readonly' ]) !!}
              </br>
              {!! Form::label('sumber', 'Sumber Dana :') !!}
              {!! Form::text('bank', $setting->bank, ['class' => 'form-control','placeholder' => 'No Rekening','readonly'=>'readonly']) !!} 
              </br>
              {!! Form::label('sumber', 'Atas Nama :') !!}
              {!! Form::text('account_name', $setting->account_name, ['class' => 'form-control','placeholder' => 'No Rekening','readonly'=>'readonly']) !!} 
              </br>
              {!! Form::label('sumber', 'Cara Transfer :') !!}
              </br>
              <select name="type" class="selectpicker">
                <option value="">Pilih Cara Transfer</option>
                @foreach($method_bayar as $key => $val)
                  <option value="{!! $key !!}">{!! $val !!}</option>
                @endforeach
                <!-- <option value="atm">ATM</option>
                <option value="mobile">Mobile Banking</option>
                <option value="internet">Internet Banking</option> -->
              </select>
              </br><br>
              @if(!empty($konfirmasi))
                {!! Form::label('sumber', 'Silahkan melakukan konfirmasi transaksi.') !!}
              @else
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
              @endif
              <!-- {!! Form::close() !!} -->
              </form>
            </div>
            <div class="tab-pane col-sm-6" id="2a">
              <h4>Detail transaksi.</h4>
              <table class="table table-responsive">
                <thead>
                  <th>Bank</th>
                  <th>Metode</th>
                  <th>Nominal</th>
                  <th>Tanggal</th>
                </thead>
                <tbody>
                  @if(!empty($konfirmasi))
                    <tr>
                      <td>{!! $konfirmasi[0]->name !!}</td>
                      @if($konfirmasi[0]->type == 'atm')
                        <td>ATM</td>
                      @elseif($konfirmasi[0]->type == 'mobile')
                        <td>Mobile Banking</td>
                      @elseif($konfirmasi[0]->type == 'internet')
                        <td>Internet Banking</td>
                      @endif
                      <td>RP. {!! number_format($konfirmasi[0]->price, 2) !!}</td>
                      <td>{!! date('d F Y H:i:s' ,strtotime($konfirmasi[0]->created_at)) !!}</td>
                    </tr>
                  @else
                    <td colspan="4">Tidak ada transaksi</td>
                  @endif
                </tbody>
              </table>
              <form method="POST" action="{{ url('/setting/postKonfirmSaldo') }}" enctype="multipart/form-data">
                {!! csrf_field() !!}
                {!! Form::hidden('id_deposit', empty($konfirmasi) ? null : $konfirmasi[0]->id, ['class' => 'form-control','placeholder' => 'No Rekening','readonly'=>'readonly']) !!}
              <h3>Lengkapi isian berikut untuk melakukan konfirmasi transfer.</h3>
              </br>
              {!! Form::label('bank', 'Bank:') !!}
              {!! Form::text('bank', $setting->bank, ['class' => 'form-control','placeholder' => 'No Rekening','readonly'=>'readonly']) !!}
              </br>
              {!! Form::label('sumber', 'Cara Transfer :') !!}
              </br>
              <select class="selectpicker form-control">
                @foreach($method_bayar as $key => $val)
                  @if(!empty($konfirmasi) && $key == $konfirmasi[0]->type)
                    <option value="{!! $key !!}" selected>{!! $val !!}</option>
                  @else
                    <option value="{!! $key !!}">{!! $val !!}</option>
                  @endif
                @endforeach
              </select>
              </br>
              {!! Form::label('account_number', 'No Rekening Anda:') !!}
              {!! Form::text('account_number', $setting->account_number, ['class' => 'form-control','placeholder' => 'No Rekening','readonly'=>'readonly' ]) !!}
              </br>
              {!! Form::label('sumber', 'Jumlah Transfer :') !!}
              {!! Form::text('transfer_total', null, ['class' => 'form-control','placeholder' => 'Jumlah Transfer']) !!} 
              </br>
              {!! Form::label('sumber', 'Tanggal Transfer :') !!}
              {!! Form::text('transfer_date', null, ['class' => 'form-control date','placeholder' => 'Tanggal Transfer']) !!} 
              </br>
              {!! Form::label('sumber', 'Berita Transfer (Jika Ada) :') !!}
              {!! Form::text('transfer_letter', null, ['class' => 'form-control','placeholder' => 'Berita Transfer (Jika Ada)']) !!} 
              </br>
              <input type="file" class="form-control" name="transfer_photo" accept="image/*">
              </br>
              {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            </form>
            </div>
            <div class="tab-pane col-sm-12" id="3a">
              <table class="table table-responsive" id="kuota-table">
                <thead>
                  <th>Bank</th>
                  <th>Metode</th>
                  <th>Nominal</th>
                  <th>Tanggal</th>
                  <th>Konfirmasi</th>
                  <th>Status</th>
                </thead>
                <tbody>
                  @foreach($history as $history)
                    <tr>
                      <td>{!! $history->bank_name !!}</td>
                      <!-- <td>{!! $history->type !!}</td> -->
                      @if($history->type == 'atm')
                        <td>ATM</td>
                      @elseif($history->type == 'mobile')
                        <td>Mobile Banking</td>
                      @elseif($history->type == 'internet')
                        <td>Internet Banking</td>
                      @endif
                      <td>RP. {!! number_format($history->price, 2) !!}</td>
                      <td>{!! date('d F Y H:i:s' ,strtotime($history->created_at)) !!}</td>
                      <!-- cek sudah di konfirmasi atau belum -->
                      @if(!empty($history->transfer_photo))
                        <td>
                          <div class="skus"><i class="fa fa-check" aria-hidden="true"></i> Konfirmasi</div>
                        </td>
                      @else
                        <td>
                          <div class="blok"><i class="fa fa-bookmark" aria-hidden="true"></i> Belum Konfirmasi</div>
                        </td>
                      @endif
                      <!-- cek apakah sudah di verivikasi oleh admin atau belum -->
                      @if($history->status == '1')
                        <td>
                          <div class="skus"><i class="fa fa-check" aria-hidden="true"></i> Ter Verifikasi</div>
                        </td>
                      @else
                        <td>
                          <div class="blok"><i class="fa fa-bookmark" aria-hidden="true"></i> Proses</div>
                        </td>
                      @endif
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection