
<!-- Costumer Id Field -->
<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('user_id', 'Nama:') !!}
    <p>{!! $setting->user->name !!}</p>
</div>


<!-- Costumer Id Field -->
<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('mindepn', 'Min Deposit Non Kyc:') !!}
    <p>Rp {!! number_format($setting->mindepn,0,'','.')   !!}</p>
</div>

<!-- Costumer Id Field -->
<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('mindepk', 'Min Deposit Kyc:') !!}
    <p>Rp {!! number_format($setting->mindepk,0,'','.')   !!}</p>
</div>

<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('maxdepn', 'Max Deposit Non Kyc:') !!}
    <p>Rp {!! number_format($setting->maxdepn,0,'','.')   !!} </p>
</div>

<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('maxdepk', 'Max Deposit Kyc:') !!}
    <p>Rp {!! number_format($setting->maxdepk,0,'','.')   !!}</p>
</div>





<!-- Costumer Id Field -->
<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('mintrn', 'Min Transaksi Non Kyc:') !!}
    <p>Rp {!! number_format($setting->mintrn,0,'','.')   !!}</p>
</div>

<!-- Costumer Id Field -->
<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('mintrn', 'Min Transaksi Kyc:') !!}
    <p>Rp {!! number_format($setting->mintrk,0,'','.')   !!}</p>
</div>

<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('maxtrn', 'Max Transaksi Non Kyc:') !!}
    <p>Rp {!! number_format($setting->maxtrn,0,'','.')   !!} </p>
</div>

<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('maxtrk', 'Max Transaksi Kyc:') !!}
    <p>Rp {!! number_format($setting->maxtrk,0,'','.')   !!}</p>
</div>





<!-- Costumer Id Field -->
<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('minwdn', 'Min Withdraw Non Kyc:') !!}
    <p>Rp {!! number_format($setting->minwdn,0,'','.')   !!}</p>
</div>

<!-- Costumer Id Field -->
<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('minwdk', 'Min Withdraw Kyc:') !!}
    <p>Rp {!! number_format($setting->minwdk,0,'','.')   !!}</p>
</div>

<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('maxwdn', 'Max Withdraw Non Kyc:') !!}
    <p>Rp {!! number_format($setting->maxwdn,0,'','.')   !!} </p>
</div>

<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('maxwdk', 'Max Withdraw Kyc:') !!}
    <p>Rp {!! number_format($setting->maxwdk,0,'','.')   !!}</p>
</div>


<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('admin', 'Admin:') !!}
    <p>Rp {!! number_format($setting->admin,0,'','.')   !!}</p>
</div>


<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('transfer', 'Transfer:') !!}
    <p>Rp {!! number_format($setting->transfer,0,'','.')   !!}</p>
</div>


<div class="form-group col-sm-3 col-lg-3">
    {!! Form::label('transaksi', 'Transaksi:') !!}
    <p>Rp {!! number_format($setting->transaksi,0,'','.')   !!}</p>
</div>



