@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Setting
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['url' => 'setting/store'.'?type=admin']) !!}

                        @include('setting.formadmin')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
