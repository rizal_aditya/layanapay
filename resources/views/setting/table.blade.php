<table class="table table-responsive" id="setting-table">
    <thead>
        <th>Name</th>
        <th>Min Deposit </th>
        <th>Max Deposit</th>
	<th>Min Transaksi</th>
        <th>Max Transaksi</th>
	<th>Min Withdraw</th>
        <th>Max Withdraw</th>
        <th>Issuer</th>
	<th>Merchant</th>
	<th>Costumer</th>
	<th>Cobranding</th>
	<th>Reveral</th>
	<th>Admin Trx</th>
	<th>Admin Trf</th>
	<th>Free</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($settings as $setting)
        <tr>
            <td>{!! $setting->user->name !!}</td>
            <td>Non Rp {!! number_format($setting->mindepn,0,'','.')  !!} | kyc Rp {!! number_format($setting->mindepk,0,'','.')  !!}</td>
            <td>Non Rp {!! number_format($setting->maxdepn,0,'','.')  !!} | kyc Rp {!! number_format($setting->maxdepk,0,'','.')  !!}</td>
	    
	    <td>Non Rp {!! number_format($setting->mintrn,0,'','.')  !!} | kyc Rp {!! number_format($setting->mintrk,0,'','.')  !!}</td>
            <td>Non Rp {!! number_format($setting->maxtrn,0,'','.')  !!} | kyc Rp {!! number_format($setting->maxtrk,0,'','.')  !!}</td>
	  
	    <td>Non Rp {!! number_format($setting->minwdn,0,'','.')  !!} | kyc Rp {!! number_format($setting->minwdk,0,'','.')  !!}</td>
            <td>Non Rp {!! number_format($setting->maxwdn,0,'','.')  !!} | kyc Rp {!! number_format($setting->maxwdk,0,'','.')  !!}</td>
	  

	
            <td>Rp {!! number_format($setting->issuer,0,'','.')  !!}</td>
	   <td>Rp {!! number_format($setting->merchant,0,'','.')  !!}</td>
	    <td>Rp {!! number_format($setting->costumer,0,'','.')  !!}</td>
	   <td>Rp {!! number_format($setting->cobranding,0,'','.')  !!}</td>
	   <td>Rp {!! number_format($setting->reveral,0,'','.')  !!}</td>

           <td>Rp {!! number_format($setting->admintrx,0,'','.')  !!}</td>
	  <td>Rp {!! number_format($setting->admintrf,0,'','.')  !!}</td>	
		<td>{!! $setting->count !!} Transaksi / bulan</td>
            <td>@if($setting->status =="1") Verifikasi @else Proses @endif</td>
            <td>
                {!! Form::open(['route' => ['setting.destroy', $setting->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! url('setting/'.$setting->id.'') !!}?type=admin" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! url('setting', [$setting->id]) !!}/edit?type=admin" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>