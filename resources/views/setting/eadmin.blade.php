@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Setting | {!! $setting->user->name !!} Saldo : Rp {!! number_format($setting->saldo,0,'','.')   !!}
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($setting, ['route' => ['setting.update', $setting->id.'?type=admin'], 'method' => 'patch']) !!}

                        @include('setting.formadmin')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection