
<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


<!-- Account Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('account_name', 'Nama Bank:') !!}
    {!! Form::text('account_name', null, ['class' => 'form-control']) !!}
</div>



<!-- Type Field -->
<div class="form-group col-sm-12">
    {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!}
    {!! Form::hidden('type', $type, ['class' => 'form-control']) !!} 
</div>

<div class="form-group col-sm-12">
    @if(!empty($user->photo))
    <img src="{!! url($user->photo) !!}" width="100" height="100">
@endif  
</div>


<div class="form-group col-sm-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>
</div>


<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($user) ? $user->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($user) ? $user->status == 0 : true) }} No
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('bank.index') !!}" class="btn btn-default">Cancel</a>
</div>
