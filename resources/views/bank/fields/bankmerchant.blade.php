<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('type', $type, ['class' => 'form-control']) !!}
</div>



<!-- Account Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('account_name', 'Account Name:') !!}
    {!! Form::text('account_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Account Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('account_number', 'Account Number:') !!}
    {!! Form::text('account_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Account Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($user) ? $user->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($user) ? $user->status == 0 : true) }} No
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if($type == "admin")
    <a href="{!! url('admin') !!}" class="btn btn-default">Cancel</a>
    @else
    <a href="{!! route('bank.index') !!}" class="btn btn-default">Cancel</a>
    @endif
</div>
