<table class="table table-responsive" id="bank-table">
    <thead>
        <th>Image</th>
        <th>Name</th>
        <th>Account Name</th>
        <th>Account Number</th>
        <th>Saldo</th>
        <th>Admin</th>
      
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($bank as $bank)
        <tr>
             <td><img width="150" height="50" src="{!! $bank->image !!}"></td>
            <td>{!! $bank->name !!}</td>
       
            <td>{!! $bank->account_name !!}</td>
            <td>{!! $bank->account_number !!}</td>
            <td>Rp {!! number_format($bank->saldo ,0,'','.') !!}</td>
            <td>Rp {!! number_format($bank->admin ,0,'','.') !!}</td>
         
            <td>@if($bank->status =="1") Publish  @else Not Publish @endif</td>
            <td>
                {!! Form::open(['route' => ['bank.destroy', $bank->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('bank.show', [$bank->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('bank.edit', [$bank->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>