<table class="table table-responsive" id="bank-table">
    <thead>
        <th>Image</th>
        <th>Nama Bank</th>
        <th>Grop</th>
        <th>Akun</th>
        
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($bank as $bank)
        <tr>
            <td><img src="{!! empty($bank->image) ? config('app.photo_default') : url($bank->image)  !!}" width="70" height="70"></td>
            <td>{!! $bank->account_name !!}</td>
            <td>{!! $bank->name !!}</td>
            <td>{!! $bank->user->name !!}</td>
            
            <td>@if($bank->status =="0") Non Aktif @else Aktif @endif</td>
            <td>
                {!! Form::open(['route' => ['bank.destroy', $bank->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! url('bank/'.$bank->id.'/?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! url('bank/'.$bank->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>