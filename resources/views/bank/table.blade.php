@if($type == "bank")
    @include('bank.table.listbank')
@elseif($type =="admin")
    @include('bank.table.adminbank')
@elseif($type =="merchant")
	@include('bank.table.merchantbank')
@elseif($type =="user")
    @include('bank.table.userbank')
@endif

