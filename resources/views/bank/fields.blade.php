@if($type == "bank")
    @include('bank.fields.listbank')
@elseif($type =="admin")
    @include('bank.fields.bankadmin')
@elseif($type =="merchant")
    @include('bank.fields.bankmerchant')    
@elseif($type =="user")
    @include('bank.fields.userbank')
@endif
