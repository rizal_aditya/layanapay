<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="menu-table">
    <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Url</th>
        <th>Icon</th>
        <th>Parent</th>
        <th>Position</th>
        <th>Type</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($menu as $menu)
        <tr>
            <td>{!! $menu->id !!}</td>
            <td>{!! $menu->name !!}</td>
            <td>{!! $menu->url !!}</td>
            <td>{!! $menu->icon !!}</td>
            <td>{!! $menu->parent !!}</td>
            <td>{!! $menu->position !!}</td>
             <td>{!! $type !!} </td>
            <td>@if($menu->status == '1')yes @else No @endif</td>
            <td>
                
                 {!! Form::open(['route' => ['menu.destroy', $menu->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    
                    <a href="{!! url('menu/'.$menu->id.'/?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! url('menu/'.$menu->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

