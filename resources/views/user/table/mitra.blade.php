<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="dynamic-table" >
    <thead>
        <th>Photo</th>
        <th>PIN QR</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
       
     @foreach($user as $user)
        
        <tr>
            <td><img width="50" height="50" src="{!! $user->photo == '' ? config('app.photo_default') : url($user->photo)  !!} "></td>
             @if($user->qrcode !="")
             <td>{!! QrCode::size(100)->generate($user->qrcode); !!}</td>
             @else
             <td>Mitra Belum Aktif</td>
             @endif	
             <td>{!! $user->name !!}</td>
             <td>{!! $user->email !!}</td>
             <td>{!! $user->phone !!}</td>
             <td>{!! $user->address !!}</td>    
             <td> 

             @if($user->status>0)
                <a href="{!! url('user/'.$user->id.'/disable?status=0'.'&type='.$type) !!}">Aktif</a>
             @else
                <a href="{!! url('user/'.$user->id.'/enable?status=1'.'&type='.$type) !!}">Non Aktif</a>
             @endif

             </td>
           

              @role('admin')
             <td>
               {!! Form::open(['route' => ['user.destroy', $user->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    <a href="{!! route('user.show', [$user->id.'?type='.$type]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                   
                     <a href="{!! url('user/'.$user->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                   
                   <!--  {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!} -->
                   
                </div>
                {!! Form::close() !!}
             </td>
             @endrole


        </tr>
    @endforeach
    </tbody>
</table>
</div>



