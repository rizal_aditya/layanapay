<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dynamic-table" >
        <thead>
            <th>Photo</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
    		<th>Saldo</th>
    		<!-- <th>No Rekening</th>
    		<th>Bank</th>
    		<th>Atas Nama</th> -->
            <th>Verified</th>
            <th colspan="3">Action</th>
        </thead>
         <tbody id="CobrandTable">
        
        </tbody>
    </table>

    <ul class="pagination">
         <div class="firstDep"></div>   
         <div class="navigationDep"></div>
         <div class="lastDep"></div>
        
        </li>
    </ul>
</div>