<!-- Name Field -->

@if($type == 'kontak')

<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', "placeholder" => "Name"]) !!}
    {!! Form::hidden('type', $type, ['class' => 'form-control']) !!} 
    {!! Form::hidden('client_id', '0', ['class' => 'form-control']) !!} 
</div>


<div class="form-group col-sm-12">
    {!! Form::label('owner', 'Owner:') !!}
    {!! Form::text('owner', null, ['class' => 'form-control','placeholder' => 'Owner']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control','placeholder' => 'Phone']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::text('location', null, ['class' => 'form-control','placeholder' => 'Location']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control',"placeholder" => "Email"]) !!}
</div>
        @if(!empty($user->password))
        <div class="form-group col-sm-12">
              {!! Form::label('password', 'Password:') !!}
              <input class="form-control" placeholder="Password" name="password" id="password" type="password">
            @if(Route::currentRouteName() == 'user.edit')
                <div class="alert alert-info">
                  <span class="fa fa-info-circle"></span> Leave the password field blank if you wish to keep it the same.
                </div>
            @endif
        </div>
        @else
        <div class="form-group col-sm-12">
        {!! Form::label('password', 'Password:') !!}
        {{ Form::password('password', array('class' => 'form-control','placeholder'=>'Password')) }}

        </div>
        @endif




<div class="form-group col-sm-12">
    @if(!empty($user->photo))
	<img src="{!! url($user->photo) !!}" width="100" height="100">
@endif	
</div>


<div class="form-group col-sm-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>
</div>


<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($user) ? $user->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($user) ? $user->status == 0 : true) }} No
</div>

@elseif($type =="deposite")
 {!! Form::hidden('client_id', null, ['class' => 'form-control']) !!} 
 {!! Form::hidden('type', $type, ['class' => 'form-control']) !!} 
  {!! Form::hidden('email', null, ['class' => 'form-control']) !!} 
<div class="form-group col-sm-12">
    {!! Form::label('deposite', 'Deposite:') !!}
    {!! Form::text('deposite', null, ['class' => 'form-control','placeholder' => 'Deposite']) !!}
</div>


@endif


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('user/?type='.$type) !!}" class="btn btn-default">Cancel</a>
</div>