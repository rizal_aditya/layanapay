

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nama:') !!}
    <p>{!! $user->name !!}</p>
</div>


<div class="form-group">
    {!! Form::label('type_id', 'Jenis Identitas:') !!}
    <p>{!! $user->type_id !!}</p>
</div>

<div class="form-group">
    {!! Form::label('identity_number', 'No Identitas:') !!}
    <p>{!! $user->identity_number !!}</p>
</div>

<div class="form-group">
    {!! Form::label('identity_photo', 'Photo Identitas:') !!}
    <p><img width="100" height="100" src="{!! $user->identity_photo !!}" ></p>
</div>

<div class="form-group">
    {!! Form::label('bank', 'Bank:') !!}
    <p>{!! $user->bank !!}</p>
</div>

<div class="form-group">
    {!! Form::label('no_rekening', 'No Rekening:') !!}
    <p>{!! $user->no_rekening !!}</p>
</div>



<div class="form-group">
    {!! Form::label('atas_nama', 'Atas Nama:') !!}
    <p>{!! $user->atas_nama !!}</p>
</div>




<!-- Name Field -->
<div class="form-group">
    {!! Form::label('qrcode', 'QRCODE :') !!}
    <p> <td>{!! QrCode::size(300)->generate($user->qrcode); !!}</td></p>
</div>


<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $user->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $user->phone !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $user->address !!}</p>
</div>




<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>
         @if($user->status>0)
            No
            @else
            Yes
          @endif  

    </p>
</div>





<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $user->created_at->format('d F Y') !!}</p>
</div>




