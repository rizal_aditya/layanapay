@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">User
                <small>{{ $type }}</small>
        </h1>

        <h1 class="pull-right">
         @if($type == 'admin')
          <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! url('user/create?type='.$type) !!}">Add New</a>
         @elseif($type == 'co-branding')
          <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal" onclick="GetFieldAddCoBranding();" data-target="#myModal">Add New</a>
         @elseif($type == 'merchant')
          <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal" onclick="GetFieldAddMerchant();" data-target="#myModal">Add New</a>
         @elseif($type == 'issuer')
          <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal" onclick="GetFieldAddIssuer();" data-target="#myModal">Add New</a>
         @endif
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
  <div class="box-header">
    <h3 class="box-title">Data {{ $type }} 

              @if($type =="costumer")

              <select id="StVerCos"  class=" selectpicker" >
                <option value="10" selected>Semua</option>
                <option value="0">Proses</option>
                <option value="1">Verifikasi</option>
              </select>

              @elseif($type == "merchant")

               <select id="StVerMer"  class=" selectpicker" >
                <option value="10" selected>Semua</option>
                <option value="0">Proses</option>
                <option value="1">Verifikasi</option>
              </select>

              @elseif($type == "co-branding")

              <select id="StVerCo"  class=" selectpicker" >
                <option value="10" selected>Semua</option>
                <option value="0">Proses</option>
                <option value="1">Verifikasi</option>
              </select>

              @endif

              </h3>





  <form class="navbar-form navbar-right" method="GET" >
  <div class="box-tools">
    <div class="input-group input-group-sm" style="width: 150px;">
      @if($type =="costumer")
        <input id="searchCos" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      @elseif($type == "merchant")
        <input id="searchMer" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      @elseif($type == "co-branding")
        <input id="searchCob" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      @elseif($type == "issuer")
        <input id="searchCob" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      @endif
     
    </div>
  </div>
  </form>

</div>              
    
                
                        @include('user.table')
               

              
            </div>
        </div>
    </div>
@endsection



<div class="modal fade" id="myModal" role="dialog"  >
     <div class="modal-dialog">  
        <div id="Modals" class="pull-left"></div>
      </div>

  </div>
