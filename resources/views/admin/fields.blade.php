<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', "placeholder" => "Name"]) !!}
    
    {!! Form::hidden('client_id', '0', ['class' => 'form-control']) !!} 
</div>


<div class="form-group col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control','placeholder' => 'Phone']) !!}
</div>



<div class="form-group col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control',"placeholder" => "Email"]) !!}
</div>
        @if(!empty($user->password))
        <div class="form-group col-sm-12">
              {!! Form::label('password', 'Password:') !!}
              <input class="form-control" placeholder="Password" name="password" id="password" type="password">
            @if(Route::currentRouteName() == 'user.edit')
                <div class="alert alert-info">
                  <span class="fa fa-info-circle"></span> Leave the password field blank if you wish to keep it the same.
                </div>
            @endif
        </div>
        @else
        <div class="form-group col-sm-12">
        {!! Form::label('password', 'Password:') !!}
        {{ Form::password('password', array('class' => 'form-control','placeholder'=>'Password')) }}

        </div>
        @endif




<div class="form-group col-sm-12">
    @if(!empty($user->photo))
	<img src="{!! url($user->photo) !!}" width="100" height="100">
@endif	
</div>


<div class="form-group col-sm-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
</div>





<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('admin') !!}" class="btn btn-default">Cancel</a>
</div>