@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Edit Profile</h1>
       
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($user, ['route' => ['admin.update', $user->id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('admin.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection