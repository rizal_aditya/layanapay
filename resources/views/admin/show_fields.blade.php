
<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Photo:') !!}
    <p><img width="50" height="50" src="{!! $user[0]->photo !!}"></p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nama:') !!}
    <p>{!! $user[0]->name !!}</p>
</div>


<div class="form-group">
    {!! Form::label('type_id', 'Email:') !!}
    <p>{!! $user[0]->email !!}</p>
</div>

<div class="form-group">
    {!! Form::label('type_id', 'Phone:') !!}
    <p>{!! $user[0]->phone !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $user[0]->address !!}</p>
</div>

<div class="form-group">
    
       <table class="table table-responsive">
           <thead>
                <th>Photo</th>
                <th>Bank</th>
                <th>No Rekening</th>
                <th>Atas Nama</th>
                <th>Action</th>
           </thead>
           @foreach($bank as $bank)
           <tr>
              
                <td><img width="100" height="50" src="{!! $bank->image !!}"> </td>
                <td>{!! $bank->name !!}</td>
                <td>{!! $bank->account_number !!}</td>
                <td>{!! $bank->account_name !!}</td>
                <td>
                  
                  {!! Form::open(['route' => ['bank.destroy', $bank->id.''.'?type=admin'], 'method' => 'delete']) !!}
                <div class='btn-group'>
                   
                    <a href="{!! url('bank/'.$bank->id.'/edit?type=admin') !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
                </td>
           </tr>
           @endforeach
       </table> 
    
</div>















