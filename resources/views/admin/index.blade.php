@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Data
            <small>Admin</small>
        </h1>
    </section>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.show_fields')
                    


                     <!-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModals">Edit</a> -->

                 		<a href="{!! route('admin.edit', [$user[0]->id]) !!}" class="btn btn-primary" >Edit</a>
                </div>
            </div>
        </div>
    </div>
@endsection



<!-- Modal -->
<div class="modal fade" id="myModals" tabindex="" role="dialog"  aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
      </div>
      <div class="modal-body">
        	<div class="form-group">
			    {!! Form::label('name', 'Nama:') !!}
			    {!! Form::text('name', $user[0]->name, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('email', 'Email:') !!}
			    {!! Form::text('email', $user[0]->email, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('phone', 'Phone:') !!}
			    {!! Form::text('phone', $user[0]->phone, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('address', 'Address:') !!}
			    {!! Form::textarea('address', $user[0]->address, ['class' => 'form-control']) !!}
			</div>
		

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="SimpanProfile();" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>

