<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Konfirmasi {!! $merchant !!}</title>
    </head>


<style type="text/css">
body{
background: #dedede;

} 

#container{
    float: left;
    width: 100%;
}

#container .margin-ats{
    margin: 0 auto;
    width: 610px;
}

#container .margin-ats .header,#container .margin-ats .body{
    float: left;
    width: 100%;
    background: #fff;
    
}

.ats-mg{
    
    border-radius: 10px 10px 0px 0px;
}
.border-top{
    border-top: 2px solid #b71111;
    
}

#container .header .margin-logo, .margin-btn{
    margin: 0px auto;
    width: 200px;
}
#container .header .margin-logo img{
    padding: 13px 74px;
}
#container .margin-ats .body p{
    padding: 0px 15px;
    color: #423f3f;
}

.po-spase{
    width: 100%;
    padding: 0px 0px;
    float: left;
}

.btn{
     background-color: #f4f4f4;
    color: #444;
    border-color: #ddd;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    padding: 6px 12px;
    border: 1px solid #ddd;
    border-radius: 3px;
    cursor: pointer;

}

.btn:hover{
    background-color: #c02814;
background-image: linear-gradient(to bottom,#bf2c1a,#ad2817);
}

.tengt{
     background-color: #d14836;
    color: #fff;
    border: 1px solid transparent;
    
    margin: 10px 63px;
}
.table{
    padding: 0px 15px;
    color: #423f3f;
    
}

.base{
    margin-bottom: 21px;
}
.footer-col{
    background: #b71111;
    float: left;
width: 100%;
color:#fff;
border-radius: 0px 0px 10px 10px;
}

.footer-col .bgr{
   padding: 25px 20px;
}

</style>
    <body>
        <div id="container" style="float: left;width: 100%;background: #dedede; padding: 40px 0px;">
                <div class="margin-ats" style="margin: 0 auto;width: 610px;">
                    <div class="header ats-mg" style=" float: left;width: 100%;background: #fff;border-radius: 10px 10px 0px 0px;">
                            <div class="margin-logo" style="margin: 0px auto;width: 200px;">
                                <img style=" padding: 13px 74px;"   height="50" src="{!! $logo !!}">
                            </div>
                    </div>
                    <div class="body border-top" style="float: left;width: 100%;background: #fff; border-top: 2px solid #b71111;">

                        <div class="po-spase" style="width: 100%;padding: 0px 0px;float: left;">
                        <p style="padding: 0px 15px;color: #423f3f;">Hai {{ $nama }},<br> Terima kasih atas kepercayaan anda telah mengisi saldo di {!! $merchant !!} Mohon segera lakukan pembayaran sebelum:</p>
                         </div>   
                       
                          <div class="po-spase"  style="width: 100%;padding: 0px 0px;float: left;">
                            <div style="margin: 9px auto;width: 435px;">
                               
                                <div style="float: left;background-color: #bdbdbd;padding: 11px 15px;border-radius: 3px;color: #2f2f2f;font-weight: bold;">
                              		{{ $jttempo }} WIB 
                              </div>
                            </div>  
                        </div> 

                        

                        <div class="po-spase base"  style="width: 100%;padding: 0px 0px;float: left;margin-bottom: 21px;">
                       			<div style="margin: 19px auto;width: 235px;">  
                                  <div style="color:#2f2f2f;text-align:center;font-size: 17px; ">   Lakukan pembayaran sebesar: 
                                  </div>  
                                </div>
                                  <div style="margin: 19px auto;width: 209px;">   
                                      <div style="text-align:center;font-size:33px;font-weight:bold;color:#2f2f2f; ">  
                       			      <div style="width: auto;text-align:center;">Rp {{ $price }}  </div>
                                      </div>

                                   </div>
                                   <div style="margin: 19px auto;width: 235px;">  
                                  <div style="color:#2f2f2f;text-align:center;font-size: 17px; ">   <b>TEPAT</b> hingga <b>3 digit terakhir</b>
                                  </div>  
                                </div> 

                                 <div style="margin: 19px auto;width: 425px;">  
                                  <div style="color: #2f2f2f;text-align: center;font-size: 16px;font-style: italic; ">   Perbedaan nilai transfer akan menghambat proses verifikasi
                                  </div>  
                                </div> 

                                  <div class="po-spase base"  style="width: 100%;padding: 0px 0px;float: left;margin-bottom: 21px;">
                                   <p style="padding: 0px 15px;color: #423f3f;">
                                Pembayaran dapat dilakukan ke salah satu nomor rekening a/n PT Layana Computindo:
                                </p>
                                </div>


                                 <div class="po-spase base"  style="width: 100%;padding: 0px 0px;float: left;margin-bottom: 21px;">
                                        <div style="margin: 0px auto;width: 380px;"> 
                                            <ul style="float: left;">
                                            <?php  $bank = DB::table('bank')->where(['type'=>'admin','user_id'=>$id])->get(); ?>
                                            @foreach($bank as $bank)
                                          

                                            <li style="float: left;list-style:none;border-top: 1px solid #ddd;width: 100%;">
                                            <div style="float:left;">
                                            <img stye="float:left;" src="{!! $bank->image !!}">
                                             </div>
                                             
                                             <div style="float: right;width: 200px;">
                                                 <div style="float: right;padding: 6px 0px;font-size: 14px;font-weight: bold;">{{ $bank->account_name }}, {!! $bank->branch !!}</div>
                                                <div style="float: right;padding: 0px 1px;font-size: 14px;font-weight: bold;">{!! $bank->account_number !!}</div>
                                             </div>   

                                            </li>
                                            @endforeach
                                            </ul>
                                        </div>
                                 </div>

                                 <div class="po-spase base"  style="width: 100%;padding: 0px 0px;float: left;margin-bottom: 21px;">
                                    <p style="padding: 0px 15px;color: #423f3f;">
                                    Berikut adalah penjelasan tagihan pembayaran:
                                    </p>
                                     <table class="table" style="padding: 0px 15px;color: #423f3f;">
                                        <tr>
                                            <td>Waktu Transaksi</td>
                                            <td>:</td>
                                            <td>{{ $dateTrans }}</td>
                                        </tr>
                                          <tr>
                                            <td>Pembeli</td>
                                            <td>:</td>
                                            <td>{{ $nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Metode Pembayaran</td>
                                            <td>:</td>
                                            <td>{{ $metPem }}</td>
                                        </tr>
                                         <tr>
                                            <td>Topup TambahSaldo</td>
                                            <td>:</td>
                                            <td><b>Rp {{ $price }}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Harga Total Belanja</td>
                                            <td>:</td>
                                            <td><b>Rp {{ $price }}</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>TOTAL PEMBAYARAN</b></td>
                                            <td>:</td>
                                            <td><b>Rp {{ $price }}</b></td>
                                        </tr>
                                        


                                    </table>


                                 </div>
                                   
                        </div> 

                        <div class="po-spase base"  style="width: 100%;padding: 0px 0px;float: left;margin-bottom: 21px;">
                          <p style="padding: 0px 15px;color: #423f3f;">
                        Setelah melakukan pembayaran, sistem kami akan memverifikasi pembayaran secara otomatis. Jika kamu menghadapi kendala mengenai pembayaran, silakan langsung Hubungi {!! $merchant !!} atau Ubah Metode Pembayaran untuk memilih metode pembayaran lain.
                        </p>
                        </div> 

                        <div class="po-spase"  style="width: 100%;padding: 0px 0px;float: left;">
                         
                        <table class="table" style="padding: 0px 15px;color: #423f3f;">
                            <!-- <tr>
                                <td>Segala bentuk informasi seperti nomor kontak, alamat e-mail, atau password kamu bersifat rahasia. Jangan menginformasikan data-data tersebut kepada siapa pun, termasuk kepada pihak yang mengatasnamakan {!! $merchant !!}.</td>  
                            </tr> -->
                              <tr>
                                <td>Admin {!! $merchant !!}</td>
                            </tr>
                        </table>
                       
                        </div>

                    </div>
                    <div class="footer-col " style="    background: #b71111;float: left;width: 100%;color:#fff;border-radius: 0px 0px 10px 10px;">
                        <div class="bgr" style=" padding: 25px 20px;" >
                        {!! $merchant !!}
                        </div>
                    </div>
                </div>
                
        </div>
        

    </body>
</html>


