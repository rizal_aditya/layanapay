<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Forgot Password {!! $merchant !!}</title>
    </head>


<style type="text/css">
body{
background: #dedede;

} 

#container{
    float: left;
    width: 100%;
}

#container .margin-ats{
    margin: 0 auto;
    width: 610px;
}

#container .margin-ats .header,#container .margin-ats .body{
    float: left;
    width: 100%;
    background: #fff;
    
}

.ats-mg{
    
    border-radius: 10px 10px 0px 0px;
}
.border-top{
    border-top: 2px solid #b71111;
    
}

#container .header .margin-logo, .margin-btn{
    margin: 0px auto;
    width: 200px;
}
#container .header .margin-logo img{
    padding: 13px 74px;
}
#container .margin-ats .body p{
    padding: 0px 15px;
    color: #423f3f;
}

.po-spase{
    width: 100%;
    padding: 0px 0px;
    float: left;
}

.btn{
     background-color: #f4f4f4;
    color: #444;
    border-color: #ddd;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    padding: 6px 12px;
    border: 1px solid #ddd;
    border-radius: 3px;
    cursor: pointer;

}

.btn:hover{
    background-color: #c02814;
background-image: linear-gradient(to bottom,#bf2c1a,#ad2817);
}

.tengt{
     background-color: #d14836;
    color: #fff;
    border: 1px solid transparent;
    
    margin: 10px 63px;
}
.table{
    padding: 0px 15px;
    color: #423f3f;
    
}

.base{
    margin-bottom: 21px;
}
.footer-col{
    background: #b71111;
    float: left;
width: 100%;
color:#fff;
border-radius: 0px 0px 10px 10px;
}

.footer-col .bgr{
   padding: 25px 20px;
}

</style>
    <body >
        <div id="container" style="float: left;width: 100%;background: #dedede; padding: 40px 0px;">
                <div class="margin-ats" style="margin: 0 auto;width: 610px;">
                    <div class="header ats-mg" style=" float: left;width: 100%;background: #fff;border-radius: 10px 10px 0px 0px;">
                            <div class="margin-logo" style="margin: 0px auto;width: 200px;">
                                <img style=" padding: 13px 74px;"  width="50" height="50" src="{!! $logo !!}">
                            </div>
                    </div>
                    <div class="body border-top" style="float: left;width: 100%;background: #fff; border-top: 2px solid #b71111;">
                        <div class="po-spase" style="width: 100%;padding: 0px 0px;float: left;">
                        <p style="padding: 0px 15px;color: #423f3f;">Kami menerima permintaan dari Anda untuk mengganti kata sandi Anda:</p>
                         </div>   
                        <div class="po-spase"  style="width: 100%;padding: 0px 0px;float: left;">
                            <div class="margin-btn" style="margin: 0px auto;width: 200px;">
                                    
                                    <a href="{!! $domain !!}/forgotpassword?get={{ $url }}" style="background-color: #d14836;color: #fff;border: 1px solid transparent;margin: 10px 63px;border-color: #ddd;display: inline-block;font-size: 14px;font-weight: 400;padding: 6px 30px;border: 1px solid #ddd;border-radius: 3px;cursor: pointer;width: 102px;
    text-decoration: none;" class="btn tengt">Ganti kata sandi</a>

                            </div>
                        </div> 
                          <div class="po-spase"  style="width: 100%;padding: 0px 0px;float: left;">
                           <p style="padding: 0px 15px;color: #423f3f;"> Jika tombol di atas tidak bekerja, copy dan paste url berikut di browser anda:</p>
                           <p  style="padding: 0px 15px;color: #423f3f;" ><a href="{!! $domain !!}/forgotpassword?get={{ $url }}" >{!! $domain !!}/forgotpassword?get={{ $url }}</a></p>
                        </div> 

                        <div class="po-spase"  style="width: 100%;padding: 0px 0px;float: left;">
                        <p style="padding: 0px 15px;color: #423f3f;">Anda memiliki waktu 24 jam untuk mengganti kata sandi. Setelah itu, Anda harus meminta yang baru. 
Tidak meminta kata sandi baru? Abaikan saja email ini.</p>
                        </div> 

                        <div class="po-spase base"  style="width: 100%;padding: 0px 0px;float: left;margin-bottom: 21px;">
                        <table class="table" style="padding: 0px 15px;color: #423f3f;">
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td>{{ $name }}</td>
                            </tr>
                              <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td>{{ $email }}</td>
                            </tr>
                        </table>
                       
                        </div> 

                       

                    </div>
                    <div class="footer-col " style="    background: #b71111;float: left;width: 100%;color:#fff;border-radius: 0px 0px 10px 10px;">
                        <div class="bgr" style=" padding: 25px 20px;" >
                        {!! $merchant !!}
                        </div>
                    </div>
                </div>
                
        </div>
        

    </body>
</html>


