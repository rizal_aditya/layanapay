<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;


class BankCriteria implements CriteriaInterface
{

     
     private $type;
     private $id;
    
     public function __construct($type,$id)
     {
        $this->type = $type;
       	$this->id = $id;
        
     }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
	
        $model = $model->where(['type'=>$this->type,'user_id'=>$id])->orderBy('id','desc');
        return $model;
    }
}
