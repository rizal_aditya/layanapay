<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\API\CreateGenerateAPIRequest;
use App\Http\Requests\API\UpdateGenerateAPIRequest;
use App\Models\Generate;
use App\Repositories\GenerateRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use Illuminate\Routing\UrlGenerator;
use File;
use DB;
use QrCode;



class AccessController extends AppBaseController
{
    /** @var  ActivityRepository */
    private $generateRepository;
    protected $url;

	

    public function __construct(GenerateRepository $generateRepo,UrlGenerator $url)
    {
        $this->generateRepository = $generateRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Activity.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // return Response::json(ResponseUtil::makeError('Data tidak bisa diakses'), 404);
    }

    public function login(Request $request)
    {
        $params = json_decode(file_get_contents('php://input'),true);

  	if($params==null || $params =="" || $params==" ")
  	{
    		//echo 'Data belum lengkap data tidak bisa akses';
    		return 'Data belum lengkap dan data tidak bisa akses';
  	}

	$email = $params['email'];
	$api_key = $params['api_key'];
	$api_secret = $params['api_secret'];
	
	if(empty($email))
	{
		return Response::json(ResponseUtil::makeError('Email Kosong'), 404);
	}
	
	if(empty($api_key))
	{
		return Response::json(ResponseUtil::makeError('Api Key Kosong'), 404);
	}

	if(empty($api_secret))
	{
		return Response::json(ResponseUtil::makeError('Api Secret Kosong'), 404);
	}


	$emailID = DB::table('client')->where('email',$email)->get();
	if(!empty($emailID))
	{

	  $api_keyID = DB::table('client')->where('api_key',$api_key)->get();
	  if(!empty($api_keyID))
	  {

             $api_secretID = DB::table('client')->where('api_secret',$api_secret)->get();
	     if(!empty($api_secretID))
	     {	

		//sukses
		$ip_address = $api_secretID[0]->ip_address;

		$myUrl =  $api_secretID[0]->url_server;
		$myParsedURL = parse_url($myUrl);
		$myDomainName= $myParsedURL['host'];
		$ipAddress = gethostbyname($myDomainName);

		if($ipAddress == $ip_address)
		{

			$generate = bcrypt($api_keyID[0]->email.$api_keyID[0]->ip_address.$api_keyID[0]->role_id);
			DB::table('client')->where(['email'=>$api_keyID[0]->email,'ip_address'=>$api_keyID[0]->ip_address])->update(['key_code'=>$generate]); 
			 $data['token'] = $generate;

			return $this->sendResponse($data, 'Token Created successfully');

			
		}else{

			return Response::json(ResponseUtil::makeError('IP Address Tidak Terdaftar'), 404);

		}
		

	     }else{

		return Response::json(ResponseUtil::makeError('Api Secret Tidak Terdaftar'), 404);

	     }	

	  }else{

		return Response::json(ResponseUtil::makeError('Api Key Tidak Terdaftar'), 404);

	  }		


	}else{

	  return Response::json(ResponseUtil::makeError('Email Tidak Terdaftar'), 404);

	}


	
    }



	public function reset()
	{

	$setting = DB::table('setting')->update(['count'=>'10']);

	}


	public function transaksi(Request $request)
	{
		$input = $request->all();

		$token = $request->token;
		

		$qrcode = $request->qrcode;
        	        	



		
		$client = DB::table('client')->where('key_code',$token)->get();
		if(!empty($client[0]->id))
		{
			
			$user = DB::table('users')->where('email',$client[0]->email)->get();
			$costumer_id = $user[0]->id;
        		$saldo_costumer = $user[0]->saldo;
       		 	$pin  = $user[0]->pin;
        		$costumer_name = $user[0]->name;
			$param = $user[0]->param;

			$saldoiss = DB::table('setting')->where('user_id',$param)->get();
        		if($saldoiss[0]->saldo =="0")
        		{
            	 		return Response::json(ResponseUtil::makeError('server terjadi masalah coba beberapa saat lagi!'), 404);  
        		}else{


				                                    	 
				
                                $generate = DB::table('generate')->where(['code'=>$qrcode,'user_id'=>$costumer_id])->get();
				$price = $generate[0]->nominal;
				$saldo_mitra = $user[0]->saldo;

				//kurangi saldo mitra 
				//DB::table('setting')->where('user_id',$costumer_id)->update(['saldo'=>$mitra_saldo]);
				//insert transaksi
                             	DB::table('transaction')->insert(
						[
							'qrcode'=>$qrcode,
							'user_id'=>$costumer_id,
							'price'=>$price,
							'type'=>'tr-generate',
							'status'=>'1',
							'tanggal'=> date("Y-m-d"),
							'created_at'=> DB::raw('now()')
						]);

				$transaksi = DB::table('transaction')->where('qrcode',$qrcode)->get();

						
								//potongan untuk cobrading	
								$biayacobranding = $saldoiss[0]->cobrandingtrx;		
								$potongan = $saldoiss[0]->admintrx+$biayacobranding;
                                                		$total_mitra = $saldo_mitra+$price-$potongan; 

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$saldoiss[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$costumer_id])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$saldoiss[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$costumer_id])->update(['saldo'=>$duit_isuer]);
				  


				DB::table('history')->insert(
						[
							'transaction_id'=>$transaksi[0]->id,
							'user_id'=>$costumer_id,
							'price'=>$price,
							'type'=>'generate',
							'description'=>'Transaksi lewat API Service'
							'admin'=>$total_mitra, 
							'status'=>'1',
							'tanggal'=> date("Y-m-d"),
							'created_at'=> DB::raw('now()')
						]);


				$history = DB::table('history')->where('transaction_id',$transaksi[0]->id)->get();
				
				return $this->sendResponse($history, 'Transaksi berhasil');
         		}


		}else{

			return Response::json(ResponseUtil::makeError('Token tidak terdaftar'), 404);


		}

	}


	public function profile(Request $request)
	{

		$token = $request->get('token');
				
		$client = DB::table('client')->where('key_code',$token)->get();
		
		

		$user = DB::table('users')->select('a.id','a.name','a.type','a.status')->where('email',$client[0]->email)->get();	
		return $this->sendResponse($user, 'Profile berhasil di akses');
		


	}


	


    /**
     * Show the form for creating a new Activity.
     *
     * @return Response
     */
    public function create()
    {
        return Response::json(ResponseUtil::makeError('Data tidak bisa diakses'), 404);
    }

    /**
     * Store a newly created Activity in storage.
     *
     * @param CreateActivityRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityRequest $request)
    {
	return Response::json(ResponseUtil::makeError('Data tidak bisa diakses'), 404);
    }

    /**
     * Display the specified Activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
	//return Response::json(ResponseUtil::makeError('Data tidak bisa diakses'), 404);
    }

    /**
     * Show the form for editing the specified Activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
      return Response::json(ResponseUtil::makeError('Data tidak bisa diakses'), 404);
           }

    /**
     * Update the specified Activity in storage.
     *
     * @param  int              $id
     * @param UpdateActivityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityRequest $request)
    {

	return Response::json(ResponseUtil::makeError('Data tidak bisa diakses'), 404);
            }

    /**
     * Remove the specified Activity from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
       return Response::json(ResponseUtil::makeError('Data tidak bisa diakses'), 404);
    }
}
