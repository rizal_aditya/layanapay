<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Repositories\UserRepository;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\UserCriteria;
use Response;

use Illuminate\Support\Facades\Mail;
use App\Mail\Verifikasi;
use DB;


class EmailController extends Controller
{
     private $userRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo)
    {
        //$this->middleware('auth');
        $this->userRepository = $userRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->get('get');
        
        $userid = base64_decode($id); 

        $kode = explode("&", $userid);
        $email = explode("=", $kode[1]);
         $user = $this->userRepository->findWithoutFail($kode[0]);
         $name = $user->name;
        
         $pin = $user->pin;

         DB::table('users')->where('id',$userid)->update(['status'=> '1']);

          Mail::to($email[1])->send(new Verifikasi($name,$pin));


        //return view('email.verifikasi')->with(['user' => $user,'name'=>$name,'email'=>$email,'pin'=>$pin]);
        
    }
}
