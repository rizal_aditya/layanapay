<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSettingRequest;
use App\Http\Requests\UpdateSettingRequest;
use App\Repositories\SettingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use DB;
use Config;
use File;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;

class SettingController extends AppBaseController
{
    /** @var  SettingRepository */
    private $settingRepository;
    protected $url;

    public function __construct(SettingRepository $settingRepo, UrlGenerator $url)
    {
        $this->settingRepository = $settingRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Setting.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $this->settingRepository->pushCriteria(new RequestCriteria($request));
        // $setting = $this->settingRepository->all();
         $type = $request->get('type'); 
         $id  = Auth::user()->id;


	if($type != "admin")
	{

         $history = $konfirmasi =  NULL;
         if ($type == 'kuota') {
            $history = DB::table('deposit')
                         ->join('bank', 'bank.id', '=', 'deposit.bank_id')
                         ->select('bank.name as bank_name', 'deposit.*')
                         ->where('deposit.user_id', '=', $id)->get();

            $konfirmasi = DB::table('deposit')
                         ->join('bank', 'bank.id', '=', 'deposit.bank_id')
                         ->select('deposit.*', 'bank.name')
                         ->where('deposit.user_id', '=', $id)
                         ->whereNull('deposit.transfer_date')
                         ->orderBy('deposit.created_at', 'ASC')->get();
         }
         // var_dump($konfirmasi);die();
         $this->settingRepository->pushCriteria(new RequestCriteria($request));
         $settings = $this->settingRepository->findByField('user_id',$id);
         $setting = $settings[0];
         
        return view('setting.index')
            ->with(['setting'=> $setting,'type'=>$type, 'history' => $history, 'konfirmasi' => $konfirmasi]);

	}else{


	   $this->settingRepository->pushCriteria(new RequestCriteria($request));
           $settings = $this->settingRepository->paginate();
	
	  return view('setting.admin')->with(['settings'=> $settings]);	

	}
    }

    /**
     * Show the form for creating a new Setting.
     *
     * @return Response
     */
    public function create(Request $request)
    {
	$type = $request->get('type');
        return view('setting.create')->with(['type'=> $type]);
    }


  

    /**
     * Store a newly created Setting in storage.
     *
     * @param CreateSettingRequest $request
     *
     * @return Response
     */
    public function store(CreateSettingRequest $request)
    {
	$type = $request->get('type');
        $input = $request->all();

        $setting = $this->settingRepository->create($input);

        Flash::success('Setting saved successfully.');

        return view('setting.eadmin')->with('setting', $setting);
    }

    /**
     * Display the specified Setting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,Request $request)
    {
	$type = $request->get('type');
        $setting = $this->settingRepository->findWithoutFail($id);

        if (empty($setting)) {
            Flash::error('Setting not found');

             return view('setting.eadmin')->with('setting', $setting);
        }

        return view('setting.show')->with('setting', $setting);
    }

    /**
     * Show the form for editing the specified Setting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id,Request $request)
    {
        $type = $request->get('type');
	$setting = $this->settingRepository->findWithoutFail($id);
	
        if (empty($setting)) {
            Flash::error('Setting not found');

            return redirect(route('setting.index'));
        }


	if($type == "admin")
	{
			
		return view('setting.eadmin')->with('setting', $setting);

	}else{

        return view('setting.edit')->with('setting', $setting);

	}
    }

    /**
     * Update the specified Setting in storage.
     *
     * @param  int              $id
     * @param UpdateSettingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSettingRequest $request)
    {
	$type = $request->get('type');
         $input = $request->all();
        $setting = $this->settingRepository->findWithoutFail($id);

        if (empty($setting)) {
            Flash::error('Setting not found');

            return redirect(route('setting.index'));
        }

	if($type == "admin")
	{

		$setting = $this->settingRepository->update($input, $id);
       
        	Flash::success('Setting updated successfully.');
        	return redirect('setting?type=admin');



	}else{
        
        	//$user_id = Auth::user()->id;
        	//$input['user_id'] = $user_id;

        	//$setting = $this->settingRepository->update($input, $id);
       
        	//Flash::success('Setting updated successfully.');
        	//return redirect('setting?type=nominal');
        
	}
    }

    /**
     * Remove the specified Setting from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,Request $request)
    {
	$type = $request->get('type');

        $setting = $this->settingRepository->findWithoutFail($id);

        if (empty($setting)) {
            Flash::error('Setting not found');

            return redirect(route('setting.index'));
        }

        $this->settingRepository->delete($id);

        Flash::success('Setting deleted successfully.');

        return redirect('setting?type=admin');
    }

    public function postDeposit(Request $request)
    {
        $data = [
                    'user_id' => Auth::user()->id,
                    'price'  => $request->input('price'),
                    'bank_id' => 3,
                    'account_number' => $request->input('account_number'),
                    'account_name' => $request->input('account_name'),
                    'type' => $request->input('type'),
                    'status' => 0,
                    'param' => 'issuer',
                    'created_at' => date("Y-m-d H:i:s")
                ];
        try {
            DB::table('deposit')->insert($data);
            Flash::success('Setting store successfully.');
        } catch (Exception $e) {
            Flash::error($e);
        }
        return redirect('setting?type=kuota');
    }

    public function postKonfirmasi(Request $request)
    {
        $id = $request->input('id_deposit');
        $photox = base64_encode($request->file('transfer_photo'));
        $fileFormat = ".jpg";
        $userPath = base64_encode(Auth::user()->email);
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$userPath;
        $fileName = base64_encode(Auth::user()->email . time());
        $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        File::makeDirectory($files, 0777, true, true);
        if ($image = base64_decode($photox, true)) {
            $img = Image::make($image)->save($fullPath, 60);
            $url = $this->url->to('/'.$fullPath);
        } else {
            $url = null;
        }
        
        $letter = $request->input('transfer_letter');
        $data = [
                    'transfer_total' => $request->input('transfer_total'),
                    'transfer_date' => date("Y-m-d H:i:s", strtotime($request->input('transfer_date'))),
                    'transfer_letter' => empty($letter) ? NULL : $letter,
                    'transfer_photo' => $url,
                    'updated_at' => date("Y-m-d H:i:s")
                ];
        try {
            DB::table('deposit')->where('id', $id)->update($data);
            Flash::success('Setting store successfully.');
        } catch (Exception $e) {
            Flash::error($e);
        }
        return redirect('setting?type=kuota');
    }

}
