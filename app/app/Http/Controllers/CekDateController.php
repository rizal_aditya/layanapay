<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Repositories\ClientRepository;
use App\Repositories\RoleUserRepository;
use App\Http\Controllers\AppBaseController;
use App\Criteria\UserCriteria;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use file;
use App\User;
use QrCode;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeUser;

class CekDateController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    private $roleUserRepository;
    private $clientRepository;
    protected $url;

    public function __construct(UserRepository $userRepo,RoleUserRepository $roleUserRepo,UrlGenerator $url,ClientRepository $clientRepo)
    {
        $this->userRepository = $userRepo;
        $this->roleUserRepository = $roleUserRepo;
        $this->clientRepository = $clientRepo;
        $this->url = $url;
    }

     /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return User
     */
    public function index(Request $request)
    {
          echo date('Y-m-d H:i:s');
		  echo '<br>';
		  echo $date->now();;
    }

      /**
     * Show the form for creating a new User.
     *
     * @return User
     */



    public function login(Request $request)
    {
      
        
        $email = $request['email'];
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $user   = $this->userRepository->findByField('email',$email);
        

        return view('user.login')
            ->with(['user' => $user,'email'=>$email]);

    }


     public function create(Request $request)
    {

        $type = $request['type'];
        return view('user.create')
            ->with('type', $type);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return User
     */
    public function store(CreateUserRequest $request )
    {
        $type = $request['type'];
        $input = $request->all();
        
        $input['password'] = bcrypt($request->get('password'));

        $user = $this->userRepository->create($input);

        if($type != "personil")
        {

        $id = $user->id;
        $role_id = '3';
        $dataRole['user_id'] = $id;
        $dataRole['role_id'] = $role_id;
        $roles = $this->roleUserRepository->create($dataRole);

        }


        Flash::success('User saved successfully.');
        return redirect(url('user?type='.$type));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return User
     */
    public function show($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(url('user?type='.$type));
        }

        return view('user.show')->with(['user' => $user,'type'=> $type]);
        
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return User
     */



     public function edit($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);



        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');
           return redirect(url('user?type='.$type));

        }

        return view('user.edit')->with(['user'=> $user, 'type' => $type]);
        
        
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int   $id
     * @param UpdateUser $request
     *
     * @return User
     */
    


    public function update($id, UpdateUserRequest $request)
    {
        $user         = $this->userRepository->findWithoutFail($id);
        $data = $request->all();
        $type = $request['type'];
        if (empty($user)) 
        {
            Flash::error('User not found');
            return redirect(url('user?type='.$type));
        }


        $data = $request->except('password', 'password_confirmation');
        if ($request->has('password')) {
            if ($request['password']!='') {
                $data['password'] = bcrypt($request->get('password'));
            }
        }
        
        
        
         if(!empty($request->photo)){  
        
        	
	          if(!empty($user->photo))
		  {
			$file_gambar  =   $this->url->to(''.$user->photo);  
			File::delete($file_gambar);
		  }
	  
	  
	  	
		$file = $request['photo'];
		
      		$original_name = $file->getClientOriginalName();
		$file_path = $file->getPathName();
		$nama 				= gmdate('Ymd').time().'-'.'photo'.'-'.$original_name;
		$dir 				= 'files/';
		$alt				= 'YWlzYWtpQGdtYWlsLmNvbQ==/';
		$photo				= $dir.$alt.$original_name;
		$extensi 			= $this->get_ekstensi($original_name);
		$input['photo'] 			= $photo;
		
		if(move_uploaded_file($file_path, $photo))
		{
		
		
        	$user = $this->userRepository->update($input, $id);
		 
		}
        	  
	        Flash::success('User updated successfully.');
        	return redirect(url('user?type='.$type));

        }else{

        $user = $this->userRepository->update($data, $id);
	 Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));

        }

       
    }

    /**
     * Remove the specified Person from storage.
     *
     * @param  int $id
     *
     * @return User
     */

    public function enable($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $status = $request['status'];
        
         $secure = sprintf("%04d", mt_rand(1, 999999));
         $data = "LY-";
         $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
         $security = $secure.$id;
         $qrcode = $kode.$id;
         $png = QrCode::format('png')->size(100)->generate(1);
          

         $us = User::where('id', $id)->update(['pin' => $security,'kyc'=>$status,'qrcode'=>$qrcode]);

        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


     public function disable($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $status = $request['status'];
       
        $us = User::where('id', $id)->update(['pin' => '','kyc'=>$status]);

        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


     public function notification($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $input = $request['status'];
        $user = $this->userRepository->update($request->all(), $id);
        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


   

     /**
     * Remove the specified PersonUser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,Request $request)
    {
        $type = $request['type'];
        $user = $this->userRepository->findWithoutFail($id);
       // $kode = $user->id;

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(url('user?type='.$type));
        }

         $this->userRepository->delete($id);
         
         

        Flash::success('User deleted successfully.');

        return redirect(url('user?type='.$type));
    }

    public function getRegister(Request $request)
    {

        return view('auth/register');

    }

    public function postRegister(Request $request)
    {

        $input = $request->all();

        if($request->password == $request->password_confirmation)
        {
            $types = $request['type'];

            $secure = sprintf("%04d", mt_rand(1, 999999));
            $data = "LY-";
            $security = $secure;
            $date = date('Ymd');
            $secret = $secure.$data.$date;


            $api_key = bcrypt($security);
            $api_secret = bcrypt($secret);

                   if(empty($types))
                   {

                     $type = "mitra";
                     $role_id = "2";
                   }else{

                     $type = "admin";
                     $role_id = "1";
                   }


           
            $status = "0";
            
            $name = $request->name;
            $email = $request->email;
            $password = bcrypt($request->get('password'));

            $client['role_id'] = $role_id;
            $client['name'] = $name;
            $client['email'] = $email;
            $client['api_key'] = $api_key;
            $client['api_secret'] = $api_secret;
            $client['type'] = $type;
            $client['status'] = $status;


           
            $clients = $this->clientRepository->create($client);
           


            $user['client_id'] = $clients->id;
            $user['name'] = $name;
            $user['email'] = $email;
            $user['password'] = $password;
            $user['bank'] = $request->bank;
            $user['atas_nama'] = $request->atas_nama;
            $user['no_rekening'] = $request->no_rekening;
            $user['type'] = $type;
            $user['status'] = $status;

           

            $users = $this->userRepository->create($user);

            $id = $users->id;
            
            $dataRole['user_id'] = $id;
            $dataRole['role_id'] = $clients->role_id;
            $roles = $this->roleUserRepository->create($dataRole);

              Mail::to('ryzal.kazama@gmail.com')->send(new WelcomeUser('ryzal'));
           

        }else{

             Flash::error('password no matching');
        }
        
       
        return redirect(url('login'));

    }

    public function get_ekstensi($str)
    {
	$i = strrpos($str,".");
		if (!$i) { return ""; }
			$l = strlen($str) - $i;
			$ext = substr($str,$i+1,$l);
	return strtolower($ext);
   }


   
}
