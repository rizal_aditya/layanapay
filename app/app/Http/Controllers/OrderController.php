<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Repositories\TransactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\TransactionCriteria;
use Response;
use Auth;
use App\User;
use DB;


class OrderController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepo)
    {
        $this->transactionRepository = $transactionRepo;
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

       

		$cari = $request->get('search', null);
      
        $user   = Auth::user();
	$first  = DB::table('users')->select('id','name as nama_merchant','type')->where(['param'=>$user->id,'type'=>'cobranding']);
	$select  = DB::table('users')->select('id','name as nama_merchant','type')->where(['param'=>$user->id,'type'=>'merchant'])->union($first)->get();



	

        $transaction = DB::table('transaction as a')
	  ->select('a.id as kode','b.name as costumer','b.id','b.param','a.type','a.price','c.type as type_merchant','c.name as merchant','d.transaksi','d.transfer','d.admin','e.name as issuer','f.name as pengikut')
		->join('users as b', 'a.costumer_id', '=', 'b.id')
			->join('users as c', 'a.user_id', '=', 'c.id')
			   ->join('setting as d', 'd.user_id', '=', 'b.param')
		   		->join('users as e', 'e.id', '=', 'c.param')
				  ->join('users as f', 'f.id', '=', 'b.param')
				->where(['b.param'=>$user->id])->orderBy('id','desc')->paginate(10);

	

      
         return view('transaction.order')
        ->with(['transaction'=>$transaction,'select'=>$select]);

       
        
    }

     public function search(Request $request)
    {

        $cari = $request->get('search', null);
      
       if ($cari!=null)
         {
          return redirect('order?search='.$cari.'');
  
         }
    }



    /**
     * Show the form for creating a new Transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('transaction.create');
    }

    /**
     * Store a newly created Transaction in storage.
     *
     * @param CreateTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionRequest $request)
    {
        $input = $request->all();

        $transaction = $this->transactionRepository->create($input);

        Flash::success('Transaction saved successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Display the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.show')->with('transaction', $transaction);
    }

    /**
     * Show the form for editing the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.edit')->with('transaction', $transaction);
    }

    /**
     * Update the specified Transaction in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionRequest $request)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        $transaction = $this->transactionRepository->update($request->all(), $id);

        Flash::success('Transaction updated successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Remove the specified Transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        
	$transaction = DB::table('transaction')->where('id',$id)->get();
        if (empty($transaction)) {
            Flash::error('Transaksi tidak ada!');

           return redirect(url('order'));
        }

       $transaction = DB::table('transaction')->where('id',$id)->delete();

        Flash::success('Transaksi berhasil dihapus.');

        return redirect(url('order'));
    }
}
