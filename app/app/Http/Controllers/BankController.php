<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateBankRequest;
use App\Http\Requests\UpdateBankRequest;
use App\Repositories\BankRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\BankCriteria;
use Response;
use Auth;
use DB;
use Config;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;

class BankController extends AppBaseController
{
    /** @var  BankRepository */
    private $bankRepository;
    protected $url;

    public function __construct(BankRepository $bankRepo,UrlGenerator $url)
    {
        $this->bankRepository = $bankRepo;
	$this->url = $url;
    }

    /**
     * Display a listing of the Bank.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type'); 
	$user = Auth::user(); 
        //$this->bankRepository->pushCriteria(new BankCriteria($type,$user->id));
      // $this->bankRepository->pushCriteria(new RequestCriteria($request));
	if($type =="admin")
	{
       		$bank = DB::table('bank')->where(['type'=>$type,'user_id'=>$user->id])->paginate(10);
	}else{
		$bank = DB::table('bank')->where(['type'=>$type,'param'=>$user->id])->paginate(10);
	}
        return view('bank.index')
         ->with(['bank'=> $bank,'type'=>$type]);

	
    }

    /**
     * Show the form for creating a new Bank.
     *
     * @return Response
     */
    public function create(Request $request)
    {   
        $type = $request->get('type');
        return view('bank.create')
         ->with(['type'=>$type]);
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param CreateBankRequest $request
     *
     * @return Response
     */
    public function store(CreateBankRequest $request)
    {
        $input = $request->all();
        $type = $request->get('type');
        $input['type'] = $type;




        $photox = base64_encode($request->photo);
        if (empty($request->photo)) 
        {
            $photo = Config::get('app.photo_default');
            $input['image'] = $this->url->to('/'.$photo);      

        }else {

            $fileFormat = ".jpg";
            $userPath = base64_encode($request->email);
            $dir = Config::get('elfinder.dir')[0];
            $files = $dir."/".$userPath;
            $fileName = base64_encode($request->email . time());
            $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
            File::makeDirectory($files, 0777, true, true);

                if ($image = base64_decode($photox, true))
                {
                    $img = Image::make($image)->save($fullPath, 60);
                    $input['image'] = $this->url->to('/'.$fullPath);
                } else {
                    return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                }
             
        }


        $bank = $this->bankRepository->create($input);

        Flash::success('Bank saved successfully.');

        return redirect('bank/?type='.$type);
    }

    /**
     * Display the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,Request $request)
    {
        $bank = $this->bankRepository->findWithoutFail($id);
        $type = $request->get('type');
        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect('bank/?type='.$type);
        }

        return view('bank.show')->with('bank', $bank);
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id,Request $request)
    {
        $bank = $this->bankRepository->findWithoutFail($id);
        $type = $request->get('type');
        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect('bank/?type='.$type);
        }

        return view('bank.edit')->with(['bank'=>$bank,'type'=>$type]);
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param  int              $id
     * @param UpdateBankRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBankRequest $request)
    {
         $input = $request->all();
        $bank = $this->bankRepository->findWithoutFail($id);
        $type = $request->get('type');
        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect('bank/?type='.$type);
        }



        $bank->type = $type;

        $photox = base64_encode($request->photo);
        if (empty($request->photo)) 
        {
            $photo = Config::get('app.photo_default');
            $input['image'] = $this->url->to('/'.$photo);      

        }else {
		

	     if($bank->image != "http://apiv2.kelasdewa.com/img/avatar.png")
	     {
	
             	$image1 = $bank->image;   
             	unlink($image1);
	     }

            $fileFormat = ".jpg";
            $userPath = base64_encode($request->email);
            $dir = Config::get('elfinder.dir')[0];
            $files = $dir."/".$userPath;
            $fileName = base64_encode($request->email . time());
            $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
            File::makeDirectory($files, 0777, true, true);

                if ($image = base64_decode($photox, true))
                {
                    $img = Image::make($image)->save($fullPath, 60);
                    $input['image'] = $this->url->to('/'.$fullPath);
                } else {
                    return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                }
             
        }


        $bank = $this->bankRepository->update($input, $id);

        Flash::success('Bank updated successfully.');

        return redirect('bank/?type='.$type);
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bank = $this->bankRepository->findWithoutFail($id);
        $type = $request->get('type');
        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect('bank/?type='.$type);
        }

        $this->bankRepository->delete($id);


         $image = $bank->image;   
         unlink($image);

        Flash::success('Bank deleted successfully.');

        return redirect('bank/?type='.$type);
    }
}
