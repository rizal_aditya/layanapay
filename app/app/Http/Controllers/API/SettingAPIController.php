<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSettingAPIRequest;
use App\Http\Requests\API\UpdateSettingAPIRequest;
use App\Models\Setting;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;


/**
 * Class SettingController
 * @package App\Http\Controllers\API
 */

class SettingAPIController extends AppBaseController
{
    /** @var  SettingRepository */
    private $settingRepository;

    public function __construct(SettingRepository $settingRepo)
    {
        $this->settingRepository = $settingRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/setting",
     *      summary="Get a listing of the Setting.",
     *      tags={"Setting"},
     *      description="Get all Setting",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Setting")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->settingRepository->pushCriteria(new RequestCriteria($request));
        $this->settingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $setting = $this->settingRepository->all();

        return $this->sendResponse($setting->toArray(), 'Setting retrieved successfully');
        
        





    }



    public function checkNominal(Request $request)
    {

	$users  = Auth::user();
	$param = $users->param;	
       	$type = $request->type;

	$issuer = DB::table('users')->where('id',$param)->get();


               if($users->kyc =="0")
                {
			if($type =="deposite")
			{		
                    		$setting = DB::table('setting')->select('user_id','mindepn as min','maxdepn as max')->where(['user_id'=>$issuer[0]->id])->get();
                    	
			}else if($type =="withdraw"){
				
				$setting = DB::table('setting')->select('user_id','minwdn as min','maxwdn as max')->where(['user_id'=>$issuer[0]->id])->get();
			
			}else if($type == "transaksi"){

				$setting = DB::table('setting')->select('user_id','mintrn as min','maxtrn as max')->where(['user_id'=>$issuer[0]->id])->get();

			}

                }else if($users->kyc =="1"){
			
			if($type =="deposite")
			{

                    		$setting = DB::table('setting')->select('user_id','mindepk as min','maxdepk as max')->where(['user_id'=>$issuer[0]->id])->get();
			
			}else if($type =="withdraw"){

				$setting = DB::table('setting')->select('user_id','minwdk as min','maxwdk as max')->where(['user_id'=>$issuer[0]->id])->get();
			
			}else if($type == "transaksi"){

				$setting = DB::table('setting')->select('user_id','mintrk as min','maxtrk as max')->where(['user_id'=>$issuer[0]->id])->get();
			
			}
                }   

        return $this->sendResponse($setting, 'Setting retrieved successfully');


     }


    /**
     * @param CreateSettingAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/setting",
     *      summary="Store a newly created Setting in storage",
     *      tags={"Setting"},
     *      description="Store Setting",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Setting that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Setting")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Setting"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSettingAPIRequest $request)
    {
        $input = $request->all();

        $setting = $this->settingRepository->create($input);

        return $this->sendResponse($setting->toArray(), 'Setting saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/setting/{id}",
     *      summary="Display the specified Setting",
     *      tags={"Setting"},
     *      description="Get Setting",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Setting",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Setting"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Setting $setting */
        $setting = $this->settingRepository->find($id);

        if (empty($setting)) {
            return Response::json(ResponseUtil::makeError('Setting not found'), 404);
        }

        return $this->sendResponse($setting->toArray(), 'Setting retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSettingAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/setting/{id}",
     *      summary="Update the specified Setting in storage",
     *      tags={"Setting"},
     *      description="Update Setting",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Setting",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Setting that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Setting")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Setting"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSettingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Setting $setting */
        $setting = $this->settingRepository->find($id);

        if (empty($setting)) {
            return Response::json(ResponseUtil::makeError('Setting not found'), 404);
        }

        $setting = $this->settingRepository->update($input, $id);

        return $this->sendResponse($setting->toArray(), 'Setting updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/setting/{id}",
     *      summary="Remove the specified Setting from storage",
     *      tags={"Setting"},
     *      description="Delete Setting",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Setting",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Setting $setting */
        $setting = $this->settingRepository->find($id);

        if (empty($setting)) {
            return Response::json(ResponseUtil::makeError('Setting not found'), 404);
        }

        $setting->delete();

        return $this->sendResponse($id, 'Setting deleted successfully');
    }
}
