<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTransactionAPIRequest;
use App\Http\Requests\API\UpdateTransactionAPIRequest;
use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use App\Repositories\SettingRepository;
use App\Repositories\UserRepository;
use App\Repositories\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Depuser;
use App\Mail\DepAdmin;
use App\Mail\WelcomeUser;
use App\Mail\Blokir;
use App\Mail\Transaksi;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;
use Config;
use File;

/**
 * Class TransactionController
 * @package App\Http\Controllers\API
 */

class TransactionAPIController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;
    private $userRepository;
    private $packetRepository;
    private $settingRepository;
    protected $url;

    public function __construct(TransactionRepository $transactionRepo,UserRepository $userRepo,PacketRepository $packetRepo,SettingRepository $settingRepo,UrlGenerator $url)
    {
        $this->transactionRepository = $transactionRepo;
        $this->userRepository = $userRepo;
        $this->packetRepository = $packetRepo;
        $this->settingRepository = $settingRepo;
        $this->url = $url;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/transaction",
     *      summary="Get a listing of the Transaction.",
     *      tags={"Transaction"},
     *      description="Get all Transaction",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Transaction")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->transactionRepository->pushCriteria(new RequestCriteria($request));
        $this->transactionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $transaction = $this->transactionRepository->all();


        return $this->sendResponse($transaction->toArray(), 'Transaction retrieved successfully');
    }

    public function historyBank(Request $request)
    {
       $id = Auth::user()->id;

       $transaction = DB::table('transaction')->where('user_id',$id)->get();
       if(empty($transaction))
       {

             $bank = DB::table('bank')->where(['user_id'=>$id,'type'=>'primary'])->first();
             if(!empty($bank))
             {

                 $transaction = DB::table('bank as b')->select('b.id','b.name','b.account_name','b.account_number','b.type','b.status')->where(['b.user_id'=>$id,'b.type'=>'primary'])->get();

                return $this->sendResponse($transaction, 'Transaction retrieved successfully');
             }else{

                return Response::json(ResponseUtil::makeError('Bank Utama Belum di isi!'), 404);
             }



       }else{

         $transaction = DB::table('bank as b')->select('b.id','b.name','b.account_name','b.account_number','b.type','b.status')->join('transaction as a', 'a.bank_id', '=', 'b.id')->where('a.user_id',$id) ->limit(5)->groupBy('b.account_number')->get();
         
          return $this->sendResponse($transaction, 'Transaction retrieved successfully');

       } 
      
    }

    /**
     * @param CreateTransactionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/transaction",
     *      summary="Store a newly created Transaction in storage",
     *      tags={"Transaction"},
     *      description="Store Transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Transaction that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Transaction")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */



public function generate(CreateTransactionAPIRequest $request)
{

        //$input = $request->all();

        $qrcode = $request->qrcode;
       
        $security = $request->pin;
        $type = 'generate';
        $tanggal = $request->tanggal;

        // saldo costumer
        $costumer = Auth::user();
        $costumer_id = $costumer->id;
        $saldo_costumer = $costumer->saldo;
        $pin  = $costumer->pin;
        $costumer_name = $costumer->name;

        $saldoiss = DB::table('setting')->where('user_id',$costumer->param)->get();
        if($saldoiss[0]->saldo =="0")
        {

             return Response::json(ResponseUtil::makeError('server terjadi masalah coba beberapa saat lagi!'), 404);  

        }else{



                if($security == $pin){
                    if($saldo_costumer =="0")
                    {

                            return Response::json(ResponseUtil::makeError('Saldo 0 Transaksi tidak diperbolehkan!'), 404);        
                    }else{  

                                // generate mitra 
                                $qr = DB::table('generate')->where('code',$qrcode)->get();            
                                if(empty($qr[0]->user_id))
                                {
                                
                                   $qr_user = "";
                                   $qr_nominal = "";

                                   return Response::json(ResponseUtil::makeError('Tidak ada generate QR!'), 404); 

                                }else{    

                                    $qr_user  = $qr[0]->user_id;
                                    $qr_nominal  = $qr[0]->nominal;

                                }

                                //mitra generate param id issuer
                                $param = DB::table('users')->where('id',$qr_user)->get();
                                $issuer = $param[0]->param;                              
                                $setting = DB::table('setting')->where('user_id',$issuer)->get();
                                // transaksi non kyc
                                $tr_nonkycmax = $setting[0]->maxtrn;
                                // transaksi  kyc
                                $tr_kycmax = $setting[0]->maxtrk;

                                if($qr[0]->nominal > $saldo_costumer)
                                {

                                    return Response::json(ResponseUtil::makeError('Saldo tidak cukup Transaksi tidak diperbolehkan!'), 404);  

                                }else{ 

                                    if($costumer->kyc =="0") // non kyc
                                    {

                                        if($saldo_costumer < $tr_nonkycmax){

                                        // saldo mitra    
                                        //$mitra       = User::where('id',$qr_user)->get();
                                        $mitra_name  = $param[0]->name;
                                        $saldo_mitra = $param[0]->saldo;
					$type_mitra  = $param[0]->type;

                                        $admerchant = DB::table('bank')->get();
                                       // $bayar = "bayar";
                                       // $this->loghistory($bayar);    
                                        // cek bayar 5x
                                   
                                        if(empty($admerchant))
                                        {

                                            return Response::json(ResponseUtil::makeError('Bank distributor tidak diperbolehkan!'), 404);

                                        }else{
                                        
                                             
                                            	//transaksi qrpay
     						$rpqrpay = DB::table('setting')->where(['user_id'=>$issuer])->get();


							//bayar 
							if($type_mitra == "merchant")
							{
								//potongan untuk merchant	
								$biayaiusr = $rpqrpay[0]->issuertrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$qr_nominal-$potongan; 


								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
				  



							}

							if($type_mitra == "cobranding")
							{

								//potongan untuk cobrading	
								$biayacobranding = $rpqrpay[0]->cobrandingtrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayacobranding;
                                                		$total_mitra = $saldo_mitra+$qr_nominal-$potongan; 

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
				  



							}

						//free biaya
						if($rpqrpay[0]->count !="0")
						{
								$total_count = $rpqrpay[0]->count-1;
								$potongan = "0";
                                                		$total_mitra = $saldo_mitra+$qr_nominal;

						}else{

							


							if($type_mitra == "costumer")
							{
								
								//potongan untuk penerima transfer	
								$biayaiusr = $rpqrpay[0]->issuertrf;		
								$potongan = $rpqrpay[0]->admintrf+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$qr_nominal-$potongan;
								$fee = $potongan;


                                                		//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrf;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrf;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
	

							}

						}//end biaya
                                        }   
 
				  
					
				  

				    

				    


                                    $updateMitra = User::where('id', $qr_user)->update(['saldo' => $total_mitra]);

                                    // saldo costumer
                                    $total_costumer = $saldo_costumer-$qr_nominal;
                                    $updateCostumer = User::where('id', $costumer_id)->update(['saldo' => $total_costumer]);

                                    //history transaksi
                                    $input['costumer_id'] = $costumer_id; // input costumer
                                    $input['user_id']     = $qr_user; // input user
                                    $input['price']       = $qr_nominal; // input price
                                    $input['status']      = "1"; // input status
                                    $input['type']        = "generate";
                                    $input['tanggal']     = date("Y-m-d");
                                    $input['param']       = $issuer; // transaksi id issuer 

                                        if($costumer_id != $qr_user)
                                        {
                                             $transaction = $this->transactionRepository->create($input);   
                                        }else{

                                            return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan!'), 404);
                                        }

                                    // create history
                                    $this->create_h_transaction($transaction->id,$qr_nominal,$mitra_name,$costumer_name,$costumer_id,$qr_user,$potongan,$type);
                                
                                
                                   $history = DB::table('history')->where(['transaction_id'=>$transaction->id,'user_id'=>$costumer_id])->first();
                                   return $this->sendResponse($history, 'Transaksi berhasil');
                            		
                                    }else{
                                        return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan Check Status anda!'), 404);
                                    }



                                    }else if($costumer->kyc =="1"){ // user kyc
                                            if($saldo_costumer < $tr_kycmax)
                                            {
                                            // saldo mitra    
                                            $mitra       = User::where('id',$qr_user)->get();
                                            $mitra_name  = $mitra[0]->name;
                                            $saldo_mitra = $mitra[0]->saldo; 
					    $type_mitra  = $param[0]->type;

                                            $admerchant = DB::table('bank')->get();
                                            //$bayar = "bayar";
                                            //$this->loghistory($bayar);    
                                                // cek bayar 5x
                                           
                                            if(empty($admerchant))
                                            {

                                                return Response::json(ResponseUtil::makeError('Bank distributor tidak diperbolehkan!'), 404);

                                            }else{
                                                
                                           	//transaksi qrpay
     						$rpqrpay = DB::table('setting')->where(['user_id'=>$issuer])->get();
						//free biaya

						//bayar 

							if($type_mitra == "merchant")
							{

                                                		//potongan untuk merchant	
								$biayaiusr = $rpqrpay[0]->issuertrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$qr_nominal-$potongan; 
								

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
 

							}

							if($type_mitra == "cobranding")
							{

                                                		//potongan untuk cobrading	
								$biayacobranding = $rpqrpay[0]->cobrandingtrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayacobranding;
                                                		$total_mitra = $saldo_mitra+$qr_nominal-$potongan; 

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
				  
 

							}

						if($rpqrpay[0]->count !="0")
						{
								$total_count = $rpqrpay[0]->count-1;
								$potongan = "0";
                                                		$total_mitra = $saldo_mitra+$qr_nominal;


						}else{

							


							if($type_mitra == "costumer")
							{

                                //potongan untuk penerima transfer	
								$biayaiusr = $rpqrpay[0]->issuertrf;		
								$potongan = $rpqrpay[0]->admintrf+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$qr_nominal-$potongan;

                                                		//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrf;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrf;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);

							}


                                                  }  

                                            }     


				    
					
				   
                                           
                                            $updateMitra = User::where('id', $qr_user)->update(['saldo' => $total_mitra]);

                                            // saldo costumer
                                            $total_costumer = $saldo_costumer-$qr_nominal;
                                            $updateCostumer = User::where('id', $costumer_id)->update(['saldo' => $total_costumer]);

                                            //history transaksi
                                            $input['costumer_id'] = $costumer_id; // input costumer
                                            $input['user_id']     = $qr_user; // input user
                                            $input['price']       = $qr_nominal; // input price
                                            $input['status']      = "1"; // input status
                                            $input['type']        = "tr-generate";
                                            $input['tanggal']     =  date("Y-m-d");
                                            $input['param']       = $issuer; // transaksi id issuer

                                            if($costumer_id != $qr_user)
                                            {
                                                 $transaction = $this->transactionRepository->create($input);   
                                            }else{

                                                return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan!'), 404);
                                            }

                                             //create history
                                           $this->create_h_transaction($transaction->id,$qr_nominal,$mitra_name,$costumer_name,$costumer_id,$qr_user,$potongan,$type);

                                        
                                            $history = DB::table('history')->where(['transaction_id'=> $transaction->id,'user_id'=>$costumer_id])->first();

                                            // $delgenerate = DB::table('generate')->where(['code'=>$qrcode])->delete();

                                            return $this->sendResponse($history, 'Anda melakukan pembayaran ke '.$mitra_name.'. Terimakasih sudah menggunakan QRPay.id untuk solusi pembayaran Anda');
                           
                                    
                               
                                        }else{
                                             return Response::json(ResponseUtil::makeError('Saldo tidak cukup Transaksi tidak diperbolehkan!'), 404);
                                        }

                                    }else if($costumer->status =="2"){
                                        return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan akun diblokir!'), 404);
                                    } 

                                }    

                    } 
                    

                }else{

                            $blokir = "blokir";
                            $pintgl = DB::table('activity')->where(['type'=>$blokir,'user_id'=>$costumer_id])->get();    
                            if(empty($pintgl[0]))
                            {
                                 $this->loghistory($blokir);  
                                 return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan, check pin transaksi anda!'), 404);

                            
                            }else if($pintgl[0]->count =='4'){
                                  
                                    

                                         $updateLog = DB::table('users')->where('id', $costumer_id)->update(['status' => '2']);   
                                         $this->emailblokir();
                                         return Response::json(ResponseUtil::makeError('Akun anda telah di non aktifkan, kami menemukan aktitas mencurigakan. Cek email untuk mengaktifkan akun anda!'), 404);

                                  

                            }else{

                                $this->loghistory($blokir);  
                                return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan, check pin transaksi anda!'), 404);

                            }  


                }      
        }
    }   


    public function nongenerate(CreateTransactionAPIRequest $request)
    {  
       // $input = $request->all();

        $qrcode = $request->qrcode;
        $price = $request->price;
        $security = $request->pin;
        $type = 'nongenerate';
        $tanggal = $request->tanggal;

        // saldo costumer
        $costumer = Auth::user();
        $costumer_id = $costumer->id;
        $saldo_costumer = $costumer->saldo;
        $pin  = $costumer->pin;
        $costumer_name = $costumer->name;

       
    

        $saldoiss = DB::table('setting')->where('user_id',$costumer->param)->get();
        if($saldoiss[0]->saldo =="0")
        {

             return Response::json(ResponseUtil::makeError('server terjadi masalah coba beberapa saat lagi!'), 404);  

        }else{       



    if($security == $pin)
    {
        if($saldo_costumer =="0")
        {

                return Response::json(ResponseUtil::makeError('Saldo 0 Transaksi tidak diperbolehkan!'), 404);        
        }else{ 



        if($price > $saldo_costumer)
        {

            return Response::json(ResponseUtil::makeError('Saldo tidak cukup Transaksi tidak diperbolehkan!'), 404);

        }else{


                    $user = $this->userRepository->findByField('qrcode', $qrcode);
                    $mitra_id  = $user[0]->id;
                    $mitra_name  = $user[0]->name;
                    $saldo_mitra = $user[0]->saldo; 
		    $type_mitra  = $user[0]->type;

                    //mitra generate param id issuer
                    $param = DB::table('users')->where('id',$mitra_id)->get();
                    $issuer = $param[0]->param;                              
                    $setting = DB::table('setting')->where('user_id',$issuer)->get();
                    // transaksi non kyc
                    $tr_nonkycmax = $setting[0]->maxtrn;
                    // transaksi  kyc
                    $tr_kycmax = $setting[0]->maxtrk;

                    // non kyc transaksi biasa
                            if($costumer->kyc =="0"){ 
                                
                                if($saldo_costumer < $tr_nonkycmax){

                                    
                                        
                                    $admerchant = DB::table('bank')->get();
                                    //$bayar = "bayar";
                                    //$this->loghistory($bayar);    
                                        // cek bayar 5x
                                   
                                    if(empty($admerchant))
                                    {

                                        return Response::json(ResponseUtil::makeError('Bank distributor tidak diperbolehkan!'), 404);

                                    }else{
                                        
                                    
                                    		//transaksi qrpay
     						$rpqrpay = DB::table('setting')->where(['user_id'=>$issuer])->get();
						//free biaya

						//bayar
							if($type_mitra == "merchant")
							{

                                                		
								//potongan untuk merchant	
								$biayaiusr = $rpqrpay[0]->issuertrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$price-$potongan; 
								

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
 



							}

							if($type_mitra == "cobranding")
							{

                                                		
								//potongan untuk cobrading	
								$biayacobranding = $rpqrpay[0]->cobrandingtrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayacobranding;
                                                		$total_mitra = $saldo_mitra+$price-$potongan; 

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
				  



							}


						if($rpqrpay[0]->count !="0")
						{

							$total_count = $rpqrpay[0]->count-1;

                                                	$fee = "0";
                                                	$total_mitra = $saldo_mitra+$price; 

							


						}else{


							



							if($type_mitra == "costumer")
							{

                                                		 

								//potongan untuk penerima transfer	
								$biayaiusr = $rpqrpay[0]->issuertrf;		
								$potongan = $rpqrpay[0]->admintrf+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$price-$potongan;

                                                		//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrf;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrf;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);


							}

                                        
						}//end biaya
 

                                    }     

				    
					
				    $duit_isuer = $rpqrpay[0]->transfer-$rpqrpay[0]->admin+$rpqrpay[0]->saldo;
                                    $updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['count' => $total_count,'saldo'=>$duit_isuer]);
				    //tambah fee layana
				    $ceo = "2";
				    $saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    $duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admin;
				    $updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);


                                     $updateMitra = User::where('id', $mitra_id)->update(['saldo' => $total_mitra]);


                                    //saldo costumer
                                    $total_costumer = $saldo_costumer-$price;
                                    $updateCostumer = User::where('id', $costumer_id)->update(['saldo' => $total_costumer]);

                                    //history transaksi
                                    $input['costumer_id'] = $costumer_id; // input costumer
                                    $input['user_id']     = $mitra_id; // input user
                                    $input['price']       = $price; // input price
                                    $input['status']      = "1"; // input status
                                    $input['type']        = "nongenerate";
                                    $input['tanggal']     =  date("Y-m-d");
                                    $input['param']       = $user[0]->param; // transaksi id issuer

                                    if($costumer_id != $mitra_id)
                                    {
                                         $transaction = $this->transactionRepository->create($input);   
                                    }else{

                                        return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan!'), 404);
                                    }


                                    // create history
                                    $this->create_h_transaction($transaction->id,$price,$mitra_name,$costumer_name,$costumer_id,$mitra_id,$potongan,$type);

                                    $history = DB::table('history')->where(['transaction_id'=> $transaction->id,'user_id'=>$costumer_id])->first();
                                    return $this->sendResponse($history, 'Anda melakukan pembayaran ke '.$mitra_name.'. Terimakasih sudah menggunakan QRPay.id untuk solusi pembayaran Anda');

                                // check saldo    
                                }else{
                                         return Response::json(ResponseUtil::makeError('Saldo tidak cukup Transaksi tidak diperbolehkan!'), 404);
                                }

                                
                            }else{ // kyc 

                                if($saldo_costumer < $tr_kycmax){


                                    $user = $this->userRepository->findByField('qrcode', $qrcode);
                                    $mitra_id  = $user[0]->id;
                                    $mitra_name  = $user[0]->name;
                                    $saldo_mitra = $user[0]->saldo; 
                                    $type_mitra  = $user[0]->type;

                                    $admerchant = DB::table('bank')->get();
                                    //$bayar = "bayar";
                                    //$this->loghistory($bayar);    
                                        // cek bayar 5x
                                   
                                    if(empty($admerchant))
                                    {

                                        return Response::json(ResponseUtil::makeError('Bank distributor tidak diperbolehkan!'), 404);

                                    }else{
                                        
                                         	//transaksi qrpay
     						$rpqrpay = DB::table('setting')->where(['user_id'=>$issuer])->get();
						//free biaya

							if($type_mitra == "merchant")
							{

                                                		//potongan untuk merchant	
								$biayaiusr = $rpqrpay[0]->issuertrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$price-$potongan; 
								

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
 


 

							}

							if($type_mitra == "cobranding")
							{

                                                		
								//potongan untuk cobrading	
								$biayacobranding = $rpqrpay[0]->cobrandingtrx;		
								$potongan = $rpqrpay[0]->admintrx+$biayacobranding;
                                                		$total_mitra = $saldo_mitra+$price-$potongan; 

								//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrx;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrx;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);
				  


							}

						if($rpqrpay[0]->count !="0")
						{
								$total_count = $rpqrpay[0]->count-1;
								$potongan = "0";
                                                		$total_mitra = $saldo_mitra+$price;

						}else{
						
							


							if($type_mitra == "costumer")
							{

                                                		//potongan untuk penerima transfer	
								$biayaiusr = $rpqrpay[0]->issuertrf;		
								$potongan = $rpqrpay[0]->admintrf+$biayaiusr;
                                                		$total_mitra = $saldo_mitra+$price-$potongan;

                                                		//tambah fee layana
				    				$ceo = "2";
				   				$saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    				$duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admintrf;
				    				$updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);

								//tambah fee untuk issuer
								$saldoissuer = DB::table('setting')->where(['user_id'=>$issuer])->get();
								$duit_isuer = $saldoissuer[0]->saldo+$rpqrpay[0]->issuertrf;
                                    				$updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['saldo'=>$duit_isuer]);

 

							}

                                               }                                                                     
                                         	

                                    }   
                                     
				    
					
				    $duit_isuer = $rpqrpay[0]->transfer-$rpqrpay[0]->admin+$rpqrpay[0]->saldo;
                                    $updgratis = DB::table('setting')->where(['user_id'=>$issuer])->update(['count' => $total_count,'saldo'=>$duit_isuer]);
				    //tambah fee layana
				    $ceo = "2";
				    $saldoceo = DB::table('setting')->where(['user_id'=>$ceo])->get();
				    $duitceo = $saldoceo[0]->saldo+$rpqrpay[0]->admin;
				    $updfeeceo = DB::table('setting')->where(['user_id'=>$ceo])->update(['saldo' => $duitceo]);
     
                                     $updateMitra = User::where('id', $mitra_id)->update(['saldo' => $total_mitra]);

                                   
                                    // saldo costumer
                                    $total_costumer = $saldo_costumer-$price;
                                    $updateCostumer = User::where('id', $costumer_id)->update(['saldo' => $total_costumer]);

                                    //history transaksi
                                    $input['costumer_id'] = $costumer_id; // input costumer
                                    $input['user_id']     = $mitra_id; // input user
                                    $input['price']       = $price; // input price
                                    $input['status']      = "1"; // input status
                                    $input['type']        = "transaction";
                                    $input['tanggal']     =  date("Y-m-d");
                                    $input['param']       = $user[0]->param; // transaksi id issuer

                                        if($costumer_id != $mitra_id)
                                        {
                                             $transaction = $this->transactionRepository->create($input);   
                                        }else{

                                            return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan!'), 404);
                                        }

                                      // create history
                                    $this->create_h_transaction($transaction->id,$price,$mitra_name,$costumer_name,$costumer_id,$mitra_id,$potongan,$type);    
                                
                                    $history = DB::table('history')->where(['transaction_id'=> $transaction->id,'user_id'=>$costumer_id])->first();
                                    return $this->sendResponse($history, 'Anda melakukan pembayaran ke '.$mitra_name.'. Terimakasih sudah menggunakan QRPay.id untuk solusi pembayaran Anda');


                                }else{

                                    return Response::json(ResponseUtil::makeError('Saldo tidak cukup Transaksi tidak diperbolehkan!'), 404);
                                }
                            }

       }


   }//saldo tidak nol

    }else{ // blokir

                $blokir = "blokir";
                $pintgl = DB::table('activity')->where(['type'=>$blokir,'user_id'=>$costumer_id])->get();    
                if(empty($pintgl[0]))
                {
                     $this->loghistory($blokir);  
                     return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan, check pin transaksi anda!'), 404);

                
                }else if($pintgl[0]->count =='4'){
                      
                        

                             $updateLog = DB::table('users')->where('id', $costumer_id)->update(['status' => '2']);   
                             $this->emailblokir();
                             return Response::json(ResponseUtil::makeError('Akun anda telah di non aktifkan, kami menemukan aktitas mencurigakan. Cek email untuk mengaktifkan akun anda!'), 404);

                      

                }else{

                    $this->loghistory($blokir);  
                    return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan, check pin transaksi anda!'), 404);

                } 

    } 

 }

}

 

public function deposite(CreateTransactionAPIRequest $request)
{

        $input = $request->all();

       
        $price = $request->price;
    
        $type = 'deposite';
        //$tanggal = $request->tanggal;

                $user = Auth::user();
                $packet = $this->packetRepository->find($request->packet_id);
        
                $merchant = DB::table('users')->where('id',$user->param)->get();
                $setting =  DB::table('setting')->where('user_id',$merchant[0]->id)->get();
                // deposite non kyc
                $tr_nonkycmax = $setting[0]->maxdepn;
                // deposite kyc
                $tr_kycmax = $setting[0]->maxdepk;


                if($user->kyc =="0")
                {


                  

                   //costumer biasa
                   if($price < $tr_nonkycmax)
                   {

                    if($packet->name == "Bebas")
                    {

                    $input['packet_id'] = $request->packet_id;
                    $input['price'] = $request->price;
                    $input['denom'] = $request->price;
                    $input['bank_id'] = $request->bank_id;
                    $input['bank_send'] = $request->bank_send;
                    $input['user_id'] = $user->id;
                    $input['last_balance'] = $user->saldo;
                    $input['tanggal'] = $request->tanggal;
                    $input['type'] = $type;
                    $input['status'] = '0';
                    $input['param']  = '0';
                    
                    }else{
                    
                    $input['packet_id'] = $request->packet_id;
                    $input['price'] = $packet->price;
                    $input['denom'] = $packet->denom;
                    $input['bank_id'] = $request->bank_id;
                    $input['bank_send'] = $request->bank_send;
                    $input['user_id'] = $user->id;
                    $input['last_balance'] = $user->saldo;
                     $input['tanggal'] = $request->tanggal;
                    $input['status'] = '0';
                     $input['type'] = $type;
                    $input['param']  = '0';

                    }


                    $transaction = $this->transactionRepository->create($input);
                     $this->emaildeposite($transaction->price,$transaction->created_at,$transaction->bank_send);
                     
                    
                    	

                    $price =  number_format($transaction->price,0,'','.');
                    // history to plus
                    $insert = DB::table('history')->insert(
                        [ 
                            [
                                'transaction_id' => $transaction->id,
                                'bank_id'     => $transaction->bank_id,
                                'tanggal'     => $request->tanggal,
                                'bank_send'     => $transaction->bank_send,
                                'price'        =>  $price,
                                'description' => 'Tambah Saldo Rp '.$price.' ( Proses )',
                                'user_id' => $user->id,
                                'param'=> '0',
                                'type' => 'deposite',
                                'status' => '0',
                                'created_at' => DB::raw('now()')
                            ],
                        ]);
      
                        $history = DB::table('history')->where('transaction_id', $transaction->id)->first();
                       return $this->sendResponse($history, 'Deposit berhasil');

                       
                        
                        


                   }else{

                    return Response::json(ResponseUtil::makeError('Tambah Saldo tidak boleh diatas Rp 1.000.000'), 404);

                   } 


                }else{    

                    //costumer lengkap
                    if($price < $tr_kycmax)
                    {


                        if($packet->name == "Bebas")
                        {

                        $input['packet_id'] = $request->packet_id;
                        $input['price'] = $request->price;
                        $input['denom'] = $request->price;
                        $input['bank_id'] = $request->bank_id;
                        $input['bank_send'] = $request->bank_send;
                        $input['user_id'] = $user->id;
                        $input['tanggal'] = $request->tanggal;
                        $input['last_balance'] = $user->saldo;
                        $input['status'] = '0';
                       $input['type'] = $type;
                         $input['param']  = '0';
                        }else{
                    
                        $input['packet_id'] = $request->packet_id;
                        $input['price'] = $packet->price;
                        $input['denom'] = $packet->denom;
                        $input['bank_id'] = $request->bank_id;
                        $input['bank_send'] = $request->bank_send;
                        $input['user_id'] = $user->id;
                        $input['tanggal'] = $request->tanggal;
                        $input['last_balance'] = $user->saldo;
                        $input['status'] = '0';
                        $input['type'] = $type;
                        $input['param']  = '0';
                        }


                    $transaction = $this->transactionRepository->create($input);
                     $this->emaildeposite($transaction->price,$transaction->created_at,$transaction->bank_send);

                     $tgl = explode(' ',$transaction->created_at);
                    
                     
                    $price =  number_format($transaction->price,0,'','.');
                    // history to plus
                    $insert = DB::table('history')->insert(
                        [ 
                            [
                                'transaction_id' => $transaction->id,
                                'bank_id'     => $transaction->bank_id,
                                'tanggal'     => $request->tanggal,
                                'bank_send'     => $transaction->bank_send,
                                'price'        =>   $request->price,
                                'description' => 'Tambah Saldo Rp '.$transaction->price.' ( Proses )',
                                'user_id' => $user->id,
                                'type' => 'deposite',
                                 'param'=> $user->param,
                                 'admin' => '0',
                                'status' => '0',
                                'created_at' => DB::raw('now()')
                            ],
                        ]);
      
                        $history = DB::table('history')->where('transaction_id', $transaction->id)->first();
                        return $this->sendResponse($history, 'Deposit berhasil');

   


                    }else{

                    return Response::json(ResponseUtil::makeError('Tambah Saldo tidak boleh diatas Rp 5.000.000'), 404);

                   }    
                }

}


public function withdraw(CreateTransactionAPIRequest $request)
{

                $users    = Auth::user();
                $saldo  = $users->saldo;
                                 
                $kyc    = $users->kyc;
                $param    = $users->param;
                $price   = $request->price;
                $security   = $request->pin;

                $pin = $users->pin;

                $merchant = DB::table('users')->where('id',$users->param)->get();
                $setting =  DB::table('setting')->where('user_id',$merchant[0]->id)->get();
                // WD max kyc
                $tr_kycmax = $setting[0]->maxwdk;
                // WD min kyc
                $tr_kycmin = $setting[0]->minwdk;
		$bank = DB::table('bank')->where(['type'=>'primary','user_id'=>$users->id])->get();
		$bank_send = DB::table('bank')->where('user_id',$users->param)->get();

            if($saldo =="0")
            {

                return Response::json(ResponseUtil::makeError('Withdraw tidak diperbolehkan, check saldo anda!'), 404);

            }else{

                if($saldo < $price)
                {

                    return Response::json(ResponseUtil::makeError('Withdraw tidak diperbolehkan, saldo anda tidak mencukupi!'), 404);

                }else{   

                    if($security == $pin)
                    {          

                        if($kyc =="1")
                        { 
                              
                           

                            if(!empty($bank[0]))
                            {


                                if($bank[0]->user_id == $users->id)
                                {

                                    if($price < $tr_kycmin){

                                        return Response::json(ResponseUtil::makeError('Penarikan Saldo tidak dapat dilakukan, periksa saldo atau jumlah yang ditarik, anda hanya bisa tarik tunai minimal Rp 100.000 !'), 404);

                                    }else if($price > $tr_kycmax){

                                        return Response::json(ResponseUtil::makeError('Penarikan Saldo tidak dapat dilakukan, periksa saldo atau jumlah yang ditarik, anda hanya bisa tarik tunai maksimal Rp 10.000.000'), 404);

                                    }else{ 


                            	 	

                                    $input['user_id']       = $users->id; 
                                    $input['price']         = $price;
                                    $input['denom']         = $price;
                                    $input['last_balance']  = $saldo;
                                    $input['tanggal']       = date("Y-m-d");
                                    $input['type']          = "withdraw";
                                    $input['bank_id']       = $bank[0]->id;
                                    $input['bank_send']     = $bank_send[0]->id;
                                    $input['packet_id']     = '5';
                                    $input['status']        = "0";
                                    $input['param']         = "0";

                                    $transaction = $this->transactionRepository->create($input);
                                 
                                    $total_costumer = $saldo-$price;
                                    $updateCostumer = User::where('id', $users->id)->update(['saldo' => $total_costumer]);

                                                $insert = DB::table('history')->insert(
                                                [ 
                                                    [
                                             'transaction_id' => $transaction->id,
                                             'price' => $price,
                                             'description' => 'Tarik Saldo ( Sedang diproses )',
                                             'user_id' => $users->id,
                                             'bank_id' => $bank[0]->id,
                                              'name' => '0',
                                              'type' => 'withdraw',
                                              'param'=> '0',
                                              'admin' => '0',
                                              'status' => '1',
                                              'created_at' => DB::raw('now()')
                                                    ],
                                             ]);




                                     $history = DB::table('history')->where('transaction_id', $transaction->id)->first();
                                        return $this->sendResponse($history, 'Permintaan anda akan diproses dalam waktu maksimal 1x24 jam');   

                                     } 


                                }else{

                                    return Response::json(ResponseUtil::makeError('Bank penarikan masih kosong!'), 404);   
                                }    

                            }    

                          
                            

                        }else{

                            return Response::json(ResponseUtil::makeError('Tarik tunai tidak diperbolehkan, identitas belum lengkap!'), 404);
                        }
           

                    }else{

                            $blokir = "blokir";
                            $pintgl = DB::table('activity')->get();    
                            if(empty($pintgl[0]))
                            {
                                 $this->loghistory($blokir);  
                                 return Response::json(ResponseUtil::makeError('Tarik tunai tidak diperbolehkan, check pin transaksi anda!'), 404);

                            
                            }else if($pintgl[0]->count =="4"){
                                  
                                    if($pintgl[0]->type =="blokir")
                                    {

                                         $updateLog = DB::table('users')->where('id', $user)->update(['status' => '2']);   
                                         $this->emailblokir();
                                         return Response::json(ResponseUtil::makeError('Akun anda telah di non aktifkan, kami menemukan aktitas mencurigakan. Cek email untuk mengaktifkan akun anda!'), 404);

                                    }

                            }else{

                                $this->loghistory($blokir);  
                                return Response::json(ResponseUtil::makeError('Tarik tunai tidak diperbolehkan, check pin transaksi anda!'), 404);

                            }   

                            

                            
                    }      
                          
                }     

            }

}  

/* non generate merchant */

 public function merchant(CreateTransactionAPIRequest $request)
 {  
       // $input = $request->all();

        $qrcode = $request->qrcode;
        $price = $request->price;
        $security = $request->pin;
        $type = 'nongenerate';
        $tanggal = $request->tanggal;

        // saldo costumer
        $costumer = Auth::user();
        $costumer_id = $costumer->id;
        $saldo_costumer = $costumer->saldo;
        $pin  = $costumer->pin;
        $costumer_name = $costumer->name;

          
	if($security == $pin)
    	{
        	if($saldo_costumer =="0")
        	{

                	return Response::json(ResponseUtil::makeError('Saldo 0 Transaksi tidak diperbolehkan!'), 404);        
        	}else{ 

        		if($price > $saldo_costumer)
        		{

            			return Response::json(ResponseUtil::makeError('Saldo tidak cukup Transaksi tidak diperbolehkan!'), 404);

       			}else{

				   $user = $this->userRepository->findByField('qrcode', $qrcode);
                    		   $mitra_id  = $user[0]->id;
                    		   $mitra_name  = $user[0]->name;
                    		   $saldo_mitra = $user[0]->saldo;

				  $fee = "0";
				  $total_mitra = $saldo_mitra+$price;	


				   $updateMitra = User::where('id', $mitra_id)->update(['saldo' => $total_mitra]);


                                    //saldo costumer
                                    $total_costumer = $saldo_costumer-$price;
                                    $updateCostumer = User::where('id', $costumer_id)->update(['saldo' => $total_costumer]);

                                    //history transaksi
                                    $input['costumer_id'] = $costumer_id; // input costumer
                                    $input['user_id']     = $mitra_id; // input user
                                    $input['price']       = $price; // input price
                                    $input['status']      = "1"; // input status
                                    $input['type']        = "nongenerate";
                                    $input['tanggal']     =  date("Y-m-d");
                                    $input['param']       = $user[0]->param; // transaksi id issuer

                                    if($costumer_id != $mitra_id)
                                    {
                                         $transaction = $this->transactionRepository->create($input);   
                                    }else{

                                        return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan!'), 404);
                                    }


                                    // create history
                                    $this->create_h_transaction($transaction->id,$price,$mitra_name,$costumer_name,$costumer_id,$mitra_id,$potongan,$type);

                                     
                                   $history = DB::table('history')->where(['transaction_id'=> $transaction->id,'user_id'=>$costumer_id])->first();
                                    return $this->sendResponse($history, 'Anda melakukan pembayaran ke '.$mitra_name.'. Terimakasih sudah menggunakan QRPay.id untuk solusi pembayaran Anda');




			}

      		 }


	}else{

		return Response::json(ResponseUtil::makeError('Transaksi tidak diperbolehkan, check pin transaksi anda!'), 404);

	}

}



  



public function konfirmasi(CreateTransactionAPIRequest $request)
{

     $users     = Auth::user();
     $bank      = $request->bank_id;
     $transfer  = $request->transfer;
     $no_rekening = $request->no_rekening;
     $price      = $request->price;
     $tanggal      = $request->tanggal;
     $berita      = $request->berita;
     $photo      = $request->photo;

       $insert = DB::table('deposit')->insert(
            [ 
                [

         'user_id' => $users->id,
         'bank_id' => $bank,
         'type' => $transfer,
         'account_number' => $no_rekening,
         'price'=> $price,
         'transfer_date' => $tanggal,
         'transfer_letter' => $berita,
         'param'        => 'costumer',
         'status'       => '0',
         'created_at' => DB::raw('now()')
                ],
         ]);

                if (!$request->has('photo') || $photo =='') 
                {
                    $photo = Config::get('app.photo_default');
                    $ph = $this->url->to('/'.$photo);      

                }else if ($request->has('photo')) {
                    $fileFormat = ".jpg";
                    $userPath = base64_encode(Auth::user()->email);
                $fileName = base64_encode(Auth::user()->email . time());    

                    $dir = Config::get('elfinder.dir')[0];
                    $files = $dir."/".$userPath;
                    $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
                    File::makeDirectory($files, 0777, true, true);

                        if ($image = base64_decode($photo, true))
                        {
                            $img = Image::make($image)->save($fullPath, 60);
                            
                            $dbUpdate = DB::table('deposit')->where(['user_id'=>$users->id,'status'=>'0'])->update(['transfer_photo'=> $this->url->to('/'.$fullPath)]);

                            
                        } else {
                            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                        }
                     
                } 



      

        $konf = DB::table('deposit')->where(['user_id'=>$users->id,'status'=>'0'])->first();
        return $this->sendResponse($konf, 'Permintaan anda akan diproses dalam waktu maksimal 1x24 jam');  
} 



public function store(CreateTransactionAPIRequest $request)
{
                                       
                       
                              
                                  
    }


     public function history(Request $request)
    {

        $id = Auth::user()->id; 
        $datepm = date("y-m-d");

       $transaction = DB::table('history')->select('id','description','created_at','admin','price')
       ->where(['user_id'=>$id,'status'=>'1'])
        ->where('created_at', 'LIKE', "%$datepm%")->get();

        return $this->sendResponse($transaction, 'List transaksi');
    }
   

    public function batal(Request $request)
    {

       
        $id = $request->id;
        $pin = $request->pin;
     

        $merchant = Auth::user();
	$saldoM = $merchant->saldo;
        $mitra_name = $merchant->name;
        $transaction = DB::table('history')->where('id', $id)->get();

        $tr = DB::table('transaction')->where('id',$transaction[0]->transaction_id)->get();
        $costumer = DB::table('users')->where('id',$tr[0]->costumer_id)->get();
	$saldoC = $costumer[0]->saldo;

         if($pin == $merchant->pin)
         {

            
            $updt = DB::table('transaction')->where('id', $transaction[0]->transaction_id)->update(['status' => '0']);

            $updth = DB::table('history')->where('id', $id)->update(['status' => '2']);

		
		//tambah user
		$dcp = $saldoC+$transaction[0]->price;
		$edpc = DB::table('users')->where('id', $tr[0]->costumer_id)->update(['saldo' => $dcp]);
               // dikirim ke user



            $insert = DB::table('history')->insert(
            [ 
                [
                    'transaction_id' => $transaction[0]->transaction_id,
                    'price' => $transaction[0]->price,
                    'tanggal'=> DB::raw('now()'),
                    'description' => 'Transaksi dibatalkan ( '.$mitra_name.' )',
                    'user_id' => $transaction[0]->user_id,
                    'name' => '0',
                     'param'=>'0',
                     'admin' => '0',
                    'type' => $transaction[0]->type,
                    'status' => '2',
                    'created_at' => DB::raw('now()')
                ],
            ]);


     // history dikirim ke mitra

	$dcpm = $saldoM-$transaction[0]->price;
	$edpcm = DB::table('users')->where('id', $merchant->id)->update(['saldo' => $dcpm]);

        $insert = DB::table('history')->insert(
         [ 
            [
                'transaction_id' => $transaction[0]->transaction_id,
                'price' => $transaction[0]->price,
                'tanggal'=> DB::raw('now()'),
                'description' => 'Pembatalan pembayaran ( '.$costumer[0]->name.' )',
                'user_id' => $merchant->id,
                'name' => '0' ,
                'param'=>'1',
                'admin' => '0',
                'type' => $transaction[0]->type,
                'status' => '2',
                'created_at' => DB::raw('now()')
            ],
        ]);


         $fcm_id = $costumer[0]->fcm;
         $message = "Pembayaran anda telah dibatalkan ".$mitra_name." sejumlah Rp ".$transaction[0]->price."";

           $msg = array
           (
               'saldo' => $costumer[0]->saldo,
               'type'  => 'Transaksi',
               'message' => $message     
           );

         $this->send_fcm($fcm_id,$msg);
         $history = DB::table('history')->where(['user_id'=> $merchant->id,'transaction_id'=>$transaction[0]->transaction_id,'status'=>'2'])->get();
         return $this->sendResponse($history, 'Berhasil dibatalkan');   

         }else{

              return Response::json(ResponseUtil::makeError('Pembatalan tidak diperbolehkan, pin transaksi anda salah!'), 404);

         }   


       


    }

    

    public function loghistory($type)
    {

                $user = Auth::user();

                $pin = DB::table('activity')->where('user_id',$user->id)->get();
                 if(empty($pin))
                 {

                        //insert    
                         $insert = DB::table('activity')->insert(
                            [ 
                                [
                                        'user_id' => $user->id,
                                        'count' => '1',
                                        'type'   => $type,
                                        'status' => '1',
                                        'created_at' => DB::raw('now()')
                                ],
                            ]);

                      
                                 
                 }else{
                   //update

                    if($type =="blokir")
                    {    
                        $Dblok = DB::table('activity')->where(['user_id'=> $user->id,'type'=>'blokir'])->get(); 
                        
                        if(empty($Dblok))
                        {


                                    $insert = DB::table('activity')->insert(
                                    [ 
                                        [
                                        'user_id' => $user->id,
                                        'count' => '1',
                                        'type'   => $type,
                                        'status' => '1',
                                        'created_at' => DB::raw('now()')
                                        ],
                                    ]);


                        }else{

                         
                                if($Dblok[0]->count =='4')
                                {

                               

                                }else{


                                //cek type blokir
                          
                                $total = $Dblok[0]->count+1;


                                $updateLog = DB::table('activity')->where(['user_id'=> $user->id,'type'=>'blokir'])->update(['count' => $total,'tanggal' => DB::raw('now()')]);


                                } 


                        }    

                    }

                    if($type =="bayar")
                    {

                        $Dbayar = DB::table('activity')->where(['user_id'=> $user->id,'type'=>'bayar'])->get(); 
                        
                        if(empty($Dbayar))
                        {


                                    $insert = DB::table('activity')->insert(
                                    [ 
                                        [
                                        'user_id' => $user->id,
                                        'count' => '1',
                                        'type'   => $type,
                                        'status' => '1',
                                        'created_at' => DB::raw('now()')
                                        ],
                                    ]);


                        }else{


                                $Dblok = DB::table('activity')->where(['user_id'=> $user->id,'type'=>'bayar'])->get();
                                $total = $Dblok[0]->count+1;
                                $updateLog = DB::table('activity')->where(['user_id'=> $user->id,'type'=>'bayar'])->update(['count' => $total,'tanggal' => DB::raw('now()')]);

                        }        

                    } 

                }   



    }


    public function emailblokir()
    {

             $user = Auth::user();

             $pin = DB::table('activity')->where('user_id',$user->id)->get();
             $name = $user->name;
             $dateBlokir =  $pin[0]->tanggal;
             $hariTempo = $this->tgl($dateBlokir);
             $userid = $user->id."&";     
             $jamtempo = substr($dateBlokir, +11, -3);
             $dateTrans = $hariTempo." Pukul ".$jamtempo;
             $kode = base64_encode($userid . time());

             Mail::to($user->email)->send(new Blokir($name,$kode,$dateTrans)); 


    } 


    public function emaildeposite($total,$tanggal,$bank)
    {
       //send email 
        $user = Auth::user();
        $nama =  $user->name;
      
        $price  =   number_format($total,0,'','.');  
        
        $tglT = $tanggal;
        $dateTrans = $this->tgl($tglT);

        $dtjtp = strtotime($tglT . " +12 hours");
        $hariTempo = $this->tgl(date("Y-m-d", $dtjtp));
        $jam = date("H:i:s", $dtjtp);
        $jamtempo = substr($jam, 0, -3); 

        $jttempo = $hariTempo." Pukul ".$jamtempo;    
        $metPem = "Transfer";
       
        $eadmin = "layanacomputindo@gmail.com";
      

	$setting = DB::table('setting')->where(['user_id'=>$user->param])->get();
	$logo = $setting[0]->logo;
	$username = $setting[0]->email;
	$merchant = $setting[0]->name;
	
	$id  = $user->param;

        
        Mail::to($user->email)->send(new Depuser($price,$nama,$metPem,$dateTrans,$jttempo,$logo,$username,$merchant,$id));

       // Mail::to($eadmin)->send(new DepAdmin($price,$nama,$metPem,$dateTrans,$jttempo,$bank));

    }

public function create_h_transaction($kode,$price,$mitra_name,$costumer_name,$costumer_id,$mitra_id,$potongan,$type)
{
    $user = Auth::user();
    // history to min
    $insert = DB::table('history')->insert(
    [ 
        [
            'transaction_id' => $kode,
            'price' => $price,
            'tanggal'=> DB::raw('now()'),
            'description' => 'Lakukan pembayaran ( '.$mitra_name.' )',
            'user_id' => $costumer_id,
            'name' => '0',
             'param'=> $user->param,
             'admin' => '0',
            'type' => $type,
            'status' => '1',
            'created_at' => DB::raw('now()')
        ],
    ]);


// history to plus
    $insert = DB::table('history')->insert(
     [ 
        [
            'transaction_id' => $kode,
            'price' => $price,
            'tanggal'=> DB::raw('now()'),
            'description' => 'Terima pembayaran ( '.$costumer_name.' )',
            'user_id' => $mitra_id,
            'name' => '0' ,
            'param'=>$user->param,
            'admin' => $potongan,
            'type' => $type,
            'status' => '1',
            'created_at' => DB::raw('now()')
        ],
    ]);

    $harga = number_format($price,0,'','.');
    $penerima = DB::table('users')->where('id',$mitra_id)->get();
    $fcm_id = $penerima[0]->fcm;
    $message = "Anda telah menerima pembayaran dari ".$costumer_name." sejumlah Rp ".$harga."";

        $msg = array
        (
            'saldo' => $penerima[0]->saldo,
            'type'  => 'Transaksi',
            'message' => $message     
        );

        $this->send_fcm($fcm_id,$msg);

        $type1 ="penerima";
        $type2 ="pengirim";

        // send mail transaksi 
	$setting = DB::table('setting')->where('user_id',$user->param)->get();
	$merchant = $setting[0]->name;
	$logo = $setting[0]->logo;
	$username = $setting[0]->email;


        Mail::to($penerima[0]->email)->send(new Transaksi($costumer_name,$harga,$type1,$merchant,$logo,$username));
        Mail::to($user->email)->send(new Transaksi($mitra_name,$harga,$type2,$merchant,$logo,$username));

}


   


    public function send_fcm($fcm_id,$message)
    {

    $api_access_key = "AAAAhg-KJAI:APA91bGbK1hEG8RUMS_7yEz8MMo3NC4LMUkbABsQ4QlZy2858MEOEMMs179l5oPM3u3dpmL0BKVq5ilqs5i0l8ELGBZxoGpNGtdN0bFgVNv4YDmoGCbreEcq7SCxp1oRKDPjN15qA8qN";
       
    $registrationIds = $fcm_id;
    $fields = array
            (
                'to'        => $registrationIds,
                'priority' => 'high',
                'data'  => $message
            );

    $headers = array (
        'Authorization: key=' . $api_access_key,
        'Content-Type: application/json'
    );


#Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
#Echo Result Of FireBase Server
// echo $result;

    }



    public function tgl($tgl, $hari_tampil = true){

        $bulan  = array("Januari"
                        , "Februari"
                        , "Maret"
                        , "April"
                        , "Mei"
                        , "Juni"
                        , "Juli"
                        , "Agustus"
                        , "September"
                        , "Oktober"
                        , "November"
                        , "Desember");
        $hari   = array("Senin"
                        , "Selasa"
                        , "Rabu"
                        , "Kamis"
                        , "Jum'at"
                        , "Sabtu"
                        , "Minggu");
        $tahun_split    = substr($tgl, 0, 4);
        $bulan_split    = substr($tgl, 5, 2);
        $hari_split     = substr($tgl, 8, 2);
        $tmpstamp       = mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
        $bulan_jadi     = $bulan[date("n", $tmpstamp)-1];
        $hari_jadi      = $hari[date("N", $tmpstamp)-1];
        if(!$hari_tampil)
        $hari_jadi="";
        return $hari_jadi.", ".$hari_split." ".$bulan_jadi." ".$tahun_split;

    }


    
    public function show($id)
    {
        /** @var Transaction $transaction */
        $transaction = $this->transactionRepository->find($id);

        if (empty($transaction)) {
            return Response::json(ResponseUtil::makeError('Transaction not found'), 404);
        }

        return $this->sendResponse($transaction->toArray(), 'Transaction retrieved successfully');
    }



    
}
