<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePostfileAPIRequest;
use App\Http\Requests\API\UpdatePostfileAPIRequest;
use App\Models\Postfile;
use App\Repositories\PostfileRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PostfileController
 * @package App\Http\Controllers\API
 */

class PostfileAPIController extends AppBaseController
{
    /** @var  PostfileRepository */
    private $postfileRepository;

    public function __construct(PostfileRepository $postfileRepo)
    {
        $this->postfileRepository = $postfileRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/postfile",
     *      summary="Get a listing of the Postfile.",
     *      tags={"Postfile"},
     *      description="Get all Postfile",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Postfile")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->postfileRepository->pushCriteria(new RequestCriteria($request));
        $this->postfileRepository->pushCriteria(new LimitOffsetCriteria($request));
        $postfile = $this->postfileRepository->all();

        return $this->sendResponse($postfile->toArray(), 'Postfile retrieved successfully');
    }

    /**
     * @param CreatePostfileAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/postfile",
     *      summary="Store a newly created Postfile in storage",
     *      tags={"Postfile"},
     *      description="Store Postfile",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Postfile that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Postfile")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Postfile"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePostfileAPIRequest $request)
    {
        $input = $request->all();

        $postfile = $this->postfileRepository->create($input);

        return $this->sendResponse($postfile->toArray(), 'Postfile saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/postfile/{id}",
     *      summary="Display the specified Postfile",
     *      tags={"Postfile"},
     *      description="Get Postfile",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Postfile",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Postfile"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Postfile $postfile */
        $postfile = $this->postfileRepository->find($id);

        if (empty($postfile)) {
            return Response::json(ResponseUtil::makeError('Postfile not found'), 404);
        }

        return $this->sendResponse($postfile->toArray(), 'Postfile retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePostfileAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/postfile/{id}",
     *      summary="Update the specified Postfile in storage",
     *      tags={"Postfile"},
     *      description="Update Postfile",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Postfile",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Postfile that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Postfile")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Postfile"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePostfileAPIRequest $request)
    {
        $input = $request->all();

        /** @var Postfile $postfile */
        $postfile = $this->postfileRepository->find($id);

        if (empty($postfile)) {
            return Response::json(ResponseUtil::makeError('Postfile not found'), 404);
        }

        $postfile = $this->postfileRepository->update($input, $id);

        return $this->sendResponse($postfile->toArray(), 'Postfile updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/postfile/{id}",
     *      summary="Remove the specified Postfile from storage",
     *      tags={"Postfile"},
     *      description="Delete Postfile",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Postfile",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Postfile $postfile */
        $postfile = $this->postfileRepository->find($id);

        if (empty($postfile)) {
            return Response::json(ResponseUtil::makeError('Postfile not found'), 404);
        }

        $postfile->delete();

        return $this->sendResponse($id, 'Postfile deleted successfully');
    }
}
