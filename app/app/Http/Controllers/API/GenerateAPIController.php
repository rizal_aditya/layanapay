<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGenerateAPIRequest;
use App\Http\Requests\API\UpdateGenerateAPIRequest;
use App\Models\Generate;
use App\Repositories\GenerateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use Illuminate\Routing\UrlGenerator;
use File;
use DB;
use QrCode;
/**
 * Class GenerateController
 * @package App\Http\Controllers\API
 */

class GenerateAPIController extends AppBaseController
{
    /** @var  GenerateRepository */
    private $generateRepository;
    protected $url;

    public function __construct(GenerateRepository $generateRepo,UrlGenerator $url)
    {
        $this->generateRepository = $generateRepo;
        $this->url = $url;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/generate",
     *      summary="Get a listing of the Generate.",
     *      tags={"Generate"},
     *      description="Get all Generate",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Generate")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->generateRepository->pushCriteria(new RequestCriteria($request));
        $this->generateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $generate = $this->generateRepository->all();

        return $this->sendResponse($generate->toArray(), 'Generate retrieved successfully');
    }

    /**
     * @param CreateGenerateAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/generate",
     *      summary="Store a newly created Generate in storage",
     *      tags={"Generate"},
     *      description="Store Generate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Generate that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Generate")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Generate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateGenerateAPIRequest $request)
    {
        $input = $request->all();
        $user = Auth::user();
        $id = $user->id;
        $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
        $qrcode = $kode.$id;

        $nominal = $request->nominal;

        $fileFormatqr = ".png";
        $qrPath = base64_encode('qrcode');
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$qrPath;

        $fileQr = base64_encode($qrcode . time());
        $fullqr = $dir."/".$qrPath."/".$fileQr.$fileFormatqr;
        File::makeDirectory($files, 0777, true, true);


      


        $input['user_id'] = $id;
        $input['code'] = $qrcode;
        $input['img_qrcode'] = $this->url->to($fullqr);
        $input['status'] = '1';

        $gt = $this->generateRepository->all();
        if($gt->isEmpty())
        {

            $png = QrCode::format('png')->size(500)
            ->merge('../public/img/iconqr.png', .1, true)
                ->generate(''.$kode.$user->id.'', '../public/'.$fullqr);
            
          
            $generate = $this->generateRepository->create($input);
            $gen = DB::table('generate')->where('user_id',$id)->first();
            return $this->sendResponse($gen, 'Transaksi berhasil di generate');
        
        }else{

                $gto = DB::table('generate')->where('user_id',$id)->get(); 
           
            if(empty($gto[0]->user_id))
            {

                    $png = QrCode::format('png')->size(500)
                        ->merge('../public/img/iconqr.png', .1, true)
                            ->generate(''.$kode.$user->id.'', '../public/'.$fullqr);
            
          
                        $generate = $this->generateRepository->create($input);
                        $gen = DB::table('generate')->where('user_id',$id)->first();


            }else{    

                if($gto[0]->user_id == $id)
                {    
                    //$deleteFile = DB::table('generate')->where('id',$idGen)->delete();
                    //unlink($fullqr);
                   // $generate = $this->generateRepository->create($input); 

                   $updateDB =  DB::table('generate')->where('user_id',$id)->update(['nominal'=>$nominal]);
                   $gen = DB::table('generate')->where('user_id',$id)->first();
                    
                   
                }else{
                    
                      $png = QrCode::format('png')->size(500)
                        ->merge('../public/img/iconqr.png', .1, true)
                            ->generate(''.$kode.$user->id.'', '../public/'.$fullqr);
            
          
                        $generate = $this->generateRepository->create($input);
                        $gen = DB::table('generate')->where('user_id',$id)->first();
         
                    
                } 

            }    

                 return $this->sendResponse($gen, 'Transaksi berhasil di generate.');
            
            
        }   
        
       
        
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/generate/{id}",
     *      summary="Display the specified Generate",
     *      tags={"Generate"},
     *      description="Get Generate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Generate",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Generate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Generate $generate */
        $generate = $this->generateRepository->find($id);

        if (empty($generate)) {
            return Response::json(ResponseUtil::makeError('Generate not found'), 404);
        }

        return $this->sendResponse($generate->toArray(), 'Generate retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateGenerateAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/generate/{id}",
     *      summary="Update the specified Generate in storage",
     *      tags={"Generate"},
     *      description="Update Generate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Generate",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Generate that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Generate")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Generate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateGenerateAPIRequest $request)
    {
        $input = $request->all();

        /** @var Generate $generate */
        $generate = $this->generateRepository->find($id);

        if (empty($generate)) {
            return Response::json(ResponseUtil::makeError('Generate not found'), 404);
        }

        $generate = $this->generateRepository->update($input, $id);

        return $this->sendResponse($generate->toArray(), 'Generate updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/generate/{id}",
     *      summary="Remove the specified Generate from storage",
     *      tags={"Generate"},
     *      description="Delete Generate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Generate",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Generate $generate */
        $generate = $this->generateRepository->find($id);

        if (empty($generate)) {
            return Response::json(ResponseUtil::makeError('Generate not found'), 404);
        }

        $generate->delete();

        return $this->sendResponse($id, 'Generate deleted successfully');
    }
}
