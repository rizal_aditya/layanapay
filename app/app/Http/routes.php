<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function (Request $request) {
    return redirect('home');
   
   
});


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
    	Route::post('auth', 'AuthAPIController@authenticate');
        Route::post('forgotPassword', 'AuthAPIController@forgot');
        Route::post('user/register', 'UserAPIController@register');
        Route::post('cekparam', 'AuthAPIController@cekparam');

        Route::get('user/send', 'UserAPIController@sendemail');
    	Route::group(['middleware' => ['jwt.auth']], function () {
            require config('infyom.laravel_generator.path.api_routes');
        });
    });
});


Route::group(['prefix' => 'cobranding', 'namespace' => 'COBRANDING'], function () {
    Route::group(['prefix' => 'v1'], function () {
    	Route::post('auth', 'AuthAPIController@authenticate');
       
    	Route::group(['middleware' => ['jwt.auth']], function () {
            require config('infyom.laravel_generator.path.api_routes');
        });
    });
});

Route::get('/elfinder/tinymce','Barryvdh\Elfinder\ElfinderController@showTinyMCE4');

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
// Route::get('register', 'Auth\AuthController@getRegister');
// Route::post('register', 'Auth\AuthController@postRegister');

// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => ['auth']], function () {

     Route::get('fileupload', [
        'as' => 'fileupload',
        'uses' => 'Barryvdh\Elfinder\ElfinderController@showPopup',
    ]);

    Route::group(['middleware' => ['role:admin']], function () {
    
    });
    // tampilkan sesudah login
	Route::get('home', 'HomeController@index');
 
    
    Route::resource('user/login', 'UserController@login');
    Route::resource('user', 'UserController');


    
  
    Route::get('user/{id}/enable','UserController@enable');
    Route::get('user/{id}/disable','UserController@disable');
    Route::get('user/{id}/notification','UserController@notification');
    Route::get('user/deposite','UserController@deposite');

  
    Route::resource('role', 'RoleController');
    
    Route::get('transaction/{id}/enable','TransactionController@enable');
	Route::get('transaction/{id}/disable','TransactionController@disable');
    
  
  
    Route::resource('device', 'DeviceController');
  

    Route::resource('menu', 'MenuController');
    Route::get('deposite/tes', 'DepositeController@tes');
    Route::get('deposite/jmldeposite', 'DepositeController@jmldeposite');
    Route::get('deposite/jmlwithdraw', 'DepositeController@jmlwithdraw');
    Route::get('deposite/jmluser', 'DepositeController@jmluser');
    Route::get('deposite/listuserdeposite', 'DepositeController@listuserdeposite');
    Route::get('deposite/listtabeldeposite', 'DepositeController@listtabeldeposite');
    Route::get('deposite/listuserwithdraw', 'DepositeController@listuserwithdraw');
    Route::get('deposite/listtabelwithdraw', 'DepositeController@listtabelwithdraw');

    Route::get('deposite/bankAdmin', 'DepositeController@bankAdmin');

    Route::get('deposite/listtabeluser', 'DepositeController@listtabeluser');
     Route::get('deposite/listuserkyc', 'DepositeController@listuserkyc');

    Route::get('deposite/konfirmasi', 'DepositeController@konfirmasi');
   
    
 
    Route::get('deposite/detailuser', 'DepositeController@detailuser');  
    Route::get('deposite/blokiruser', 'DepositeController@blokiruser');
    Route::get('deposite/bataluser', 'DepositeController@bataluser');
    Route::get('deposite/terimauser', 'DepositeController@terimauser');

//send
     Route::get('deposite/kirim', 'DepositeController@kirim');
     Route::get('deposite/batal', 'DepositeController@batal');
      Route::get('deposite/delete','DepositeController@delete');



     Route::get('deposite/updatenotivdep', 'DepositeController@updatenotivdep');
     Route::get('deposite/updatenotivdraw', 'DepositeController@updatenotivdraw');   
     Route::get('deposite/updatenotivuser', 'DepositeController@updatenotivuser');   
});

Route::resource('admin', 'AdminController');

/* --- transaksi --- */
Route::resource('order', 'OrderController');
Route::post('order/search', 'OrderController@search');


Route::resource('purchase', 'PurchaseController');
Route::resource('packet', 'PacketController');
Route::resource('register', 'UserController@getRegister');
Route::post('user/register', 'UserController@postRegister');
Route::resource('client', 'ClientController');
Route::get('email', 'EmailController@index');
Route::resource('transaction', 'TransactionController');
Route::resource('history', 'HistoryController');
Route::resource('bank', 'BankController');
Route::resource('generate', 'GenerateController');


Route::get('dashboard/getsaldo', 'DashboardController@getsaldo');
Route::get('dashboard/getlates', 'DashboardController@getlates');
Route::get('dashboard/jmldana', 'DashboardController@jmldana');
Route::get('dashboard/totprovite', 'DashboardController@totprovite');


Route::get('dashboard/totmember', 'DashboardController@totmember');
Route::get('dashboard/dealadmin', 'DashboardController@dealadmin');
Route::get('dashboard/totmerchant', 'DashboardController@totmerchant');
Route::resource('setting', 'SettingController');


Route::resource('promo', 'PostController');


Route::resource('activity', 'ActivityController');

Route::resource('post', 'PostController');

/* Routes Galih */
Route::post('setting/postSaldo', 'SettingController@postDeposit'); /* transaksi tambah saldo */
Route::post('setting/postKonfirmSaldo', 'SettingController@postKonfirmasi'); /* konfirmasi transaksi tambah saldo */


Route::get('setting/list', 'SettingController@listadmin'); /* setting admin */


Route::get('deposite/listtabelcobrand', 'DepositeController@listtabelcobrand'); /* list table co branding */
// Route::get('user/create', 'DepositeController@create'); /* list table co branding */
Route::post('user/postCobrand', 'DepositeController@postcobrand'); /* simpan data co branding */
Route::post('user/postHapusCo', 'DepositeController@posthapuscobrand'); /* hapus data co branding */
Route::post('user/postEditCo', 'DepositeController@posteditcobrand'); /* edit data co branding */
Route::get('user/detailcobrand/{id}', 'DepositeController@detailco'); /* detail co branding */

Route::get('deposite/listtabelmerchant', 'DepositeController@listtabelmerchant'); /* list table merchant */
Route::post('user/postmerchant', 'DepositeController@postmerchant'); /* simpan data merchant */
Route::post('user/postHapusmerchant', 'DepositeController@posthapusmerchant'); /* hapus data merchant */
Route::post('user/postEditmerchant', 'DepositeController@posteditmerchant'); /* edit data merchant */
Route::get('user/detailmerchant/{id}', 'DepositeController@detailmerchant'); /* detail merchant */

Route::get('issuer/dataissuer', 'DepositeController@getissuer'); /* list table issuer */
Route::post('user/postissuer', 'DepositeController@postissuer'); /* simpan data issuer */
Route::post('user/postHapusissuer', 'DepositeController@posthapusissuer'); /* hapus data issuer */
Route::post('user/postEditIssuer', 'DepositeController@posteditissuer'); /* edit data issuer */
Route::get('user/detailissuer/{id}', 'DepositeController@detailissuer'); /* detail issuer */

Route::get('issuer/depositissuer', 'DepositeController@DepositeIssuer');
Route::get('issuer/lisdeposit', 'DepositeController@listtableDeposit');
Route::get('issuer/detailDep', 'DepositeController@getdetailDeposit');
Route::post('issuer/konfirDep', 'DepositeController@konfirmasi_deposit');

Route::get('issuer/historytrans', 'DepositeController@historyTransaction');
Route::get('issuer/listhistorytrans', 'DepositeController@listhistoryTransaction');
Route::get('issuer/detailhistorytrans', 'DepositeController@detailhistoryTransaction');
/* End Of Routes Galih */

/* --- Cobranding API --- */

//Route::resource('access', 'AccessController');
Route::post('access/login', 'AccessController@login');
Route::post('access/transaksi', 'AccessController@transaksi');
Route::get('access/profile', 'AccessController@profile');
Route::get('reset', 'AccessController@reset');
