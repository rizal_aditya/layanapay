<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Register extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $url;
    public $logoserver;
    public $merchant;
    public $username;
    public $password;
    public $port;
    public $host;
    public $encryption;
    public $key;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$url,$logoserver,$merchant,$username,$password,$port,$host,$encryption,$key)
    {
         $this->name = $name;
         $this->email = $email;
         $this->url = $url;
         $this->logoserver = $logoserver;
         $this->merchant = $merchant;
         $this->username = $username;
         $this->password = $password;
         $this->port = $port;
         $this->host = $host;
         $this->encryption = $encryption; 
         $this->key = $key;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
    


        return $this->from($this->username,'Admin '.$this->merchant)->subject($this->merchant.': Aktivasi Akun')->view('email.welcome');
       
        
    }
}
