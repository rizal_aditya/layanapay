<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Forgot extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $url;
    public $merchant;
    public $logo;
    public $username;	
    public $domain;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$url,$merchant,$logo,$username,$domain)
    {
        $this->name = $name;
        $this->email = $email;
         $this->url = $url;
	$this->merchant = $merchant;
	$this->logo = $logo;
	$this->username = $username;
	$this->domain = $domain;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
 
	   return $this->from($this->username,'Admin '.$this->merchant)->subject($this->merchant.': Lupa Password')->view('email.forgot');
       

    }
}
