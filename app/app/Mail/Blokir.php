<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Blokir extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $id;
    public $dateTrans;
   

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$id,$dateTrans)
    {
      
        $this->name = $name;
        $this->id = $id;
        $this->dateTrans = $dateTrans;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->subject('QRPay.id: Tambah Saldo')->view('email.test');
         return $this->subject('QRPay.id: Blokir akun')->view('email.blokir');
    }
}

