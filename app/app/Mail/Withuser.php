<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Withuser extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$url)
    {
        $this->name = $name;
        $this->email = $email;
         $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('QRPay.id: Tarik Tunai')->view('email.withdraw');
        
    }
}
