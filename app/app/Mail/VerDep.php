<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerDep extends Mailable
{
    use Queueable, SerializesModels;

    public $price;
    public $total;
    public $nama;
    public $tanggal;
    public $metPem;
    public $logo;
    public $merchant;
    public $username;	


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($price,$total,$nama,$tanggal,$metPem,$logo,$merchant,$username)
    {
        $this->price = $price;
        $this->total = $total;
        $this->nama = $nama;
        $this->tanggal = $tanggal;
        $this->metPem  = $metPem;
	$this->logo = $logo;
	$this->merchant = $merchant;
	$this->username = $username;
	    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->username,'Admin '.$this->merchant)->subject($this->merchant.': Sukses Tambah Saldo')->view('email.verdep');
       

         
    }
}

