<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerUser extends Mailable
{
    use Queueable, SerializesModels;

   
    public $nama;
    public $total;
    public $tanggal;
    public $bank;
    public $no_rek;
    public $atsnama;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nama,$total,$tanggal,$bank,$no_rek,$atsnama)
    {
       
        $this->nama = $nama;
        $this->total = $total;
        $this->tanggal = $tanggal;
        $this->bank  = $bank;
        $this->no_rek = $no_rek;
        $this->atsnama = $atsnama;
      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->subject('QRPay.id: Tambah Saldo')->view('email.test');
         return $this->subject('QRPay.id: Sukses Verifikasi Akun')->view('email.veruser');
    }
}

