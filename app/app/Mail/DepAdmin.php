<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DepAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $price;
    public $deposite;
    public $h_convert;
    public $kunik;
    public $nama;
    public $metPem;
    public $dateTrans;
    public $jttempo;
    public $bank;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($price,$deposite,$h_convert,$kunik,$nama,$metPem,$dateTrans,$jttempo,$bank)
    {
        $this->price = $price;
        $this->deposite = $deposite;
        $this->h_convert = $h_convert;
        $this->kunik = $kunik;
        $this->nama = $nama;
        $this->metPem = $metPem;
        $this->dateTrans = $dateTrans;
        $this->jttempo = $jttempo;
        $this->bank = $bank;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->subject('QRPay.id: Tambah Saldo')->view('email.test');
         return $this->subject('QRPay.id: Permintaan Tambah Saldo')->view('email.depadmin');
    }
}

