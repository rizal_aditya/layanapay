<?php

namespace App\Repositories;

use App\Models\Deposite;
use InfyOm\Generator\Common\BaseRepository;

class DepositeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'qrcode',
        'user_id',
        'last_balance',
        'packet_id',
        'costumer_id',
        'bank_id',
        'bank_send',
        'price',
        'type',
        'notif',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Deposite::class;
    }
}
