<?php

namespace App\Repositories;

use App\Models\Purchase;
use InfyOm\Generator\Common\BaseRepository;

class PurchaseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price',
        'denom',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase::class;
    }
}
