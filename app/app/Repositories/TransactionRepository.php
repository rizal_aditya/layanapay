<?php

namespace App\Repositories;

use App\Models\Transaction;
use InfyOm\Generator\Common\BaseRepository;

class TransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'qrcode',
        'user_id',
        'last_balance',
        'packet_id',
        'costumer_id',
        'tanggal',
        'bank_id',
        'bank_send',
        'price',
        'type',
        
        'notif',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transaction::class;
    }
}
