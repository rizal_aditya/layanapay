<?php

namespace App\Repositories;

use App\Models\History;
use InfyOm\Generator\Common\BaseRepository;

class HistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'transaction_id',
        'user_id',
        'bank_id',
        'tanggal',
        'bank_send',
        'name',
        'price',
        'description',
        'type',
        'param',
        'admin',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return History::class;
    }
}
