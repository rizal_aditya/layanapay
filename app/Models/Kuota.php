<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kuota extends Model
{
    use SoftDeletes;

    public $table = 'deposit';
    
    protected $dates = ['deleted_at'];

    public $fillable = [
        'user_id',
        'price',
        'bank_id',
        'account_number',
        'account_name',
        'type',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'price' => 'integer',
        'bank_id' => 'integer',
        'account_number' => 'string',
        'account_name' => 'string',
        'type' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'name' => 'required',
        //'min' => 'required',
       // 'max' => 'required',
       // 'type' => 'required',
       // 'status' => 'required'
    ];
}
