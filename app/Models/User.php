<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

class User extends Model
{
    //use SoftDeletes;

    public $table = 'users';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'client_id',
        'name',
        'email',
        'password',
        'saldo',
        'pob',
        'dob',
        'gender',
        'national',
        'city',
        'postal_code',
        'education',
        'job',
        'qrcode',
        'pin',
        'photo',
        'phone',
        'type_id',
        'identity_number',
        'identity_photo',
        'img_qrcode',
        'img_sb',
        'img_location',
        'address',
        'type',
        'notif',
        'status',
        'fcm',
        'code_reveral',
        'img_reveral',
        'param',
        'kyc'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'client_id'         => 'integer',
        'name'              => 'string',
        'email'             => 'string',
        'password'          => 'string',
        'saldo'             => 'integer',
        'pob'               => 'string',
        'dob'               => 'date',
        'gender'            => 'string',
        'national'          => 'string',
        'city'              => 'string',
        'postal_code'       => 'integer',
        'education'         => 'string',
        'job'               => 'string',
        'qrcode'            => 'string',
        'pin'               => 'string',
        'photo'             => 'string',
        'phone'             => 'integer',
        'type_id'           => 'string',
        'identity_number'   => 'integer',
        'identity_photo'    => 'string',
        'img_qrcode'        => 'string',
        'img_sb'            => 'string',
        'img_location'      => 'string',
        'address'           => 'text',
        'type'              => 'string',
        'status'            => 'integer',
        'fcm'              => 'string',
        'code_reveral'     => 'string',
        'img_reveral'       => 'string',
        'param'            => 'integer',
        'kyc'               => 'integer',
        
    ];

    protected $rulesets = [

        'creating' => [
            'email'      => 'required|email',
            'password'   => 'required',
           
        ],

        'updating' => [
            'email'      => 'required|email',
            'password'   => '',
            
        ],
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
        'name' => 'required',
        'email' => 'required',
        'password' => 'required', 
        'status' => 'required',
        'type' => 'required'
    ];



    
}
