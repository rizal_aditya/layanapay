<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="History",
 *      required={"deposite_id", "transaction_id", "user_id", "bank_id", "price", "description", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="deposite_id",
 *          description="deposite_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="transaction_id",
 *          description="transaction_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="bank_id",
 *          description="bank_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class History extends Model
{
    use SoftDeletes;

    public $table = 'history';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'transaction_id',
        'user_id',
        'bank_id',
        'tanggal',
        'bank_send',
        'name',
        'price',
        'description',
        'type',
        'param',
        'admin',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'transaction_id' => 'integer',
        'user_id' => 'integer',
        'bank_user' => 'integer',
        'tanggal' => 'date',
        'bank_send' => 'integer',
        'name' => 'string',
        'price' => 'integer',
        'description' => 'string',
        'type' => 'integer',
        'param' => 'integer',
        'admin' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
        'user_id' => 'required',
        'description' => 'required',
        'type' => 'required',
        'status' => 'required'
    ];

   
     public function user()
    {
        return $this->belongsTo('App\User');
    }

    //  public function transaction()
    // {
    //     return $this->belongsTo('App\Models\Transaction');
    // }
}
