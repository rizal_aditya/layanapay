<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Transaction",
 *      required={"qrcode", "user_id", "costumer_id", "price", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="qrcode",
 *          description="qrcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="costumer_id",
 *          description="costumer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Deposite extends Model
{
    use SoftDeletes;

    public $table = 'transaction';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'qrcode',
        'user_id',
        'last_balance',
        'packet_id',
        'costumer_id',
        'bank_id',
        'bank_send',
        'price',
        'denom',
        'type',
        'notif',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'qrcode' => 'string',
        'user_id' => 'integer',
        'last_balance' =>'integer',
        'packet_id' => 'integer',
        'costumer_id' => 'integer',
        'bank_id' => 'integer',
        'bank_send' => 'integer',
        'price' => 'integer',
        'type' => 'string',
        'notif' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
       

        
    ];


     public function user()
    {
        return $this->belongsTo('App\User');
    }

     public function packet()
    {
        return $this->belongsTo('App\Models\Packet');
    }

    

    
}
