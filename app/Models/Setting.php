<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Setting",
 *      required={"name", "min", "max", "type", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="min",
 *          description="min",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="max",
 *          description="max",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Setting extends Model
{
    use SoftDeletes;

    public $table = 'setting';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
	'name',
        'email',
        'password',
        'host',
        'port',
        'encryption',
        'logo',
        'mindepn',
        'maxdepn',
        'mintrn',
        'maxtrn',
        'minwdn',
        'maxwdn',
        'mindepk',
        'maxdepk',
        'mintrk',
        'maxtrk',
        'minwdk',
        'maxwdk',
        'admin',
	'transaksi',
	'transfer',
        'saldo',
        'bank',
        'count',
        'account_number',
        'account_name',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id'=>'integer',
	'name' =>'string',
        'email' =>'string',
        'password' =>'string',
        'host' =>'string',
        'port' =>'string',
        'encryption'=>'string',
        'logo' =>'string',
        'mindepn' =>'integer',
        'maxdepn' =>'integer',
        'mintrn' =>'integer',
        'maxtrn' =>'integer',
        'minwdn' =>'integer',
        'maxwdn' =>'integer',
        'mindepk' =>'integer',
        'maxdepk' =>'integer',
        'mintrk' =>'integer',
        'maxtrk' =>'integer',
        'minwdk' =>'integer',
        'maxwdk' =>'integer',
        'admin' =>'integer',
	'transaksi' =>'integer',
	'transfer' =>'integer',
        'saldo' =>'integer',
        'bank' =>'string',
        'count' =>'integer',
        'account_name' =>'string',
        'account_number'=>'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'name' => 'required',
        //'min' => 'required',
       // 'max' => 'required',
       // 'type' => 'required',
       // 'status' => 'required'
    ];

     public function user()
    {
        return $this->belongsTo('App\User');
    }

}
