<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerWith extends Mailable
{
    use Queueable, SerializesModels;

    public $price;
    public $total;
    public $nama;
    public $tanggal;
    public $bank;
    public $no_rek;
    public $atsnama;
    public $logo;
    public $merchant;
    public $username;		

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($price,$total,$nama,$tanggal,$bank,$no_rek,$atsnama,$logo,$merchant,$username)
    {
        $this->price = $price;
        $this->total = $total;
        $this->nama = $nama;
        $this->tanggal = $tanggal;
        $this->bank  = $bank;
        $this->no_rek = $no_rek;
        $this->atsnama = $atsnama;
	$this->logo = $logo;
	$this->merchant = $merchant;
	$this->username = $username;

      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->subject('QRPay.id: Tambah Saldo')->view('email.test');
        
	 return $this->from($this->username,'Admin '.$this->merchant)->subject($this->merchant.': Sukses Tarik Saldo')->view('email.verwith');
       

    }
}

