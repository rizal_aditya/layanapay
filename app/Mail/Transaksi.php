<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Transaksi extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $price;
    public $type;
    public $merchant;
    public $logo;
    public $username;

	
 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$price,$type,$merchant,$logo,$username)
    {
        $this->name = $name;
        $this->price = $price;
        $this->type  = $type;
	$this->merchant = $merchant;
	$this->logo 	= $logo;
	$this->username  = $username;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
	 return $this->from($this->username,'Admin '.$this->merchant)->subject($this->merchant.': Transaksi Sukses')->view('email.transaksi');
       

    }
}
