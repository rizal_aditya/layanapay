<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Depuser extends Mailable
{
    use Queueable, SerializesModels;

    public $price;
    public $nama;
    public $metPem;
    public $dateTrans;
    public $jttempo;
    public $logo;
    public $username;
    public $merchant;
    public $id;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($price,$nama,$metPem,$dateTrans,$jttempo,$logo,$username,$merchant,$id)
    {
        $this->price = $price;
        $this->nama = $nama;
        $this->metPem = $metPem;
        $this->dateTrans = $dateTrans;
        $this->jttempo = $jttempo;
	$this->logo = $logo;
	$this->username = $username;
	$this->merchant = $merchant;
	$this->id = $id;   
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
	return $this->from($this->username,'Admin '.$this->merchant)->subject($this->merchant.': Tambah Saldo')->view('email.depuser');
       

    }
}

