<?php

namespace App\Repositories;

use App\Models\Maps;
use InfyOm\Generator\Common\BaseRepository;

class MapsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'latitude',
        'longitude',
        'type'
       
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Maps::class;
    }
}
