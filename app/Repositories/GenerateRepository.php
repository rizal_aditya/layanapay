<?php

namespace App\Repositories;

use App\Models\Generate;
use InfyOm\Generator\Common\BaseRepository;

class GenerateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'user_id',
        'nominal',
        'img_qrcode',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Generate::class;
    }
}
