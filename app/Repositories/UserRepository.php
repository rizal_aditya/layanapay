<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_id',
        'name',
        'email',
        'password',
        'saldo',
        'pob',
        'dob',
        'gender',
        'national',
        'city',
        'postal_code',
        'education',
        'job',
        'qrcode',
        'pin',
        'photo',
        'phone',
        'type_id',
        'identity_number',
        'identity_photo',
        'img_qrcode',
        'address',
        'type',
        'notif',
        'fcm',
        'param',
        'status'

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}