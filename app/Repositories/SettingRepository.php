<?php

namespace App\Repositories;

use App\Models\Setting;
use InfyOm\Generator\Common\BaseRepository;

class SettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'name',
        'email',
        'password',
        'host',
        'port',
        'encryption',
	'logo',
        'mindepn',
        'maxdepn',
        'mintrn',
        'maxtrn',
        'minwdn',
        'maxwdn',
        'mindepk',
        'maxdepk',
        'mintrk',
        'maxtrk',
        'minwdk',
        'maxwdk',
        'admin',
	'transaksi',
	'transfer',
        'saldo',
        'bank',
        'count',
        'account_number',
        'account_name',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Setting::class;
    }
}
