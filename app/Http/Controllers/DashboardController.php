<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Repositories\DepositeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\TransactionCriteria;
use Response;
use Auth;
use DB;
use App\Models\Deposite;


class DashboardController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $depositeRepository;

    public function __construct(DepositeRepository $depositeRepo)
    {
        $this->depositeRepository = $depositeRepo;
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    

    public function getsaldo(Request $request){
      $limit = '4';
      $id = Auth::id();
      $saldo = DB::table('bank')->where(['type'=>'admin','user_id'=>$id])->orderBy('id','desc')->paginate($limit);
      return $saldo;

    }

    public function getlates(Request $request){
      $limit = '10';
      $id = Auth::id();
      //select transaksi
           $select['a.id'] = 'a.id';
           $select['a.tanggal'] = 'a.tanggal';
           $select['a.type'] = 'a.type';
           $select['a.status'] = 'a.status';
           $select['a.price']   = 'a.price'; 
           $select['a.last_balance'] = 'a.last_balance';
           $select['a.created_at'] = 'a.created_at';
           // select user
           $select['b.name'] = 'b.name';
           $select['b.photo'] = 'b.photo';
           $select['b.saldo'] = 'b.saldo';
           $select['b.type as idc'] = 'b.type as idc';

           $selectc['c.id'] = 'c.id';
           $selectc['c.tanggal'] = 'c.tanggal';
           $selectc['c.type'] = 'c.type';
           $selectc['c.status'] = 'c.status';
           $selectc['c.price']   = 'c.price'; 
           $selectc['c.last_balance'] = 'c.last_balance';
           $selectc['c.created_at'] = 'c.created_at';
           // select user
           $select['b.name'] = 'b.name';
           $select['b.photo'] = 'b.photo';
           $select['b.saldo'] = 'b.saldo';

           $selectc['d.name'] = 'd.name';
           $selectc['d.photo'] = 'd.photo';
           $selectc['d.saldo'] = 'd.saldo';
            $selectc['d.type as idc'] = 'd.type as idc';

          //relasi user
           $dbuser = 'users as b';
           $reldbuser = 'b.id';
           $reldbdep1 = 'a.user_id'; 
           $sama = '=';


           //relasi user
           $dbuserc = 'users as d';
           $reldbuserc = 'd.id';
           $reldbdep1c = 'c.user_id';


$first =  DB::table('transaction as c')->select($selectc)->join($dbuserc, $reldbuserc, $sama, $reldbdep1c)->where('c.type','withdraw')->where("d.param", $id);

$lates = DB::table('transaction as a')->select($select)->join($dbuser, $reldbuser, $sama, $reldbdep1)->where('a.type','deposite')->where("b.param", $id)->union($first)->limit(10)->get();

   
    $data = array(['data' => $lates]);    
    return $data;

// $lates = DB::table('transaction as a')->select($select)->join($dbuser, $reldbuser, $sama, $reldbdep1)->where('a.type','deposite')->paginate($limit);

// return $lates;
    }

     public function show()
    {


    }  

     public function jmldana(Request $request)
    {
       

       $user = Auth::user();
       $total = DB::table('setting')->where('user_id',$user->id)->get();
       $data = array(['dana' => $total[0]->saldo]);
       return $data;
    }

     public function totcobranding()
    {

       $user = Auth::user();
        if($user->type == "admin")
       {

         $cobranding = DB::table('users')->where(['type'=>'cobranding','status'=>'1'])->count();
       }else{

        $cobranding = DB::table('users')->where(['type'=>'cobranding','status'=>'1', 'param' => $user->id])->count();
       } 
       
     
       
       $data = array(['cobranding' => $cobranding]);
       return $data ;
    } 


    public function totmember()
    {
        $user = Auth::user();
        if($user->type == "admin")
       {

         $member = DB::table('users')->where(['type'=>'costumer','status'=>'1'])->count();
       }else{

        $member = DB::table('users')->where(['type'=>'costumer','status'=>'1', 'param' => $user->id])->count();
       } 
       
     
       
       $data = array(['member' => $member]);
       return $data ;
    }   



     public function totissuer()
    {
       $user = Auth::user();
        if($user->type == "admin")
       {
         $merchant = DB::table('users')->where(['type'=>'issuer','status'=>'1'])->count();
       }else{

         $merchant = "";

       } 

       
       $data = array(['issuer' => $merchant]);
       return $data;
     
    }  

    public function totmerchant()
    {
       $user = Auth::user();

       if($user->type == "admin")
       {
         $merchant = DB::table('users')->where(['type'=>'merchant','status'=>'1'])->count();
       }else{
         $merchant = DB::table('users')->where(['type'=>'merchant','status'=>'1', 'param' => $user->id])->count();

       } 

       
       $data = array(['merchant' => $merchant]);
       return $data;
    }    

     public function send_fcm($fcm_id,$message)
    {

    $api_access_key = "AAAAhg-KJAI:APA91bGbK1hEG8RUMS_7yEz8MMo3NC4LMUkbABsQ4QlZy2858MEOEMMs179l5oPM3u3dpmL0BKVq5ilqs5i0l8ELGBZxoGpNGtdN0bFgVNv4YDmoGCbreEcq7SCxp1oRKDPjN15qA8qN";
       
    $registrationIds = $fcm_id;
    $fields = array
            (
                'to'        => $registrationIds,
                'priority' => 'high',
                'data'  => $message
            );

    $headers = array (
        'Authorization: key=' . $api_access_key,
        'Content-Type: application/json'
    );


#Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
#Echo Result Of FireBase Server
// echo $result;

    }
   

    

    
   
}
