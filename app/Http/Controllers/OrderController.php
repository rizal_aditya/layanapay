<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Repositories\TransactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\TransactionCriteria;
use Response;
use Auth;
use App\User;
use DB;


class OrderController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepo)
    {
        $this->transactionRepository = $transactionRepo;
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

    
      
         return view('transaction.order');

        
    }

     public function history(Request $request)
    {
                     $user   = Auth::user();
                    $type = "deposite"; 
                    $costumer_id  = $request->get('status'); 
                    $pages = $request->get('page');
                    $search = $request->get('search');



                     $limit = "10";
                    

                     //select transaksi
                     $select['a.id as kode'] = 'a.id as kode';
                     $select['b.name as costumer'] = 'b.name as costumer';
                     $select['b.id'] = 'b.id';
                     $select['b.param'] = 'b.param';
                     $select['a.type'] = 'a.type';
                     $select['a.price'] = 'a.price';
                     $select['c.type as type_merchant'] = 'c.type as type_merchant';
                     $select['c.name as merchant'] = 'c.name as merchant';
                     $select['d.admintrf'] = 'd.admintrf';
                     $select['d.admintrx'] = 'd.admintrx';
                     $select['d.issuertrx'] = 'd.issuertrx';
                     $select['d.issuertrf'] = 'd.issuertrf';
                     $select['e.name as issuer'] = 'e.name as issuer';
                     $select['f.name as pengikut'] = 'f.name as pengikut';
                   


                     $where1['b.param']  = $user->id;

                     if($costumer_id !="kosong")
                     { 
                        $where1['costumer_id']  = ''.$costumer_id.'';
                     }

                   


                     $transaction = DB::table('transaction as a');
                    
                     $sama = '=';

                     //relasi user
                     $dbuser1 = 'users as b';
                     $reldbuser1 = 'a.costumer_id';
                     $reldbdep1 = 'b.id'; 
                    
                    
                    //relasi user
                     $dbuser2 = 'users as c';
                     $reldbuser2 = 'a.user_id';
                     $reldbdep2 = 'c.id'; 




                    //setting
                    $dbsetting = 'setting as d';
                    $reldbsetting = 'd.user_id';
                    $reldbsetting1 = 'b.param'; 


                    //relasi user
                     $dbuser3 = 'users as e';
                     $reldbuser3 = 'e.id';
                     $reldbdep3 = 'c.param'; 
                    
                    //relasi user
                     $dbuser4 = 'users as f';
                     $reldbuser4 = 'f.id';
                     $reldbdep4 = 'b.param';

                    
                    if($search !="")
                     { 

                       $where2  = 'b.name'; 
                       $like    = 'like';
                       $search  = ''.$search.'%';

                       $deposite = $transaction->select($select)->where($where1)->where($where2,$like,$search)->join($dbuser1, $reldbuser1, $sama, $reldbdep1)->join($dbuser2, $reldbuser2, $sama, $reldbdep2)->join($dbsetting, $reldbsetting, $sama, $reldbsetting1)->join($dbuser3, $reldbuser3, $sama, $reldbdep3)->join($dbuser4, $reldbuser4, $sama, $reldbdep4)->orderBy('a.id','desc')->paginate($limit);

                     }else{

                          $deposite = $transaction->select($select)->join($dbuser1, $reldbuser1, $sama, $reldbdep1)->join($dbuser2, $reldbuser2, $sama, $reldbdep2)->join($dbsetting, $reldbsetting, $sama, $reldbsetting1)->join($dbuser3, $reldbuser3, $sama, $reldbdep3)->join($dbuser4, $reldbuser4, $sama, $reldbdep4)->orderBy('a.id','desc')->paginate($limit);
                     }



                    //  $this->depositeRepository->pushCriteria(new TransactionCriteria($request));
                    //  $this->depositeRepository->pushCriteria(new RequestCriteria($request));
                     
                    // $link = $this->depositeRepository->paginate(10);

                    
                    
                    return $deposite;
                    
                    // return view('transaction.table.depjs')
                    // ->with(['transaction' => $deposite,'type' => $type,'page'=>$page,'btn'=>$offset]);
                   


    }

    public function checkstatus(){

		$user   = Auth::user();
  		$data = DB::table('users')->select('type')->where('id',$user->id)->get();
  		$return['data'] = $data;
  		return $return;

    }


    public function printorder(Request $request)
    {
    	$user   = Auth::user();	
    	$user   = Auth::user();	
    	 //select transaksi
                     $select['a.id as kode'] = 'a.id as kode';
                     $select['b.name as costumer'] = 'b.name as costumer';
                     $select['b.id'] = 'b.id';
                     $select['b.param'] = 'b.param';
                     $select['a.type'] = 'a.type';
                     $select['a.price'] = 'a.price';
                     $select['c.type as type_merchant'] = 'c.type as type_merchant';
                     $select['c.name as merchant'] = 'c.name as merchant';
                     $select['d.admintrf'] = 'd.admintrf';
                     $select['d.admintrx'] = 'd.admintrx';
                     $select['d.issuertrx'] = 'd.issuertrx';
                     $select['d.issuertrf'] = 'd.issuertrf';
                     $select['e.name as issuer'] = 'e.name as issuer';
                     $select['f.name as pengikut'] = 'f.name as pengikut';
                   


                     $where1['b.param']  = $user->id;

                 
                   


                     $transaction = DB::table('transaction as a');
                    
                     $sama = '=';

                     //relasi user
                     $dbuser1 = 'users as b';
                     $reldbuser1 = 'a.costumer_id';
                     $reldbdep1 = 'b.id'; 
                    
                    
                    //relasi user
                     $dbuser2 = 'users as c';
                     $reldbuser2 = 'a.user_id';
                     $reldbdep2 = 'c.id'; 




                    //setting
                    $dbsetting = 'setting as d';
                    $reldbsetting = 'd.user_id';
                    $reldbsetting1 = 'b.param'; 


                    //relasi user
                     $dbuser3 = 'users as e';
                     $reldbuser3 = 'e.id';
                     $reldbdep3 = 'c.param'; 
                    
                    //relasi user
                     $dbuser4 = 'users as f';
                     $reldbuser4 = 'f.id';
                     $reldbdep4 = 'b.param';

              

                          $deposite = $transaction->select($select)->join($dbuser1, $reldbuser1, $sama, $reldbdep1)->join($dbuser2, $reldbuser2, $sama, $reldbdep2)->join($dbsetting, $reldbsetting, $sama, $reldbsetting1)->join($dbuser3, $reldbuser3, $sama, $reldbdep3)->join($dbuser4, $reldbuser4, $sama, $reldbdep4)->where('a.costumer_id',$user->id)->orderBy('a.id','desc')->get();
  		
  		return view('transaction.datatable')->with('print', $deposite);

                     
    } 	



    /**
     * Show the form for creating a new Transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('transaction.create');
    }

    /**
     * Store a newly created Transaction in storage.
     *
     * @param CreateTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionRequest $request)
    {
        $input = $request->all();

        $transaction = $this->transactionRepository->create($input);

        Flash::success('Transaction saved successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Display the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.show')->with('transaction', $transaction);
    }

    /**
     * Show the form for editing the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.edit')->with('transaction', $transaction);
    }

    /**
     * Update the specified Transaction in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionRequest $request)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        $transaction = $this->transactionRepository->update($request->all(), $id);

        Flash::success('Transaction updated successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Remove the specified Transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        
	$transaction = DB::table('transaction')->where('id',$id)->get();
        if (empty($transaction)) {
            Flash::error('Transaksi tidak ada!');

           return redirect(url('order'));
        }

       $transaction = DB::table('transaction')->where('id',$id)->delete();

        Flash::success('Transaksi berhasil dihapus.');

        return redirect(url('order'));
    }
}
