<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repositories\PostRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;
use Config;
use File;
use DB;
use App\Criteria\PostCriteria;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;
    protected $url;

    public function __construct(PostRepository $postRepo,UrlGenerator $url)
    {
        $this->postRepository = $postRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
	$this->postRepository->pushCriteria(new PostCriteria($request));
        $this->postRepository->pushCriteria(new RequestCriteria($request));
        $post = $this->postRepository->all();

        return view('post.index')
            ->with('post', $post);
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();
        $photox = base64_encode($request->photo);
        $user = Auth::user();

                if (empty($request->photo)) 
                {
                    $photo = Config::get('app.photo_default');
                    $input['photo'] = $this->url->to('/'.$photo);      

                }else {

                    $fileFormat = ".jpg";
                    $userPath = base64_encode($user->email);
                    $dir = Config::get('elfinder.dir')[0];
                    $files = $dir."/".$userPath;
                    $fileName = base64_encode($user->email . time());
                    $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
                    File::makeDirectory($files, 0777, true, true);

                        if ($image = base64_decode($photox, true))
                        {
                            $img = Image::make($image)->save($fullPath, 60);
                            $input['photo'] = $this->url->to('/'.$fullPath);
                        } else {
                            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                        }
                     
                }


        $post = $this->postRepository->create($input);


        $message = "Promo Terbaru '.$post->title.'";
        $userqr = DB::table('users')->where('status','1')->get();
        foreach($userqr as $row)
        {
            $reg_id = ''.$row->fcm.'';
            $fcm = $this->send_fcm($reg_id,$message);

        }


        Flash::success('Post saved successfully.');

       return redirect(url('promo'));
    }

    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('post.index'));
        }

        return view('post.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('post.index'));
        }

        return view('post.edit')->with('post', $post);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        $input = $request->all();
        $post = $this->postRepository->findWithoutFail($id);
        $user = Auth::user();


        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('post.index'));
        }

        $photox = base64_encode($request->photo);


                if (empty($request->photo)) 
                {

                    if($post->photo =="")
                    {
                          $photo = Config::get('app.photo_default');
                          $input['photo'] = $this->url->to('/'.$photo); 

                    }else{

                         $input['photo'] = $post->photo;

                    }    


                       

                }else {

                    $al = explode('/', $post->photo);
                    if($al[3] == 'img')
                    {
 

                           


                    }else{


                                $ph = explode('files', $post->photo);
                                $gambar = explode('/', $ph[1]);
                                $folder = $gambar[1];
                                $file = $gambar[2];
                                unlink('files/'.$folder.'/'.$file);

                    }    
                     
                }

                            $fileFormat = ".jpg";
                            $userPath = base64_encode($user->email);
                            $dir = Config::get('elfinder.dir')[0];
                            $files = $dir."/".$userPath;
                            $fileName = base64_encode($user->email . time());
                            $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
                            File::makeDirectory($files, 0777, true, true);

                                if ($image = base64_decode($photox, true))
                                {
                                    $img = Image::make($image)->save($fullPath, 60);
                                    $input['photo'] = $this->url->to('/'.$fullPath);
                                } else {
                                    return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                                }        



        $post = $this->postRepository->update($input, $id);

        $message = "Promo Terbaru '.$post->title.'";
        $userqr = DB::table('users')->where('status','1')->get();
        foreach($userqr as $row)
        {
            $reg_id = $row->fcm;
            $fcm = $this->send_fcm($reg_id,$message);

        }

        Flash::success('Post updated successfully.');

        return redirect(url('promo'));
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('post.index'));
        }

       
        $ph = explode('files', $post->photo);
        $gambar = explode('/', $ph[1]);
        $folder = $gambar[1];
        $file = $gambar[2];
        unlink('files/'.$folder.'/'.$file);

        $this->postRepository->delete($id);

        Flash::success('Post deleted successfully.');

        return redirect(route('post.index'));
    }

    public function send_fcm($fcm_id,$message)
    {

    $api_access_key = "AAAAhg-KJAI:APA91bGbK1hEG8RUMS_7yEz8MMo3NC4LMUkbABsQ4QlZy2858MEOEMMs179l5oPM3u3dpmL0BKVq5ilqs5i0l8ELGBZxoGpNGtdN0bFgVNv4YDmoGCbreEcq7SCxp1oRKDPjN15qA8qN";
       
    $registrationIds = $fcm_id;
    $fields = array
            (
                'to'        => $registrationIds,
                'priority' => 'high',
                'data'  => $message
            );

    $headers = array (
        'Authorization: key=' . $api_access_key,
        'Content-Type: application/json'
    );


#Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
#Echo Result Of FireBase Server
// echo $result;

    }
}
