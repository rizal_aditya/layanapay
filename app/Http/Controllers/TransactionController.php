<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Repositories\TransactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\TransactionCriteria;
use Response;
use Auth;
use App\User;
use DB;


class TransactionController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepo)
    {
        $this->transactionRepository = $transactionRepo;
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $cari = $request->get('cari', null);
        $type = $request['type'];
       
        $this->transactionRepository->pushCriteria(new RequestCriteria($request));
        $transaction = $this->transactionRepository->paginate(10);

         if ($cari!=null)
         {
          return redirect('transaction/?type='.$type.'&search=title:'.$cari.'&searchFields=title:like');
         }

         return view('transaction.index')
        ->with(['transaction' => $transaction,'type' => $type ]);
    }

    public function enable($id,Request $request)
    {

        $user   = Auth::user();
        $transaction = $this->transactionRepository->findWithoutFail($id);
        $type = $request['type'];
       
        if (empty($transaction)) {
            Flash::error('Transaction Deposite not found');

            return redirect('transaction/?type='.$type);

        }



        $status['status'] = '1';
        $saldo = $transaction->denom;

        $total = $user->saldo+$saldo;

        $user = $transaction->user_id;
        $transaction = $this->transactionRepository->update($status, $id);

        $us = User::where('id', $user)->update(['saldo' => $total]);
        
        //gcm update top up deposite
        
        DB::table('history')->where('transaction_id', $id)->update(['status' => '1']);
        
    
        

        Flash::success('Transaction updated successfully.');
       return redirect('transaction/?type='.$type);
        
    }


    public function disable($id,Request $request)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);
        $type = $request['type'];
       
        if (empty($transaction)) {
            Flash::error('Transaction not found');

           return redirect('transaction/?type='.$type);

        }

       $status['status'] = '0';
       $saldo = '0';
       $user = $transaction->user_id;
       $transaction = $this->transactionRepository->update($status, $id);
       
       $us = User::where('id', $user)->update(['saldo' => $saldo]);
      

        Flash::success('Transaction updated successfully.');
        return redirect('transaction/?type='.$type);
        
    }



    /**
     * Show the form for creating a new Transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('transaction.create');
    }

    /**
     * Store a newly created Transaction in storage.
     *
     * @param CreateTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionRequest $request)
    {
        $input = $request->all();

        $transaction = $this->transactionRepository->create($input);

        Flash::success('Transaction saved successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Display the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.show')->with('transaction', $transaction);
    }

    /**
     * Show the form for editing the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.edit')->with('transaction', $transaction);
    }

    /**
     * Update the specified Transaction in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionRequest $request)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        $transaction = $this->transactionRepository->update($request->all(), $id);

        Flash::success('Transaction updated successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Remove the specified Transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        $this->transactionRepository->delete($id);

        Flash::success('Transaction deleted successfully.');

        return redirect(route('transaction.index'));
    }
}
