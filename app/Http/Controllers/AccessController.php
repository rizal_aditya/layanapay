<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateActivityRequest;
use App\Http\Requests\UpdateActivityRequest;
use App\Repositories\ActivityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class AccessController extends AppBaseController
{
    /** @var  ActivityRepository */
    private $activityRepository;

    public function __construct(ActivityRepository $activityRepo)
    {
        $this->activityRepository = $activityRepo;
    }

    /**
     * Display a listing of the Activity.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        echo "tes";
    }

    public function login(Request $request)
    {
        $params = json_decode(file_get_contents('php://input'),true);

  	if($params==null || $params =="" || $params==" ")
  	{
    		echo 'Data belum lengkap data tidak bisa akses';
    		return 'Data belum lengkap data tidak bisa akses';
  	}

	$email = $params['email'];
	$api_key = $params['api_key'];
	$api_secret = $params['api_secret'];
	
	if(empty($email))
	{
		return Response::json(ResponseUtil::makeError('Email Kosong'), 404);
	}
	
	if(empty($api_key))
	{
		return Response::json(ResponseUtil::makeError('Api Key Kosong'), 404);
	}

	if(empty($api_secret))
	{
		return Response::json(ResponseUtil::makeError('Api Secret Kosong'), 404);
	}


	$emailID = DB::table('client')->where('email',$email)->get();
	if(!empty($emailID))
	{

	  $api_keyID = DB::table('client')->where('api_key',$api_key)->get();
	  if(!empty($api_keyID))
	  {

             $api_secretID = DB::table('client')->where('api_secret',$api_secret)->get();
	     if(!empty($api_secretID))
	     {	

		//sukses
		$ip_address = $api_secretID[0]->ip_address;

		$myUrl = "http://kartuvirtual.com";
		$myParsedURL = parse_url($myUrl);
		$myDomainName= $myParsedURL['host'];
		$ipAddress = gethostbyname($myDomainName);

		echo $ipAddress.' '.$_SERVER['REMOTE_ADDR'];
		

	     }else{

		return Response::json(ResponseUtil::makeError('Api Secret Tidak Terdaftar'), 404);

	     }	

	  }else{

		return Response::json(ResponseUtil::makeError('Api Key Tidak Terdaftar'), 404);

	  }		


	}else{

	  return Response::json(ResponseUtil::makeError('Email Tidak Terdaftar'), 404);

	}


	
    }



 public function reset()
{

$setting = DB::table('setting')->update(['count'=>'10']);

}
	

    /**
     * Show the form for creating a new Activity.
     *
     * @return Response
     */
    public function create()
    {
        return view('activity.create');
    }

    /**
     * Store a newly created Activity in storage.
     *
     * @param CreateActivityRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityRequest $request)
    {
        $input = $request->all();

        $activity = $this->activityRepository->create($input);

        Flash::success('Activity saved successfully.');

        return redirect(route('activity.index'));
    }

    /**
     * Display the specified Activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            Flash::error('Activity not found');

            return redirect(route('activity.index'));
        }

        return view('activity.show')->with('activity', $activity);
    }

    /**
     * Show the form for editing the specified Activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            Flash::error('Activity not found');

            return redirect(route('activity.index'));
        }

        return view('activity.edit')->with('activity', $activity);
    }

    /**
     * Update the specified Activity in storage.
     *
     * @param  int              $id
     * @param UpdateActivityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityRequest $request)
    {
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            Flash::error('Activity not found');

            return redirect(route('activity.index'));
        }

        $activity = $this->activityRepository->update($request->all(), $id);

        Flash::success('Activity updated successfully.');

        return redirect(route('activity.index'));
    }

    /**
     * Remove the specified Activity from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            Flash::error('Activity not found');

            return redirect(route('activity.index'));
        }

        $this->activityRepository->delete($id);

        Flash::success('Activity deleted successfully.');

        return redirect(route('activity.index'));
    }
}
