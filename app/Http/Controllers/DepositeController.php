<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Repositories\DepositeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\TransactionCriteria;
use Response;
use Auth;
use DB;
use App\Models\Deposite;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerDep;
use App\Mail\VerWith;
use App\Mail\VerUser;
use Config;
use File;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;
use QrCode;

class DepositeController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $depositeRepository;
    protected $url;

    public function __construct(DepositeRepository $depositeRepo, UrlGenerator $url)
    {
        $this->depositeRepository = $depositeRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    
    public function tes()
    {
      
    }

    


    public function jmldeposite(Request $request)
    {
        $transaction = "";
       
        $deposite = DB::table('transaction')->get();
        if(!empty($deposite))
        {

                $jml = DB::table('transaction')->where(['type'=>'deposite'])->count();
                if($jml !="0")
                {
                
                  $transaction = DB::table('transaction')->where(['type'=>'deposite','notif'=>'0'])->count();  

                }else{

                  $transaction = "";
                  
                }    
            
        }else{

          $deposite = "";
        }   

        

        return $transaction;
    }

    public function jmlwithdraw(Request $request)
    {
        $transaction = "";
       
        $deposite = DB::table('transaction')->get();
        if(!empty($deposite))
        {

                $jml = DB::table('transaction')->where(['type'=>'withdraw'])->count();
                if($jml !="0")
                {
                
                  $transaction = DB::table('transaction')->where(['type'=>'withdraw','notif'=>'0'])->count();  

                }else{

                  $transaction = "";
                  
                }    
            
        }else{

          $deposite = "";
        }   

        

        return $transaction;
    }

     public function jmluser(Request $request)
    {
        $userkyc = "";
       
        $user = DB::table('users')->get();
        if(!empty($user))
        {

                $jml = DB::table('users')->where(['type'=>'costumer'])->count();
                if($jml !="0")
                {
                
                  $userkyc = DB::table('users')->where(['kyc'=>'1','notif'=>'0'])->count();  

                }else{

                  $userkyc = "";
                  
                }    
            
        }else{

          $user = "";
        }   

        

        return $userkyc;
    }

    public function create(Request $request)
    {

        $type = $request->get('type');
        return view('user.create')
            ->with('type', $type);
    }

    public function listuserdeposite(Request $request)
    {

          //select transaksi
           $select['a.id'] = 'a.id';
           $select['a.tanggal'] = 'a.tanggal';
           $select['a.type'] = 'a.type';
           $select['a.status'] = 'a.status';
           $select['a.price']   = 'a.price'; 
           $select['a.last_balance'] = 'a.last_balance';
           $select['a.created_at'] = 'a.created_at';
           // select user
           $select['b.name'] = 'b.name';
           $select['b.photo'] = 'b.photo';
           $select['b.saldo'] = 'b.saldo';

          //relasi user
           $dbuser = 'users as b';
           $reldbuser = 'b.id';
           $reldbdep1 = 'a.user_id'; 
           $sama = '=';

           $deposite = DB::table('transaction as a')->select($select)->join($dbuser, $reldbuser, $sama, $reldbdep1)->where('a.type','deposite')->orderBy('id','desc')->paginate(7);
         
          return $deposite;

    }


    
     public function listtabeldeposite(Request $request)
    {
       
                    $type = "deposite"; 
                    $status = $request->get('status'); 
                    $pages = $request->get('page');
                    $search = $request->get('search');



                     $limit = "10";
                    

                     //select transaksi
                     $select['a.id'] = 'a.id';
                     $select['a.tanggal'] = 'a.tanggal';
                     $select['a.type'] = 'a.type';
                     $select['a.status'] = 'a.status';
                     $select['a.price']   = 'a.price'; 
                     $select['a.last_balance'] = 'a.last_balance';
                     $select['a.created_at'] = 'a.created_at';
                     // select user
                     $select['b.name'] = 'b.name';
                     $select['b.photo'] = 'b.photo';
                     $select['b.saldo'] = 'b.saldo';
                     //select paket
                     $select['c.name as packet'] = 'c.name as packet';
                     //select bank costumer
                     $select['d.name as bank_costumer'] = 'd.name as bank_costumer';
                     $select['d.account_name as account_costumer'] = 'd.account_name as account_costumer';
                     //select bank admin
                     //$select['a.bank_send'] = 'a.bank_send';
                     $select['e.name as bank_admin'] = 'e.name as bank_admin';
                     $select['e.account_name as account_admin'] = 'e.account_name as account_admin';


                     $where1['a.type']  = $type;

                     if($status !="10")
                     { 
                        $where1['a.status']  = ''.$status.'';
                     }

                   


                     $transaction = DB::table('transaction as a');
                    
                     $sama = '=';

                     //relasi user
                     $dbuser = 'users as b';
                     $reldbuser = 'b.id';
                     $reldbdep1 = 'a.user_id'; 
                    
                    
                    //relasi packet
                    $dbpaket = 'packet as c';
                    $reldbpaket = 'c.id';
                    $reldbdep2 = 'a.packet_id'; 

                    //bank costumer
                    $dbbankc = 'bank as d';
                    $reldbbankc = 'd.id';
                    $reldbdep3 = 'a.bank_id'; 

                     //bank admin
                    $dbbanka = 'bank as e';
                    $reldbbanka = 'e.id';
                    $reldbdep4 = 'a.bank_send'; 

                    
                    if($search !="")
                     { 

                       $where2  = 'b.name'; 
                       $like    = 'like';
                       $search  = ''.$search.'%';

                       $deposite = $transaction->select($select)->where($where1)->where($where2,$like,$search)->join($dbuser, $reldbuser, $sama, $reldbdep1)->join($dbpaket, $reldbpaket, $sama, $reldbdep2)->join($dbbankc, $reldbbankc, $sama, $reldbdep3)->join($dbbanka, $reldbbanka, $sama, $reldbdep4)->orderBy('a.id','desc')->paginate($limit);

                     }else{

                        $deposite = $transaction->where($where1)->select($select)->join($dbuser, $reldbuser, $sama, $reldbdep1)->join($dbpaket, $reldbpaket, $sama, $reldbdep2)->join($dbbankc, $reldbbankc, $sama, $reldbdep3)->join($dbbanka, $reldbbanka, $sama, $reldbdep4)->orderBy('a.id','desc')->paginate($limit);
                     }

       
                    
                    //  $this->depositeRepository->pushCriteria(new TransactionCriteria($request));
                    //  $this->depositeRepository->pushCriteria(new RequestCriteria($request));
                     
                    // $link = $this->depositeRepository->paginate(10);

                    return $deposite;
                    
                    // return view('transaction.table.depjs')
                    // ->with(['transaction' => $deposite,'type' => $type,'page'=>$page,'btn'=>$offset]);
                   
        
        
    }

    public function listuserwithdraw(Request $request)
    {

        //select transaksi
           $select['a.id'] = 'a.id';
           $select['a.tanggal'] = 'a.tanggal';
           $select['a.type'] = 'a.type';
           $select['a.status'] = 'a.status';
           $select['a.price']   = 'a.price'; 
           $select['a.last_balance'] = 'a.last_balance';
           $select['a.created_at'] = 'a.created_at';
           // select user
           $select['b.name'] = 'b.name';
           $select['b.photo'] = 'b.photo';
           $select['b.saldo'] = 'b.saldo';

          //relasi user
           $dbuser = 'users as b';
           $reldbuser = 'b.id';
           $reldbdep1 = 'a.user_id'; 
           $sama = '=';

           $withdraw = DB::table('transaction as a')->select($select)->join($dbuser, $reldbuser, $sama, $reldbdep1)->where('a.type','withdraw')->orderBy('id','desc')->paginate(7);
         
          return $withdraw;

    }

     public function listtabelwithdraw(Request $request)
    {
       
                    $type = "withdraw"; 
                    $status = $request->get('status'); 
                    $pages = $request->get('page');
                    $search = $request->get('search');



                     $limit = "10";
                    

                     //select transaksi
                     $select['a.id'] = 'a.id';
                     $select['a.tanggal'] = 'a.tanggal';
                     $select['a.type'] = 'a.type';
                     $select['a.status'] = 'a.status';
                     $select['a.price']   = 'a.price'; 
                     $select['a.last_balance'] = 'a.last_balance';
                     $select['a.created_at'] = 'a.created_at';



                     // select user
                     $select['b.name'] = 'b.name';
                     $select['b.photo'] = 'b.photo';
                     $select['b.saldo'] = 'b.saldo';



                     //select paket
                     $select['c.name as packet'] = 'c.name as packet';
                     //select bank costumer


                     $select['d.name as bank_costumer'] = 'd.name as bank_costumer';
                     $select['d.account_name as account_costumer'] = 'd.account_name as account_costumer';


                     //select bank admin
                     $select['a.bank_send'] = 'a.bank_send';
                     $select['e.name as bank_admin'] = 'e.name as bank_admin';
                     $select['e.account_name as account_admin'] = 'e.account_name as account_admin';


                     $where1['a.type']  = $type;

                     if($status !="10")
                     { 
                        $where1['a.status']  = ''.$status.'';
                     }

                   


                     $transaction = DB::table('transaction as a');
                    
                     $sama = '=';

                     //relasi user
                     $dbuser = 'users as b';
                     $reldbuser = 'b.id';
                     $reldbdep1 = 'a.user_id'; 
                    
                    
                    //relasi packet
                    $dbpaket = 'packet as c';
                    $reldbpaket = 'c.id';
                    $reldbdep2 = 'a.packet_id'; 

                    //bank costumer
                    $dbbankc = 'bank as d';
                    $reldbbankc = 'd.id';
                    $reldbdep3 = 'a.bank_id'; 

                     //bank admin
                    $dbbanka = 'bank as e';
                    $reldbbanka = 'e.id';
                   $reldbdep4 = 'a.bank_send'; 

                    
                    if($search !="")
                     { 

                       $where2  = 'b.name'; 
                       $like    = 'like';
                       $search  = ''.$search.'%';
                       //join($dbbanka, $reldbbanka, $sama, $reldbdep4)
                       $withdraw = $transaction->select($select)->where($where1)->where($where2,$like,$search)->join($dbuser, $reldbuser, $sama, $reldbdep1)->join($dbpaket, $reldbpaket, $sama, $reldbdep2)->join($dbbankc, $reldbbankc, $sama, $reldbdep3)->join($dbbanka, $reldbbanka, $sama, $reldbdep4)->orderBy('a.id','desc')->paginate($limit);

                     }else{

                        $withdraw = $transaction->select($select)->where($where1)->join($dbuser, $reldbuser, $sama, $reldbdep1)->join($dbpaket, $reldbpaket, $sama, $reldbdep2)->join($dbbankc, $reldbbankc, $sama, $reldbdep3)->join($dbbanka, $reldbbanka, $sama, $reldbdep4)->orderBy('a.id','desc')->paginate($limit);
                     }

       
                    
                    //  $this->depositeRepository->pushCriteria(new TransactionCriteria($request));
                    //  $this->depositeRepository->pushCriteria(new RequestCriteria($request));
                     
                    // $link = $this->depositeRepository->paginate(10);

                    return $withdraw;
                    
                    // return view('transaction.table.depjs')
                    // ->with(['transaction' => $deposite,'type' => $type,'page'=>$page,'btn'=>$offset]);
                   
        
        
    }

     public function listuserkyc(Request $request)
    {

          //select transaksi
          
           // select user
           $select['b.name'] = 'b.name';
           $select['b.photo'] = 'b.photo';
           $select['b.saldo'] = 'b.saldo';
           $select['b.kyc'] = 'b.kyc';
           $select['b.created_at'] = 'b.created_at';

           $user = DB::table('users as b')->select($select)->orderBy('id','desc')->paginate(7);
         
           return $user;

    }

      public function listtabeluser(Request $request)
    {
       
                    $type = "costumer"; 
		    $param  = Auth::user()->id;	
                    $status = $request->get('status'); 
                    $pages = $request->get('page');
                    $search = $request->get('search');
                    $select['a.id'] = "a.id";  
                    $select['a.name as costumer'] = "a.name as costumer";
                    $select['a.name'] = "a.name";
                    $select['a.saldo'] = "a.saldo";
                    $select['a.email'] = "a.email";
                    $select['a.phone'] = "a.phone";
                    $select['a.photo'] = "a.photo";
                    $select['a.kyc'] = "a.kyc";

                     //$select['d.name as bank'] = "d.name as bank";
                     //$select['d.account_name'] = "d.account_name";
                     //$select['d.account_number'] = "d.account_number";

                     $limit = "10";
                     $where1['a.type']  = $type;

                     if($status !="10")
                     { 
                        $where1['a.status']  = ''.$status.'';
                     }

                   


                     $transaction = DB::table('users as a');
                     $sama = '=';

                    
                    //bank costumer
                    $dbbanka = 'bank as d';
                    $reldbbanka = 'a.id';
                    $reldbdep3 = 'd.user_id'; 
                    
			$userid = Auth::user();
                    
                    if($search !="")
                     { 

                       $where2  = 'a.name'; 
                       $like    = 'like';
                       $search  = ''.$search.'%';

			if($userid->hasRole('admin'))
        		{ 
				 $deposite = $transaction->select($select)->where(['type'=>$type])->join($dbbanka, $reldbbanka, $sama, $reldbdep3)->where($where2,$like,$search)->orderBy('a.id','desc')->paginate($limit);
			}else{
			 	 $deposite = $transaction->select($select)->where(['type'=>$type,'param'=>$param])->join($dbbanka, $reldbbanka, $sama, $reldbdep3)->where($where2,$like,$search)->orderBy('a.id','desc')->paginate($limit);			
			}


                      

                     }else{
			if($userid->hasRole('admin'))
        		{ 

                        	$deposite = $transaction->select($select)->where(['type'=>$type])->orderBy('a.id','desc')->paginate($limit);

			}else{
				
				$deposite = $transaction->select($select)->where(['type'=>$type,'param'=>$param])->orderBy('a.id','desc')->paginate($limit);
			}
                     }

       
                    
                  

                    return $deposite;
                    
                
        
    }

    /* fungsi untuk populate data table merchant */
    public function listtabelmerchant(Request $request)
    {
      $type = "merchant"; 
      $param  = Auth::user()->id;

      $status = $request->get('status'); 
      $pages = $request->get('page');
      $search = $request->get('search');
      $select['a.id'] = "a.id";  
      $select['a.name as costumer'] = "a.name as costumer";
      $select['a.name'] = "a.name";
      // $select['a.saldo'] = "a.saldo";
      $select['a.saldo'] = "a.saldo";
      $select['a.email'] = "a.email";
      $select['a.phone'] = "a.phone";
      $select['a.photo'] = "a.photo";
      $select['a.status'] = "a.status";

       //$select['d.name as bank'] = "d.name as bank";
       //$select['d.account_name'] = "d.account_name";
       //$select['d.account_number'] = "d.account_number";

       $limit = "10";
       $where1['a.type']  = $type;

       if($status !="10")
       { 
          $where1['a.status']  = $status;
       }

       $transaction = DB::table('users as a');
       $sama = '=';

      
      //bank costumer
      $dbbanka = 'bank as d';
      $reldbbanka = 'a.id';
      $reldbdep3 = 'd.user_id'; 
      
	$userid = Auth::user();
      
      if($search !="")
       { 
         $where2  = 'a.name'; 
         $like    = 'like';
         $search  = ''.$search.'%';

		
        	if($userid->hasRole('admin'))
        	{ 

         		$deposite = $transaction->select($select)->where(['type'=>$type])->join($dbbanka, $reldbbanka, $sama, $reldbdep3)->where($where2,$like,$search)->orderBy('a.id','desc')->paginate($limit);
       		}else{

			$deposite = $transaction->select($select)->where(['type'=>$type,'param'=>$param])->join($dbbanka, $reldbbanka, $sama, $reldbdep3)->where($where2,$like,$search)->orderBy('a.id','desc')->paginate($limit);
       		}
			
	}else{
		
		if($userid->hasRole('admin'))
        	{ 
			$deposite = $transaction->select($select)->where(['type'=>$type])->orderBy('a.id','desc')->paginate($limit);
		}else{
			$deposite = $transaction->select($select)->where(['type'=>$type,'param'=>$param])->orderBy('a.id','desc')->paginate($limit);
		}
          		
       }
      return $deposite;
    }

    public function postmerchant(Request $req)
    {
      if (!$req->hasFile('mer_photo')) 
      {
        $photo = Config::get('app.photo_default');
        $co_potho = $this->url->to('/'.$photo);      
      }else if ($req->hasFile('mer_photo')) {
        $photox = base64_encode($req->file('mer_photo'));
        $fileFormat = ".jpg";
        $userPath = base64_encode($req->input('mer_email'));
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$userPath;
        $fileName = base64_encode($req->input('mer_email') . time());
        $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        File::makeDirectory($files, 0777, true, true);

        if ($image = base64_decode($photox, true))
        {
            $img = Image::make($image)->save($fullPath, 60);
            $co_potho = $this->url->to('/'.$fullPath);
        } else {
            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
        }  
      }
      $kosong = '';

      $clien_id = Auth::user()->client_id;

      /* Generate pin user */
      $secure = sprintf("%06d", mt_rand(1, 999999));
      $security = $secure;
      $co_pin = $security; 

      /* cek kelengkapan data */ 
      if($req->input('mer_name') != '' && $req->input('mer_email') != '' && $req->input('mer_phone') != '' && $req->input('mer_city') != '' && $req->input('mer_postal') != '' && $req->input('mer_address') != '' && $req->hasFile('mer_photo'))
      {
        $kyc = 1;
      }
      else
      {
        $kyc = 0;
      }

               
       $latest = DB::table('users')->max('code_reveral');
        // var_dump($latest);
        // die();
        if (empty($latest)) { 
            $reveral = '1000001';
        }else{
            if($latest =="0")
             {
                $reveral = '1000001';
             }else{

                $reveral = $latest+1;  
             } 
           
        }
  
       



      $user = [
                'param' => Auth::user()->id,
                'client_id' => $clien_id,
                'name' => $req->input('mer_name'),
                'saldo' => 0,
                'email' => $req->input('mer_email'),
                'pin' => $co_pin,
                'qrcode' => $kosong,
                'img_qrcode' => $kosong,
                'img_sb' => $kosong,
                'img_location' => $kosong,
                'phone' => $req->input('mer_phone'),
                'pob' => $kosong,
                'dob' => date("Y-m-d H:i:s"),
                'gender' => $kosong,
                'code_reveral' => $reveral,
                'national' => $kosong,
                'city' => $req->input('mer_city'),
                'education' => $kosong,
                'job' => $kosong,
                'password' => bcrypt('123456'),
                'postal_code' => $req->input('mer_postal'),
                'photo' => $co_potho,
                'type_id' => $kosong,
                'identity_number' => $kosong,
                'identity_photo' => $kosong,
                'address' => $req->input('mer_address'),
                'type' => 'merchant',
                'status' => '1',
                'notif' => 0,
                'kyc' => $kyc,
                'fcm' => $kosong,
                'created_at' => date("Y-m-d H:i:s")
          ];
      $user_id = DB::table('users')->insertGetId($user);

      /* Generate qr */
      $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
                
      $fileFormatqr = ".png";
      $qrPath = base64_encode('qrcode');
      $dir = Config::get('elfinder.dir')[0];
      $files = $dir."/".$qrPath;

      $fileQr = base64_encode($kode.$user_id . time());
      $fullqr = $dir."/".$qrPath."/".$fileQr.$fileFormatqr;
      File::makeDirectory($files, 0777, true, true);


      $png = QrCode::format('png')->size(500)
          ->merge('../public/img/iconqr.png', .1, true)
              ->generate(''.$kode.$user_id.'', '../public/'.$fullqr);
      /* Update data qr user */
      DB::table('users')->where('id',$user_id)->update(['qrcode'=> $kode.$user_id,'img_qrcode'=> $this->url->to($fullqr) ]);

      $role_user = [
                'role_id' => 4,
                'user_id' => $user_id
          ];
      $role_user_id = DB::table('role_user')->insertGetId($role_user);

      return redirect('/user?type=merchant');
    }

    public function posteditmerchant(Request $req)
    {
      $temp_user = DB::table('users')->where('id', '=', $req->input('_id'))->get();
      $user = array();
      if ($req->hasFile('mer_photo')) 
      {
        $photox = base64_encode($req->file('mer_photo'));
        $fileFormat = ".jpg";
        $userPath = base64_encode($req->input('mer_email'));
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$userPath;
        $fileName = base64_encode($req->input('mer_email') . time());
        $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        File::makeDirectory($files, 0777, true, true);

        if ($image = base64_decode($photox, true))
        {
          $img = Image::make($image)->save($fullPath, 60);
          $co_potho = $this->url->to('/'.$fullPath);
          $user['photo'] = $co_potho;
          /* Cek apakah fotonya default kalau tidak di unlink */
          if(strpos($temp_user[0]->photo, 'img/avatar.png') === false)
          {
            $temp_foto = explode("/", $temp_user[0]->photo);
            $location = 'files/'.$temp_foto[count($temp_foto) - 2].'/'.$temp_foto[count($temp_foto) - 1];
            unlink(public_path($location));
          }
        } else {
          return Response::json(ResponseUtil::makeError('Image not valid'), 404);
        } 
      }

      /* cek kelengkapan data */ 
      if($req->input('mer_name') != '' && $req->input('mer_email') != '' && $req->input('mer_phone') != '' && $req->input('mer_city') != '' && $req->input('mer_postal') != '' && $req->input('mer_address') != '' && (strpos($temp_user[0]->photo, 'img/avatar.png') === false || $req->hasFile('mer_photo')))
      {
        $kyc = 1;
      }
      else
      {
        $kyc = 0;
      }

      $user['name'] = $req->input('mer_name');
      $user['email'] = $req->input('mer_email');
      $user['phone'] = $req->input('mer_phone');
      $user['city'] = $req->input('mer_city');
      $user['postal_code'] = $req->input('mer_postal');
      $user['address'] = $req->input('mer_address');
      $user['updated_at'] = date("Y-m-d H:i:s");
      $user['kyc'] = $kyc;
      DB::table('users')->where('id', '=', $req->input('_id'))->update($user);

      $client = [
          'name' => $req->input('mer_name'),
          'email' => $req->input('mer_email'),
          'updated_at' => date("Y-m-d H:i:s")
      ];
      DB::table('client')->where('id', '=', $temp_user[0]->client_id)->update($client);

      return redirect('/user?type=merchant');
    }

    public function posthapusmerchant(Request $req)
    {
      DB::table('users')->where('id',$req->input('id'))->update(['status'=> '0']);
    }

    public function detailmerchant($id)
    {
      $data = DB::table('users')->where('id', $id)->get();
      return json_encode($data);
    }




    /* fungsi untuk populate data table co-branding */
    public function listtabelcobrand(Request $request)
    {
      $type = "cobranding"; 
      $param  = Auth::user()->id;

      $status = $request->get('status'); 
      $pages = $request->get('page');
      $search = $request->get('search');
      $select['a.id'] = "a.id";  
      $select['a.name as costumer'] = "a.name as costumer";
      $select['a.name'] = "a.name";
      $select['a.saldo'] = "a.saldo";
      //$select['b.saldo'] = "b.saldo";
      $select['a.email'] = "a.email";
      $select['a.phone'] = "a.phone";
      $select['a.photo'] = "a.photo";
      $select['a.status'] = "a.status";

       $select['a.param'] = "a.param";
       $select['a.client_id'] = "a.client_id";

       $limit = "10";
       $where1['a.type']  = $type;

       if($status !="10")
       { 
          $where1['a.status']  = $status;
       }

       $transaction = DB::table('users as a');
       $sama = '=';

      
      //bank costumer
      //$dbbanka = 'bank as d';
     // $reldbbanka = 'a.id';
      //$reldbdep3 = 'd.user_id'; 
      
	$userid = Auth::user();
      
      if($search !="")
       { 
         $where2  = 'a.name'; 
         $like    = 'like';
         $search  = ''.$search.'%';
	
		if($userid->hasRole('admin'))
        	{ 
			 $deposite = $transaction->select($select)->where(['a.type'=>$type])->where($where2,$like,$search)->orderBy('a.id','desc')->paginate($limit);

		}else{
			 $deposite = $transaction->select($select)->where(['a.type'=>$type,'a.param'=>$param])->where($where2,$like,$search)->orderBy('a.id','desc')->paginate($limit);
		}

        
       }else{
		
	 	
        	if($userid->hasRole('admin'))
        	{ 
			
          	$deposite = $transaction->select($select)->where(['a.type'=>$type])->orderBy('a.id','desc')->paginate($limit);
       
		}else{

		$deposite = $transaction->select($select)->where(['a.type'=>$type,'a.param'=>$param])->orderBy('a.id','desc')->paginate($limit);
       

		}

	}
      return $deposite;
    }

    public function posteditcobrand(Request $req)
    {
      $temp_user = DB::table('users')->where('id', '=', $req->input('_id'))->get();
      $user = array();
      if ($req->hasFile('co_photo')) 
      {
        $photox = base64_encode($req->file('co_photo'));
        $fileFormat = ".jpg";
        $userPath = base64_encode($req->input('co_email'));
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$userPath;
        $fileName = base64_encode($req->input('co_email') . time());
        $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        File::makeDirectory($files, 0777, true, true);

        if ($image = base64_decode($photox, true))
        {
          $img = Image::make($image)->save($fullPath, 60);
          $co_potho = $this->url->to('/'.$fullPath);
          $user['photo'] = $co_potho;
          /* Cek apakah fotonya default kalau tidak di unlink */
          if(strpos($temp_user[0]->photo, 'img/avatar.png') === false)
          {
            $temp_foto = explode("/", $temp_user[0]->photo);
            $location = 'files/'.$temp_foto[count($temp_foto) - 2].'/'.$temp_foto[count($temp_foto) - 1];
            unlink(public_path($location));
          }
        } else {
          return Response::json(ResponseUtil::makeError('Image not valid'), 404);
        } 
      }

      /* cek kelengkapan data */ 
      if($req->input('co_name') != '' && $req->input('co_email') != '' && $req->input('co_phone') != '' && $req->input('co_city') != '' && $req->input('co_postal') != '' && $req->input('co_address') != '' && (strpos($temp_user[0]->photo, 'img/avatar.png') === false || $req->hasFile('co_photo')))
      {
        $kyc = 1;
      }
      else
      {
        $kyc = 0;
      }

      $user['name'] = $req->input('co_name');
      $user['email'] = $req->input('co_email');
      $user['phone'] = $req->input('co_phone');
      $user['city'] = $req->input('co_city');
      $user['postal_code'] = $req->input('co_postal');
      $user['address'] = $req->input('co_address');
      $user['updated_at'] = date("Y-m-d H:i:s");
      $user['kyc'] = $kyc;
      DB::table('users')->where('id', '=', $req->input('_id'))->update($user);

      $client = [
          'name' => $req->input('co_name'),
          'email' => $req->input('co_email'),
          'updated_at' => date("Y-m-d H:i:s")
      ];
      DB::table('client')->where('id', '=', $temp_user[0]->client_id)->update($client);

      return redirect('/user?type=co-branding');
    }

    public function postcobrand(Request $req)
    {
      if (!$req->hasFile('co_photo')) 
      {
        $photo = Config::get('app.photo_default');
        $co_potho = $this->url->to('/'.$photo);      
      }else if ($req->hasFile('co_photo')) {
        $photox = base64_encode($req->file('co_photo'));
        $fileFormat = ".jpg";
        $userPath = base64_encode($req->input('co_email'));
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$userPath;
        $fileName = base64_encode($req->input('co_email') . time());
        $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        File::makeDirectory($files, 0777, true, true);

        if ($image = base64_decode($photox, true))
        {
            $img = Image::make($image)->save($fullPath, 60);
            $co_potho = $this->url->to('/'.$fullPath);
        } else {
            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
        }  
      }
      $kosong = '';
      $client = [
                'role_id' => 4,
                'name' => $req->input('co_name'),
                'email' => $req->input('co_email'),
                'api_key' => $kosong,
                'api_secret' => $kosong,
                'type' => 'cobranding',
                'ip_address' => $kosong,
                'status' => '0',
                'created_at' => date("Y-m-d H:i:s")
          ];
      $clien_id = DB::table('client')->insertGetId($client);

      /* Generate pin user */
      $secure = sprintf("%06d", mt_rand(1, 999999));
      $security = $secure;
      $co_pin = $security; 

      /* cek kelengkapan data */ 
      if($req->input('co_name') != '' && $req->input('co_email') != '' && $req->input('co_phone') != '' && $req->input('co_city') != '' && $req->input('co_postal') != '' && $req->input('co_address') != '' && $req->hasFile('co_photo'))
      {
        $kyc = 1;
      }
      else
      {
        $kyc = 0;
      }

      $user = [
                'param' => Auth::user()->id,
                'client_id' => $clien_id,
                'name' => $req->input('co_name'),
                'saldo' => 0,
                'email' => $req->input('co_email'),
                'pin' => $co_pin,
                'qrcode' => $kosong,
                'img_qrcode' => $kosong,
                'img_sb' => $kosong,
                'img_location' => $kosong,
                'phone' => $req->input('co_phone'),
                'pob' => $kosong,
                'dob' => date("Y-m-d H:i:s"),
                'gender' => $kosong,
                'national' => $kosong,
                'city' => $req->input('co_city'),
                'education' => $kosong,
                'job' => $kosong,
                'password' => bcrypt('123456'),
                'postal_code' => $req->input('co_postal'),
                'photo' => $co_potho,
                'type_id' => $kosong,
                'identity_number' => $kosong,
                'identity_photo' => $kosong,
                'address' => $req->input('co_address'),
                'type' => 'cobranding',
                'status' => '0',
                'notif' => 0,
                'kyc' => $kyc,
                'fcm' => $kosong,
                'created_at' => date("Y-m-d H:i:s")
          ];
      $user_id = DB::table('users')->insertGetId($user);

      /* Generate qr */
      $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
                
      $fileFormatqr = ".png";
      $qrPath = base64_encode('qrcode');
      $dir = Config::get('elfinder.dir')[0];
      $files = $dir."/".$qrPath;

      $fileQr = base64_encode($kode.$user_id . time());
      $fullqr = $dir."/".$qrPath."/".$fileQr.$fileFormatqr;
      File::makeDirectory($files, 0777, true, true);


      $png = QrCode::format('png')->size(500)
          ->merge('../public/img/iconqr.png', .1, true)
              ->generate(''.$kode.$user_id.'', '../public/'.$fullqr);
      /* Update data qr user */
      DB::table('users')->where('id',$user_id)->update(['qrcode'=> $kode.$user_id,'img_qrcode'=> $this->url->to($fullqr) ]);

      $role_user = [
                'role_id' => 4,
                'user_id' => $user_id
          ];
      $role_user_id = DB::table('role_user')->insertGetId($role_user);

      $setting_user = [
                'user_id' => $user_id,
                'mindepn' => 0,
                'maxdepn' => 0,
                'mindepk' => 0,
                'maxdepk' => 0,
                'mintrn' => 0,
                'maxtrn' => 0,
                'mintrk' => 0,
                'maxtrk' => 0,
                'minwdn' => 0,
                'maxwdn' => 0,
                'minwdk' => 0,
                'maxwdk' => 0,
                'admin' => 0,
                'saldo' => 0,
                'bank' => '',
                'account_number' => '',
                'account_name' => '',
                'count' => 0,
                'status' => 0,
                'created_at' => date("Y-m-d H:i:s"),
          ];
      $setting_id = DB::table('setting')->insertGetId($setting_user);

      $bank_user = [
                'name' => '',
                'user_id' => $user_id,
                'account_name' => '',
                'account_number' => '',
                'saldo' => 0,
                'branch' => '',
                'type' => '',
                'image' => '',
                'param' => 1,
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
          ];
      $bank_id = DB::table('bank')->insertGetId($bank_user);
      return redirect('/user?type=co-branding');
    }

    public function posthapuscobrand(Request $req)
    {
      DB::table('users')->where('id',$req->input('id'))->update(['status'=> '0']);
    }

    public function detailco($id)
    {
      $data = DB::table('users')->where('id', $id)->get();
      return json_encode($data);
    }


       public function detailuser(Request $request)
    {
       
                    // $type = "costumer"; 
                    $id = $request->get('id'); 
                    $type = $request->get('type'); 
                    if($type == 'co-branding'){
                      $type = 'cobranding';
                    }
                    
                    $select['a.id'] = "a.id";  
                    $select['a.name as costumer'] = "a.name as costumer";
                    $select['a.name'] = "a.name";
                    $select['a.saldo'] = "a.saldo";
                    $select['a.email'] = "a.email";
                    $select['a.phone'] = "a.phone";
                    $select['a.photo'] = "a.photo";
                    $select['a.kyc'] = "a.kyc";
                    $select['a.identity_photo'] = "a.identity_photo";
                    $select['a.identity_number'] = "a.identity_number";
                    

                     $limit = "2";
                     $where1['a.type']  = $type;
                     $where1['a.id']  = ''.$id.'';                    


                     $transaction = DB::table('users as a');
                     $sama = '=';

                     $dep = $transaction->select($select)->where($where1)->orderBy('a.id','desc')->get();

                     $select['d.name as bank'] = "d.name as bank";
                     $select['d.account_name'] = "d.account_name";
                     $select['d.account_number'] = "d.account_number";
                    
                    //bank costumer
                    $dbbanka = 'bank as d';
                    $reldbbanka = 'a.id';
                    $reldbdep3 = 'd.user_id'; 


                    $deposite = $transaction->select($select)->where($where1)->join($dbbanka, $reldbbanka, $sama, $reldbdep3)->orderBy('a.id','desc')->get();
  
                    
                    $temp = !empty($deposite) ? $deposite : $dep;
                    return $temp;
    }


     public function konfirmasi(Request $request)
    {
          
              $id = $request->get('id');  


              $select['b.id as user_id'] = 'b.id as user_id';   
              $select['b.name'] = 'b.name';
             
              $select['b.saldo'] = 'b.saldo';
              $select['b.name'] = 'b.name';
              $select['b.photo'] = 'b.photo';
              $select['b.phone'] = 'b.phone';  
              $select['b.email'] = 'b.email'; 
              $select['a.id as transaction_id'] = 'a.id as transaction_id';
              $select['a.price'] = 'a.price';
              $select['a.tanggal'] = 'a.tanggal';
              $select['a.type'] = 'a.type';
              $select['a.status'] = 'a.status';
              $select['a.price']   = 'a.price'; 
              $select['a.last_balance'] = 'a.last_balance';
              $select['a.created_at'] = 'a.created_at';
             


               $select['c.name as packet'] = 'c.name as packet';
               //select bank costumer
               $select['d.name as bank_costumer'] = 'd.name as bank_costumer';
               $select['d.account_name as account_costumer'] = 'd.account_name as account_costumer';
               $select['d.account_number as no_account_costumer'] = 'd.account_number as no_account_costumer';
               //select bank admin
               //$select['a.bank_send'] = 'a.bank_send';
               //$select['e.name as bank_admin'] = 'e.name as bank_admin';
               //$select['e.account_name as account_admin'] = 'e.account_name as account_admin';



              $sama = '=';

             //relasi user
             $dbuser = 'users as b';
             $reldbuser = 'b.id';
             $reldbdep1 = 'a.user_id';

            //relasi packet
            $dbpaket = 'packet as c';
            $reldbpaket = 'c.id';
            $reldbdep2 = 'a.packet_id'; 

            //bank costumer
            $dbbankc = 'bank as d';
            $reldbbankc = 'd.id';
            $reldbdep3 = 'a.bank_id'; 

             //bank admin
           // $dbbanka = 'bank as e';
           // $reldbbanka = 'e.id';
            //$reldbdep4 = 'a.bank_send'; 

               //join($dbbanka, $reldbbanka, $sama, $reldbdep4)
               $user = DB::table('transaction as a')->select($select)->join($dbuser, $reldbuser, $sama, $reldbdep1)->join($dbpaket, $reldbpaket, $sama, $reldbdep2)->join($dbbankc, $reldbbankc, $sama, $reldbdep3)->where('a.id',$id)->get();
                   
               
                return  $user;  

             
    }


    
     public function bankAdmin(Request $request)
    {
        $limit = "10";
        $bank = DB::table('bank')->where('type','admin')->orderBy('id','desc')->paginate($limit);
        return  $bank;

    }


     public function kirim(Request $request)
    {
         $content ="";
         $admin   = Auth::user();
         $id = $request->get('id');
         $type = $request->get('type');
         $bank_send = $request->get('bank_send');
         $deposite = $this->depositeRepository->findWithoutFail($id);
     
       	$setting = DB::table('setting')->where('user_id',$admin->id)->get();
	$logo = $setting[0]->logo;
	$merchant = $setting[0]->name;
	$username = $setting[0]->email;

        $status['status'] = '1';
        $status['notif'] = '1';
        $status['bank_send'] = $bank_send;

        if($type == "deposite")
        {

        $user = $deposite->user_id;
        $saldo = DB::table('users')->where('id',$user)->first()->saldo;
        $total = $saldo+$deposite->price;

        $duitL = DB::table('bank')->where('type','admin')->first()->saldo;
        $totalL = $duitL+$deposite->price;
        $duitnambah = DB::table('bank')->where('type','admin')->update(['saldo' => $totalL]);


        $deposite = $this->depositeRepository->update($status, $id);

        $us = DB::table('users')->where('id', $user)->update(['saldo' => $total]);
        
        //fcm update top up deposite
         $harga = number_format($total,0,'','.');
         $penerima = DB::table('users')->where('id',$user)->get();
         $fcm_id = $penerima[0]->fcm;
         $message = "Tambah Saldo anda telah disetujui oleh QRPay.id sejumlah Rp ".$harga."";

         $msg = array
         (

            'saldo' => $total,
            'type'  => 'Tambah saldo',
            'message' => $message     
         );

        //$this->send_fcm($fcm_id,$msg);
         //echo  $secure;
        $price = number_format($deposite->price,0,'','.');
        $totale = number_format($total,0,'','.');
        $nama  = $penerima[0]->name;
        $tgl = $deposite->updated_at;
        $tanggal = $this->tgl($tgl);
        $metPem =  "Transfer";


	
	

        Mail::to($penerima[0]->email)->send(new VerDep($price,$totale,$nama,$tanggal,$metPem,$logo,$merchant,$username));
	//Mail::to('ryzal.kazama@gmail.com')->send(new VerDep($price,$totale,$nama,$tanggal,$metPem,$logo,$merchant,$username));
 
        
        DB::table('history')->where('transaction_id', $id)->update(['status' => '1']);
         $insert = DB::table('history')->insert(
                        [ 
                            [
                                'transaction_id' => $id,
                                'bank_id'     => $deposite->bank_id,
                                'bank_send'     => $deposite->bank_send,
                                'price'        =>  $deposite->price,
                                'description' => 'Tambah Saldo  Rp '.$harga.' ( Diterima )',
                                'user_id' => $deposite->user_id,
                                'type' => 'deposite',
                                'param'=> '1',
                                'status' => '1',
                                'created_at' => DB::raw('now()')
                            ],
                        ]);

       }else if($type == "withdraw"){

         //fcm update tarik saldo
         $user = $deposite->user_id;
         $penerima = DB::table('users')->where('id',$user)->get();
         $fcm_id = $penerima[0]->fcm;
         $harga = number_format($deposite->price,0,'','.');
         $message = "Tarik Tunai anda telah disetujui oleh QRPay.id sejumlah Rp ".$harga."";

         $msg = array
         (
            'saldo' => $penerima[0]->saldo,
            'type'  => 'Tarik tunai',
            'message' => $message     
         );

        $this->send_fcm($fcm_id,$msg);

        $duitL = DB::table('bank')->where('type','admin')->first()->saldo;
        $totalL = $duitL-$deposite->price;
        $duitnambah = DB::table('bank')->where('type','admin')->update(['saldo' => $totalL]);

        $bankhome  = DB::table('bank')->where(['user_id'=>$user,'type'=>'primary'])->get();

        $nama = $penerima[0]->name;
        $totale = $penerima[0]->saldo;
        $tgl = $deposite->updated_at;
        $tanggal = $this->tgl($tgl);
        $bank =  $bankhome[0]->name;
        $no_rek = $bankhome[0]->account_number;
        $atsnama = $bankhome[0]->account_name; 


	 
        
         //Mail::to($penerima[0]->email)->send(new VerWith($harga,$totale,$nama,$tanggal,$bank,$no_rek,$atsnama,$logo,$merchant,$username));
 	Mail::to('ryzal.kazama@gmail.com')->send(new VerWith($harga,$totale,$nama,$tanggal,$bank,$no_rek,$atsnama,$logo,$merchant,$username));
       
        $deposite = $this->depositeRepository->update($status, $id);
        //DB::table('history')->where('transaction_id', $id)->update(['status' => '1']);
         $insert = DB::table('history')->insert(
                        [ 
                            [
                                'transaction_id' => $id,
                                'bank_id'     => $deposite->bank_id,
                                'bank_send'     => $deposite->bank_send,
                                'price'        =>  $deposite->price,
                                'description' => 'Tarik Saldo  Rp '.$harga.' ( Diterima )',
                                'user_id' => $deposite->user_id,
                                'param'=> '0',
                                'type' => 'withdraw',
                                'status' => '1',
                                'created_at' => DB::raw('now()')
                            ],
                        ]);


       }


              $sama = '=';

             //relasi user
             $dbuser = 'users as b';
             $reldbuser = 'b.id';
             $reldbdep1 = 'a.user_id'; 

               
              $user = DB::table('transaction as a')->select('b.id as user_id','b.name','a.price','a.id as transaction_id')->join($dbuser, $reldbuser, $sama, $reldbdep1)->where('a.id',$id)->get();
                   
               return  $user;  

    }


    public function batal(Request $request)
    {
         $content ="";
         $admin   = Auth::user();
         $id = $request->get('id');
         $type = $request->get('type');  
         $deposite = $this->depositeRepository->findWithoutFail($id);
         $saldo = DB::table('users')->where('id',$deposite->user_id)->first()->saldo; 


        if($type == "deposite")
        {  

        $status['status'] = '0';
        $status['notif'] = '1';
        

        $total = $saldo-$deposite->price;

        $user = $deposite->user_id;
        $deposite = $this->depositeRepository->update($status, $id);

        $us = DB::table('users')->where('id', $user)->update(['saldo' => $total]);
        
        //gcm update top up deposite
            $penerima = DB::table('users')->where('id',$id)->get();
            $fcm_id = $penerima[0]->fcm;
       
            $message = "Verifikasi anda telah dibatalkan oleh QRPay.id";

             $msg = array
             (
                'saldo' => $penerima[0]->saldo,
                'type'  => 'verifikasi',
                'message' => $message     
             );

              $this->send_fcm($fcm_id,$msg);
              $nama = $penerima[0]->name;
              $total = $penerima[0]->saldo;
              $tanggal = $this->tgl($penerima[0]->updated_at);

              $dbbank = DB::table('bank')->where('user_id',$id)->get();
              $bank = $dbbank[0]->name;
              $no_rek = $dbbank[0]->account_number;
              $atsnama = $dbbank[0]->account_name;

              Mail::to($penerima[0]->email)->send(new VerUser($nama,$total,$tanggal,$bank,$no_rek,$atsnama));
        //DB::table('history')->where('transaction_id', $id)->update(['status' => '0']);

        $insert = DB::table('history')->insert(
        [ 
            [
                'transaction_id' => $id,
                'bank_id'     => $deposite->bank_id,
                'bank_send'     => $deposite->bank_send,
                'price'        =>  $deposite->price,
                'description' => 'Tambah Saldo  Rp '.$deposite->price.' ( Dibatalkan )',
                'user_id' => $deposite->user_id,
                'type' => 'deposite',
                'status' => '1',
                'created_at' => DB::raw('now()')
            ],
        ]);


      }else{

        $status['status'] = '0';
        $status['notif'] = '1';
       

        $total = $saldo+$deposite->price;

        $user = $deposite->user_id;
        $deposite = $this->depositeRepository->update($status, $id);

        $us = DB::table('users')->where('id', $user)->update(['saldo' => $total]);

         $insert = DB::table('history')->insert(
        [ 
            [
                'transaction_id' => $id,
                'bank_id'     => $deposite->bank_id,
                'bank_send'     => $deposite->bank_send,
                'price'        =>  $deposite->price,
                'description' => 'Penarikan Saldo  Rp '.$deposite->price.' (Dibatalkan)',
                'user_id' => $deposite->user_id,
                'type' => 'deposite',
                'status' => '1',
                'created_at' => DB::raw('now()')
            ],
        ]);

      }

            $sama = '=';

             //relasi user
             $dbuser = 'users as b';
             $reldbuser = 'b.id';
             $reldbdep1 = 'a.user_id'; 

               
              $user = DB::table('transaction as a')->select('b.id as user_id','b.name','a.price','a.id as transaction_id')->join($dbuser, $reldbuser, $sama, $reldbdep1)->where('a.id',$id)->get();
                   
               
                return  $user;  

    }


    

     public function blokiruser(Request $request)
    {
         $content ="";
         $admin   = Auth::user();
         $id = $request->get('id');
         $type = $request->get('type');  
         


          if($type == "costumer")
          {  

          $kyc = '2';
          $us = DB::table('users')->where('id', $id)->update(['kyc' => $kyc]);


          }else{

          
          }  
          
               
              $user = DB::table('users')->select('name')->where('id',$id)->get(); 
              return  $user;  

    }

      public function bataluser(Request $request)
    {
         $content ="";
         $admin   = Auth::user();
         $id = $request->get('id');
         $type = $request->get('type');  
         


          if($type == "costumer")
          {  

          $kyc = '0';
          $us = DB::table('users')->where('id', $id)->update(['kyc' => $kyc]);


          }else{

          
          }  
          
               
              $user = DB::table('users')->select('name')->where('id',$id)->get(); 
              return  $user;  

    }

      public function terimauser(Request $request)
    {
         $content ="";
         $admin   = Auth::user();
         $id = $request->get('id');
         $type = $request->get('type');  
         


          if($type == "costumer")
          {  

          $kyc = '1';
          $us = DB::table('users')->where('id', $id)->update(['kyc' => $kyc,'updated_at'=> DB::raw('now()')]);


            
            $penerima = DB::table('users')->where('id',$id)->get();
            $fcm_id = $penerima[0]->fcm;
       
            $message = "Verifikasi anda telah disetujui oleh QRPay.id";

             $msg = array
             (
                'saldo' => $penerima[0]->saldo,
                'type'  => 'verifikasi',
                'message' => $message     
             );

              $this->send_fcm($fcm_id,$msg);
              $nama = $penerima[0]->name;
              $total = $penerima[0]->saldo;
              $tanggal = $this->tgl($penerima[0]->updated_at);

              $dbbank = DB::table('bank')->where('user_id',$id)->get();
              $bank = $dbbank[0]->name;
              $no_rek = $dbbank[0]->account_number;
              $atsnama = $dbbank[0]->account_name;

              Mail::to($penerima[0]->email)->send(new VerUser($nama,$total,$tanggal,$bank,$no_rek,$atsnama));
 

          }else if($type == 'issuer'){
              $status = '1';
              $us = DB::table('users')->where('id', $id)->update(['status' => $status,'updated_at'=> DB::raw('now()')]);
          }
          
               
              $user = DB::table('users')->select('name')->where('id',$id)->get(); 
              return  $user;  

    }

    public function updatenotivdep(Request $request)
    {

        DB::table('transaction')->where('type','deposite')->update(['notif' => '1']);

    }



    public function updatenotivdraw(Request $request)
    {

        DB::table('transaction')->where('type','withdraw')->update(['notif' => '1']);

    }

    public function updatenotivuser(Request $request)
    {

        DB::table('users')->where('type','costumer')->update(['notif' => '1']);

    }



      public function delete(request $request)
    {
       $id = $request->get('id'); 
        DB::table('Transaction')->delete($id);
        
   
    }



     public function send_fcm($fcm_id,$message)
    {

    $api_access_key = "AAAAhg-KJAI:APA91bGbK1hEG8RUMS_7yEz8MMo3NC4LMUkbABsQ4QlZy2858MEOEMMs179l5oPM3u3dpmL0BKVq5ilqs5i0l8ELGBZxoGpNGtdN0bFgVNv4YDmoGCbreEcq7SCxp1oRKDPjN15qA8qN";
       
    $registrationIds = $fcm_id;
    $fields = array
            (
                'to'        => $registrationIds,
                'priority' => 'high',
                'data'  => $message
            );

    $headers = array (
        'Authorization: key=' . $api_access_key,
        'Content-Type: application/json'
    );


#Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
#Echo Result Of FireBase Server
// echo $result;

    }
   

    public function tgl($tgl, $hari_tampil = true){

        $bulan  = array("Januari"
                        , "Februari"
                        , "Maret"
                        , "April"
                        , "Mei"
                        , "Juni"
                        , "Juli"
                        , "Agustus"
                        , "September"
                        , "Oktober"
                        , "November"
                        , "Desember");
        $hari   = array("Senin"
                        , "Selasa"
                        , "Rabu"
                        , "Kamis"
                        , "Jum'at"
                        , "Sabtu"
                        , "Minggu");
        $tahun_split    = substr($tgl, 0, 4);
        $bulan_split    = substr($tgl, 5, 2);
        $hari_split     = substr($tgl, 8, 2);
        $tmpstamp       = mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
        $bulan_jadi     = $bulan[date("n", $tmpstamp)-1];
        $hari_jadi      = $hari[date("N", $tmpstamp)-1];
        if(!$hari_tampil)
        $hari_jadi="";
        return $hari_jadi.", ".$hari_split." ".$bulan_jadi." ".$tahun_split;

    }

    /*
    *
    * ISSUER
    *
    */
    public function getissuer(Request $request)
    {
        $issuer = DB::table('users as a')->select('a.*', 'b.saldo as bank_saldo')->join('setting as b', 'a.id', '=', 'b.user_id')->where('type', 'issuer')->paginate(10);
        return $issuer;
    }

    public function postissuer(Request $req)
    {
      if (!$req->hasFile('co_photo')) 
      {
        $photo = Config::get('app.photo_default');
        $co_potho = $this->url->to('/'.$photo);      
      }else if ($req->hasFile('co_photo')) {
        $photox = base64_encode($req->file('co_photo'));
        $fileFormat = ".jpg";
        $userPath = base64_encode($req->input('co_email'));
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$userPath;
        $fileName = base64_encode($req->input('co_email') . time());
        $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        File::makeDirectory($files, 0777, true, true);

        if ($image = base64_decode($photox, true))
        {
            $img = Image::make($image)->save($fullPath, 60);
            $co_potho = $this->url->to('/'.$fullPath);
        } else {
            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
        }  
      }
      $kosong = '';
      $client = [
                'role_id' => 4,
                'name' => $req->input('co_name'),
                'email' => $req->input('co_email'),
                'api_key' => $kosong,
                'api_secret' => $kosong,
                'type' => 'issuer',
                'ip_address' => $kosong,
                'status' => '0',
                'created_at' => date("Y-m-d H:i:s")
          ];
      $clien_id = DB::table('client')->insertGetId($client);

      /* Generate pin user */
      $secure = sprintf("%06d", mt_rand(1, 999999));
      $security = $secure;
      $co_pin = $security; 

      /* cek kelengkapan data */ 
      if($req->input('co_name') != '' && $req->input('co_email') != '' && $req->input('co_phone') != '' && $req->input('co_city') != '' && $req->input('co_postal') != '' && $req->input('co_address') != '' && $req->hasFile('co_photo'))
      {
        $kyc = 1;
      }
      else
      {
        $kyc = 0;
      }

      $user = [
                'param' => Auth::user()->id,
                'client_id' => $clien_id,
                'name' => $req->input('co_name'),
                'saldo' => 0,
                'email' => $req->input('co_email'),
                'pin' => $co_pin,
                'qrcode' => $kosong,
                'img_qrcode' => $kosong,
                'img_sb' => $kosong,
                'img_location' => $kosong,
                'phone' => $req->input('co_phone'),
                'pob' => $kosong,
                'dob' => date("Y-m-d H:i:s"),
                'gender' => $kosong,
                'national' => $kosong,
                'city' => $req->input('co_city'),
                'education' => $kosong,
                'job' => $kosong,
                'password' => bcrypt('123456'),
                'postal_code' => $req->input('co_postal'),
                'photo' => $co_potho,
                'type_id' => $kosong,
                'identity_number' => $kosong,
                'identity_photo' => $kosong,
                'address' => $req->input('co_address'),
                'type' => 'issuer',
                'status' => '0',
                'notif' => 0,
                'kyc' => $kyc,
                'fcm' => $kosong,
                'created_at' => date("Y-m-d H:i:s")
          ];
      $user_id = DB::table('users')->insertGetId($user);

      /* Generate qr */
      $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
                
      $fileFormatqr = ".png";
      $qrPath = base64_encode('qrcode');
      $dir = Config::get('elfinder.dir')[0];
      $files = $dir."/".$qrPath;

      $fileQr = base64_encode($kode.$user_id . time());
      $fullqr = $dir."/".$qrPath."/".$fileQr.$fileFormatqr;
      File::makeDirectory($files, 0777, true, true);


      $png = QrCode::format('png')->size(500)
          ->merge('../public/img/iconqr.png', .1, true)
              ->generate(''.$kode.$user_id.'', '../public/'.$fullqr);
      /* Update data qr user */
      DB::table('users')->where('id',$user_id)->update(['qrcode'=> $kode.$user_id,'img_qrcode'=> $this->url->to($fullqr) ]);

      $role_user = [
                'role_id' => 5,
                'user_id' => $user_id
          ];
      $role_user_id = DB::table('role_user')->insertGetId($role_user);

      $setting_user = [
                'user_id' => $user_id,
                'mindepn' => 0,
                'maxdepn' => 0,
                'mindepk' => 0,
                'maxdepk' => 0,
                'mintrn' => 0,
                'maxtrn' => 0,
                'mintrk' => 0,
                'maxtrk' => 0,
                'minwdn' => 0,
                'maxwdn' => 0,
                'minwdk' => 0,
                'maxwdk' => 0,
                'admin' => 0,
                'saldo' => 0,
                'bank' => '',
                'account_number' => '',
                'account_name' => '',
                'count' => 0,
                'status' => 0,
                'created_at' => date("Y-m-d H:i:s"),
          ];
      $setting_id = DB::table('setting')->insertGetId($setting_user);

      return redirect('/user?type=issuer');
    }

    public function posthapusissuer(Request $req)
    {
      DB::table('users')->where('id',$req->input('id'))->update(['status'=> '0']);
    }

    public function postEditIssuer(Request $req)
    {
      $temp_user = DB::table('users')->where('id', '=', $req->input('_id'))->get();
      $user = array();
      if ($req->hasFile('co_photo')) 
      {
        $photox = base64_encode($req->file('co_photo'));
        $fileFormat = ".jpg";
        $userPath = base64_encode($req->input('co_email'));
        $dir = Config::get('elfinder.dir')[0];
        $files = $dir."/".$userPath;
        $fileName = base64_encode($req->input('co_email') . time());
        $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        File::makeDirectory($files, 0777, true, true);

        if ($image = base64_decode($photox, true))
        {
          $img = Image::make($image)->save($fullPath, 60);
          $co_potho = $this->url->to('/'.$fullPath);
          $user['photo'] = $co_potho;
          /* Cek apakah fotonya default kalau tidak di unlink */
          if(strpos($temp_user[0]->photo, 'img/avatar.png') === false)
          {
            $temp_foto = explode("/", $temp_user[0]->photo);
            $location = 'files/'.$temp_foto[count($temp_foto) - 2].'/'.$temp_foto[count($temp_foto) - 1];
            unlink(public_path($location));
          }
        } else {
          return Response::json(ResponseUtil::makeError('Image not valid'), 404);
        } 
      }

      /* cek kelengkapan data */ 
      if($req->input('co_name') != '' && $req->input('co_email') != '' && $req->input('co_phone') != '' && $req->input('co_city') != '' && $req->input('co_postal') != '' && $req->input('co_address') != '' && (strpos($temp_user[0]->photo, 'img/avatar.png') === false || $req->hasFile('co_photo')))
      {
        $kyc = 1;
      }
      else
      {
        $kyc = 0;
      }

      $user['name'] = $req->input('co_name');
      $user['email'] = $req->input('co_email');
      $user['phone'] = $req->input('co_phone');
      $user['city'] = $req->input('co_city');
      $user['postal_code'] = $req->input('co_postal');
      $user['address'] = $req->input('co_address');
      $user['updated_at'] = date("Y-m-d H:i:s");
      $user['kyc'] = $kyc;
      DB::table('users')->where('id', '=', $req->input('_id'))->update($user);

      $client = [
          'name' => $req->input('co_name'),
          'email' => $req->input('co_email'),
          'updated_at' => date("Y-m-d H:i:s")
      ];
      DB::table('client')->where('id', '=', $temp_user[0]->client_id)->update($client);

      return redirect('/user?type=issuer');
    }

    /* 
     * Deposit Issuer
     */

    public function DepositeIssuer()
    {
      return view('deposite.index')
             ->with('type', 'Deposit');
    }

    public function listtableDeposit(Request $request)
    {
      $limit = '10';
      $status = $request->get('status');
      $search = $request->get('search');
      $field = $relasi = $isian = '';
      $isi = DB::table('deposit as a')->select('b.name', 'a.*')->join('users as b', 'b.id', '=', 'a.user_id');
      if($status == 'all')
      {
        $field = 'a.user_id'; $relasi = '<>'; $isian = '0';
        $data = $isi->where($field, $relasi, $isian);
      }
      else if($status == 'not')
      {
        $field = 'a.transfer_photo'; $relasi = 'IS NULL'; $isian = '';
        $data = $isi->whereNull($field);
      }
      else
      {
        $field = 'a.transfer_photo'; $relasi = '<>'; $isian = '';
        $data = $isi->where($field, $relasi, $isian);
      }

      if($search != '')
      {
        $data->where('b.name', 'like', $search.'%');
      }
      return $data->paginate($limit);
    }

    public function getdetailDeposit(Request $request)
    {
      $id = $request->get('id');

      $sql = DB::table('deposit as a')->select('b.name', 'a.*')->join('users as b', 'b.id', '=', 'a.user_id')->where('a.id', $id)->get();
      return $sql;
    }

    public function konfirmasi_deposit(Request $request)
    {
      $id = $request->input('id');
      $saldo = $request->input('saldo');
      $user_id = $request->input('user_id');

      $setting = DB::table('setting')->where('user_id', $user_id)->get();
      $sum_saldo = $saldo + $setting[0]->saldo;

      DB::table('deposit')->where('id', $id)->update(['status' => 1]);
      DB::table('setting')->where('user_id', $user_id)->update(['saldo' => $sum_saldo]);
    }

    public function historyTransaction()
    {
      return view('deposite.indexhistory')
             ->with('type', 'History');
    }

    public function listhistoryTransaction(Request $request)
    {
      $transaction = DB::table('transaction as a')
                       ->distinct()
                       ->select('a.id', 'a.type', 'a.tanggal', 'a.price',
                                  DB::raw('(SELECT users.name FROM users WHERE users.id = a.user_id) as pengirim'),
                                  DB::raw('(SELECT users.name FROM users WHERE users.id = a.costumer_id) as tujuan') 
                               )
                       ->join('history as b', 'a.id', '=', 'b.transaction_id')
                       ->join('users as c', 'b.user_id', '=', 'c.id')
                       ->whereIn('a.type', ['tr-generate', 'transaction'])
                       ->paginate(10);
      return $transaction;
    }

    public function detailhistoryTransaction(Request $request)
    {
      $id = $request->get('id');
      $transaction = DB::table('transaction as a')
                       ->select('a.id', 'a.type', 'a.tanggal', 'a.price', 
                                  DB::raw('(SELECT users.name FROM users WHERE users.id = a.user_id) as pengirim'),
                                  DB::raw('(SELECT users.name FROM users WHERE users.id = a.costumer_id) as tujuan')
                               )
                       ->where('a.id', $id)
                       ->get();
      return $transaction;
    }

	  public function listtabelorder(Request $request)
    {
       
                    
                    $id = $request->get('id'); 
                    $pages = $request->get('page');
                    $search = $request->get('search');



                     $limit = "10";
                    

                    if($search !="")
		    {

                    $transaction = DB::table('transaction as a')
	  ->select('a.id as kode','b.name as costumer','b.id','b.param','a.type','a.price','c.type as type_merchant','c.name as merchant','d.transaksi','d.transfer','d.admin','e.name as issuer','f.name as pengikut')
		->join('users as b', 'a.costumer_id', '=', 'b.id')
			->join('users as c', 'a.user_id', '=', 'c.id')
			   ->join('setting as d', 'd.user_id', '=', 'b.param')
		   		->join('users as e', 'e.id', '=', 'c.param')
				  ->join('users as f', 'f.id', '=', 'b.param')
				->where(['b.param'=>$user->id,'a.costumer_id'=>$id])->orderBy('id','desc')->paginate(10);  
		
			}else{

			   $transaction = DB::table('transaction as a')
	  ->select('a.id as kode','b.name as costumer','b.id','b.param','a.type','a.price','c.type as type_merchant','c.name as merchant','d.transaksi','d.transfer','d.admin','e.name as issuer','f.name as pengikut')
		->join('users as b', 'a.costumer_id', '=', 'b.id')
			->join('users as c', 'a.user_id', '=', 'c.id')
			   ->join('setting as d', 'd.user_id', '=', 'b.param')
		   		->join('users as e', 'e.id', '=', 'c.param')
				  ->join('users as f', 'f.id', '=', 'b.param')
				->where(['b.param'=>$user->id])->orderBy('id','desc')->paginate(10);  



			}

		return $transaction;                 
        
        
    }



   
}
