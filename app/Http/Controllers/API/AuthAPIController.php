<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use File;
use Auth;
use Config;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Forgot;
use App\ShaHasher;
use Illuminate\Hashing\HashServiceProvider;

class AuthAPIController extends Controller
{
    use ResetsPasswords;

    public function authenticate(Request $request)
    {
        // grab credentials from the request
	 
        $credentials = $request->only('email', 'password','type','client_id');
        $credentials['status'] = 1;

$emailID = DB::table('users')->where(['email'=>$request->email])->get();
if(!empty($emailID[0]->id))
{
	
	

        try {
            // attempt to verify the credentials and create a token for the user
            $param = (!empty($request->param)) ? $request->param : 1 ;
	   
              
             $login = array($credentials);
	     
            





                  $user = DB::table('users')->where(['email'=>$request->email,'param'=>$param])->get();
                  if(empty($user[0]->id))
                  {

		      return response()->json(['success' => false, 'message' => 'Anda belum terdaftar'], 404);
			                   
                  
                  }else if($user[0]->status =="0"){

                    return response()->json(['success' => false, 'message' => 'Akun anda belum aktif, silahkan cek email anda untuk aktivasi akun'], 404);

                  }else if($user[0]->status =="2"){       

                     return response()->json(['success' => false, 'message' => 'Akun anda telah di non aktifkan, kami menemukan aktitas mencurigakan. Cek email untuk mengaktifkan akun anda!'], 404);

                  }else if (! $token = JWTAuth::attempt($credentials)) {

                  return response()->json(['success' => false, 'message' => 'password anda salah'], 404);
                     
                    }




        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'message' => 'could_not_create_token'], 500);
        }

        // $userDir  =   base64_encode(Auth::user()->email);
        // if (!File::exists('files')) {
        //     File::makeDirectory('files',0775);
        // }
        // if (!File::exists(public_path('files').'/'.$userDir)) {
        //     File::makeDirectory(public_path('files').'/'.$userDir,0775);
        // }
        // Config::set('elfinder.dir',["files/$userDir"]);


        $user = DB::table('users')->where(['email'=>$request->email,'param'=>$param,'client_id'=>$request->client_id])->get();
        if($user[0]->status =="1")
        { 
            if (!Auth::validate(array('email' => Auth::user()->email, 'password' => $request->password,'param'=>$param,'client_id'=>$request->client_id)))
            {
            	
                
            }else{

                     // all good so return the token
                return response()->json(['success' => true, 'data' => compact('token'), 'message' => 'Token success generated']);
            }


        }


}







    }


    public function forgot(Request $request)
    {
        //$this->validateSendResetLinkEmail($request);

        // $broker = $this->getBroker();

        // $response = Password::broker($broker)->sendResetLink(
        //     $this->getSendResetLinkEmailCredentials($request),
        //     $this->resetEmailBuilder()
        // );

        $email = $request->email;
        $rparam = $request->param;

        $user = DB::table('users')->where(['email'=>$email,'param'=>$rparam])->get();

        if(empty($user[0]->id))
        {


            return response()->json(['success' => false, 'message' => 'Email belum terdaftar'], 401);

        }else{

                    $st =   "1";
                    $param = "forgot";
                    $nameID  = $user[0]->name;
                    $kodeID = $user[0]->id."&email=".$user[0]->email."&st=".$st."&key=".$param;
                    $userID = base64_encode($kodeID);
                        
                    $urlID =  $userID;  

			$setting = DB::table('setting')->where('user_id',$user[0]->param)->get();
			$merchant = $setting[0]->name;
			$logo = $setting[0]->logo;
			$username = $setting[0]->email;
			
			$domain = "verifikasi.qrpay.id";

                    Mail::to($user[0]->email)->send(new Forgot($nameID,$user[0]->email,$urlID,$merchant,$logo,$username,$domain));
                
                    return response()->json(['success' => true, 'message' => 'Verifikasi ubah password telah dikirim ke email anda']);

                
        }   

        

        // switch ($response) {
        //     case Password::RESET_LINK_SENT:
        //         return response()->json(['success' => true, 'message' => 'Rest password link send']);
        //     case Password::INVALID_USER:
        //     default:
        //         return response()->json(['success' => false, 'message' => 'Email not valid'], 401);
        // }


     }


     public function cekparam(Request $request)
    {
        

        $email = $request->email;
        $password = $request->password;

        $user = DB::table('users')->where(['email'=>$email])->get();
        if(empty($user[0]->id))
        {

             return response()->json(['false' => false, 'message' => 'Anda belum terdaftar sebagai issuer'], 404);

        }else{

            if (!Auth::validate(array('email' => $email, 'password' => $request->password)))
            {
            
                return response()->json(['false' => false, 'message' => 'Password Anda Salah'], 404);
                
            }else{

                  

               $select['id'] = 'id';
               $select['param'] = 'param'; 
               $select['name'] = 'name';
               $select['photo'] = 'photo';
               $select['address'] = 'address';
               $select['phone'] = 'phone';
               $select['type'] = 'type'; 


               $users = DB::table('users')->select($select)->where(['email'=>$email])->first();

                  return response()->json(['success' => true, 'data'=> $users,  'message' => 'user view successfully'], 200);




            }


        }    

       

           //return $this->sendResponse($user->toArray(), 'User retrieved successfully');

    } 

       
}