<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Requests\API\UpdatePasswordUserAPIRequest;
use App\Http\Requests\API\UpdateSecurityUserAPIRequest;
use App\Http\Requests\API\UpdatePinUserAPIRequest;
use App\User;
use App\Role;
use App\RoleUser;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;
use Config;
use App\Models\Client;
use DB;
use QrCode;

use File;
use Storage;
use App\Criteria\MyCriteria;
use Illuminate\Support\Facades\Mail;
use App\Mail\Register;
use App\Mail\WelcomeUser;
use App\Criteria\MerchantCriteria;
use App\Criteria\IssuerCriteria;
/**
 * Class userController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  userRepository */
    use AuthenticatesAndRegistersUsers;
    private $userRepository;
    protected $url;

    public function __construct(UserRepository $userRepo,UrlGenerator $url)
    {
        $this->userRepository = $userRepo;
        $this->url = $url;
    }
     
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
        $user = $this->userRepository->all();

        return $this->sendResponse($user->toArray(), 'user retrieved successfully');
    }

    public function store(CreateUserAPIRequest $request)
    {

        $input = $request->all();

        $user = $this->create($input);

        return $this->sendResponse($user->toArray(), 'user saved successfully');
    }

    protected function create(array $data)
    {

        $data['password'] =  Hash::make($data['password']);
        $user = User::create($data);

       

        return $user;
    }

    public function show($id)
    {
        /** @var user $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($user->toArray(), 'user retrieved successfully');
    }

    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var user $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'user updated successfully');
    }

    public function destroy($id)
    {
        /** @var user $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        $user->delete();

        return $this->sendResponse($id, 'user deleted successfully');
    }


    public function sendemail()
    {

                $data = ['foo' => 'bar'];
                    Mail::send('email.welcome', $data, function($message)
                    {
                        $message->from('ryzal.jochi@gmail.com', 'Laravel');

                        $message->to('sprakoso@gmail.com');

                       
                    });    
    }


    public function listmerchant(Request $request)
    {
	$id = Auth::user()->param;	
           $limit ="20";  
           $select['id'] = 'id';
           $select['name'] = 'name';
            $select['photo'] = 'photo';
           $select['address'] = 'address';
           $select['phone'] = 'phone';
           $select['type'] = 'type'; 
           $user = DB::table('users')->select($select)->where(['type'=>'merchant','param'=>$id])->orderBy('id','desc')->paginate($limit);

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');


    }


     public function listissuer(Request $request)
    {

      	$id = Auth::user()->param;
           $limit ="20";  
           $select['id'] = 'id';
           $select['name'] = 'name';
            $select['photo'] = 'photo';
           $select['address'] = 'address';
           $select['phone'] = 'phone';
           $select['type'] = 'type'; 


           $user = DB::table('users')->select($select)->where(['type'=>'issuer','param'=>$id])->orderBy('id','desc')->paginate($limit);

           return $this->sendResponse($user->toArray(), 'User retrieved successfully');


    }

	public function listcobranding(Request $request)
    {
	   $id = Auth::user()->param;	
      
           $limit ="20";  
           $select['id'] = 'id';
           $select['name'] = 'name';
           $select['photo'] = 'photo';
           $select['address'] = 'address';
           $select['phone'] = 'phone';
           $select['type'] = 'type'; 

		
           $user = DB::table('users')->select($select)->where(['type'=>'cobranding','param'=>$id])->orderBy('id','desc')->paginate($limit);

           return $this->sendResponse($user->toArray(), 'User retrieved successfully');


    }


    

    
    public function register(CreateUserAPIRequest $request)
    {

        $input = $request->all();
        
        if($request->type == 'customer')
        {
            $input['type'] = 'costumer';
        }    

        $param = (!empty($request->param)) ? $request->param : 1;
	$client_id = (!empty($request->client_id)) ? $request->client_id : 1;

        $input['param'] = $param ;     
        $input['status'] = '0';
        $input['phone'] = $request->phone;
        $input['kyc'] = '0';

	

        //cek email
        $ID = DB::table('users')->where(['email'=>$request->email,'param'=>$request->param])->get();
        if(empty($ID))
        {



                if (!$request->has('photo') || $input['photo']=='') 
                {
                    $photo = Config::get('app.photo_default');
                    $input['photo'] = $this->url->to('/'.$photo);      

                }else if ($request->has('photo')) {
                    $fileFormat = ".jpg";
                    $userPath = base64_encode($request->email);
                    $dir = Config::get('elfinder.dir')[0];
                    $files = $dir."/".$userPath;
                    $fileName = base64_encode($request->email . time());
                    $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
                    File::makeDirectory($files, 0777, true, true);

                        if ($image = base64_decode($input['photo'], true))
                        {
                            $img = Image::make($image)->save($fullPath, 60);
                            $input['photo'] = $this->url->to('/'.$fullPath);
                        } else {
                            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                        }
                     
                }



    
      
       
        $input['client_id'] = $client_id;

        $secure = sprintf("%06d", mt_rand(1, 999999));
        $data = "LY-";
        $security = $secure;
        $input['pin'] = $security; 

                $user = $this->create($input);
                $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
                
                $fileFormatqr = ".png";
                $qrPath = base64_encode('qrcode');
                $dir = Config::get('elfinder.dir')[0];
                $files = $dir."/".$qrPath;

                $fileQr = base64_encode($kode.$user->id . time());
                $fullqr = $dir."/".$qrPath."/".$fileQr.$fileFormatqr;
                File::makeDirectory($files, 0777, true, true);


                $png = QrCode::format('png')->size(500)
                    ->merge('../public/img/iconqr.png', .1, true)
                        ->generate(''.$kode.$user->id.'', '../public/'.$fullqr);
                
                DB::table('users')->where('id',$user->id)->update(['qrcode'=> $kode.$user->id,'img_qrcode'=> $this->url->to($fullqr) ]);

                if (!$request->has('type'))
                {    
                    $role = Role::where('name', 'costumer')->first();
                   
                }else{
                    $role = Role::where('name', $input['type'])->first();
                }
            
                if (!empty($role)) 
                {

                    $roleUser = new RoleUser;
                    $roleUser->user_id = $user->id;
                    $roleUser->role_id = $role->id;
                    $roleUser->save();

                }

                    $set = DB::table('setting')->where(['user_id'=>$request->param])->get();

                    $merchant = $set[0]->name;
                    $username = $set[0]->email;
                    $password = $set[0]->password;
                    $host      = $set[0]->host;
                    $port      = $set[0]->port;
                    $encryption = $set[0]->encryption;



                    $logoserver = $set[0]->logo;
                    $sname = $set[0]->name;

                    $emailID = $request->email;
                    $nameID  = $request->name;
                    $st =   "1";
                    $param = $request->param;
                    $kodeID = $user->id."&email=".$emailID."&st=".$st."&pr=".$param;
                    $userID = base64_encode($kodeID);
           
                    $urlID =  $userID; 

                      

                     $key = "verifikasi.qrpay.id";


                    Mail::to($emailID)->send(new Register($nameID,$emailID,$urlID,$logoserver,$merchant,$username,$password,$port,$host,$encryption,$key));

                    


                
                    return $this->sendResponse($user->toArray(), 'user saved successfully');

        }else if($ID[0]->email == $request->email){

            return Response::json(ResponseUtil::makeError('Email anda sudah terdaftar!!'), 404);

        }   
    
        
    }

     public function myMerchant(Request $request)
    {
        $qrcode = $request->qrcode;
       

        $merchant = DB::table('users')->where('qrcode',$qrcode)->first();

        if($merchant !=null)
        {

             $user = DB::table('users')->select('id','name', 'qrcode','status as nominal')->where('qrcode', $qrcode)->first();
        }else{

             $user = DB::table('generate as a')->select('a.id','b.name','a.code as qrcode','a.nominal')->where(['a.code'=>$qrcode])->leftJoin('users as b', 'a.user_id', '=', 'b.id')->first();
        }    





        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('Merchant not found'), 404);
        }

        //  $user = DB::table('generate as a')->select('a.id','b.name','a.code', 'a.img_qrcode','a.nominal')->where(['a.code'=>$qrcode])->leftJoin('users as b', 'a.user_id', '=', 'b.id')->first();


        return $this->sendResponse($user, 'Merchant retrieved successfully');
    }



     public function myQrcode(Request $request)
    {
        $id = Auth::user()->id;
        $user = DB::table('users')->select('qrcode','img_qrcode')->where('id', $id)->first();

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('QRcode not found'), 404);
        }

        return $this->sendResponse($user, 'QRcode retrieved successfully');
    }

    public function myProfile()
    {
        $id = Auth::user()->id;
        $rek = DB::table('bank')->where(['user_id'=>$id])->get();
        
        if(!empty($rek))
        {    
            foreach($rek as $row)
             {

                if($row->type =='primary')
                {
                     $users = DB::table('users as a')->select('a.id','a.name','a.phone','a.img_sb','a.saldo','a.email','a.gender','a.pob','a.dob','a.national','a.city','a.postal_code','a.education','a.job','a.type_id','qrcode','img_qrcode','a.identity_number','a.address','a.identity_photo','a.photo','a.kyc','b.name as bank','b.account_name','b.account_number','b.type as type_bank')->where(['a.id'=>$id,'b.type'=>'primary'])->leftJoin('bank as b', 'a.id', '=', 'b.user_id')->first(); 
                 }else{
                     $users = DB::table('users as a')->select('a.id','a.name','a.phone','a.img_sb','a.saldo','a.email','a.gender','a.pob','a.dob','a.national','a.city','a.postal_code','a.education','a.job','a.type_id','qrcode','img_qrcode','a.identity_number','a.address','a.identity_photo','a.photo','a.kyc')->where(['a.id'=>$id])->first(); 
                 }   
             
            }
        }else{

                $users = DB::table('users as a')->select('a.id','a.name','a.phone','a.img_sb','a.saldo','a.email','a.gender','a.pob','a.dob','a.national','a.city','a.postal_code','a.education','a.job','a.type_id','qrcode','img_qrcode','a.identity_number','a.address','a.identity_photo','a.photo','a.kyc')->where(['a.id'=>$id])->first(); 
        }

        if (empty($users)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($users, 'user retrieved successfully');
    }


    


    public function cekSaldo()
    {
        $id = Auth::user()->id;
    
        $users = DB::table('users')->select('id','name', 'saldo','status')->where('id',$id)->first();

        if (empty($users)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($users, 'user retrieved successfully');
    }




   
   

     public function updateProfile(UpdateUserAPIRequest $request)
    {

	 $input = $request->except('email', 'password', 'status', 'create_at', 'update_at');

        /** @var user $user */
        $id = Auth::user()->id;
        $user = $this->userRepository->find($id);
       	$kyc = Auth::user()->kyc;
       

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

       		if (!$request->has('photo') || $input['photo']=='') 
                {
                    $photoz = Config::get('app.photo_default');
                    $input['photo'] = $this->url->to('/'.$photoz);      

                }else if ($request->has('photo')) {
                    $fileFormat = ".jpg";
                    $userPath = base64_encode(Auth::user()->email);
           	    $fileName = base64_encode(Auth::user()->email . time());	

                    $dir = Config::get('elfinder.dir')[0];
                    $files = $dir."/".$userPath;
                    $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
                    File::makeDirectory($files, 0777, true, true);

                        if ($image = base64_decode($input['photo'], true))
                        {
                            $img = Image::make($image)->save($fullPath, 60);
                            $input['photo'] = $this->url->to('/'.$fullPath);
                        } else {
                            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                        }
                     
                } 



		if (!$request->has('identity_photo') || $input['identity_photo']=='') 
                {
                    $photox = Config::get('app.photo_default');
                    $input['identity_photo'] = $this->url->to('/'.$photox);      

                }else if ($request->has('identity_photo')) {
                    $fileFormat1 = ".jpg";
                    $userPath1 = base64_encode(Auth::user()->email);
           	    $fileName1 = base64_encode(Auth::user()->email . time());	

                    $dir1 = Config::get('elfinder.dir')[0];
                    $files1 = $dir1."/".$userPath1;
                    $fullPath1 = $dir1."/".$userPath1."/".$fileName1.$fileFormat1;
                    File::makeDirectory($files1, 0777, true, true);

                        if ($image1 = base64_decode($input['identity_photo'], true))
                        {
                            $img1 = Image::make($image1)->save($fullPath1, 60);
                            $input['identity_photo'] = $this->url->to('/'.$fullPath1);
                        } else {
                            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                        }
                     
                } 


 		if (!$request->has('img_sb') || $input['img_sb']=='') 
                {
                    $photoc = Config::get('app.photo_default');
                    $input['img_sb'] = $this->url->to('/'.$photoc);      

                }else if ($request->has('img_sb')) {
                    $fileFormatc = ".jpg";
                    $userPathc = base64_encode(Auth::user()->email);
           	    $fileNamec = base64_encode(Auth::user()->email . time());	

                    $dirc = Config::get('elfinder.dir')[0];
                    $filesc = $dirc."/".$userPathc;
                    $fullPathc = $dirc."/".$userPathc."/".$fileNamec.$fileFormatc;
                    File::makeDirectory($filesc, 0777, true, true);

                        if ($imagec = base64_decode($input['img_sb'], true))
                        {
                            $imgc = Image::make($imagec)->save($fullPathc, 60);
                            $input['img_sb'] = $this->url->to('/'.$fullPathc);
                        } else {
                            return Response::json(ResponseUtil::makeError('Image not valid'), 404);
                        }
                     
                } 



	if($kyc =="0")
	{
		  $input['kyc'] = '1';
	}else{
		  $input['kyc'] = '1';
	}
      
         $input['status'] = '1';


        if($user->qrcode =="")
        {
                $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
                
                $fileFormatqr = ".png";
                $qrPath = base64_encode('qrcode');
                $dir = Config::get('elfinder.dir')[0];
                $files = $dir."/".$qrPath;

                $fileQr = base64_encode($kode.$user->id . time());
                $fullqr = $dir."/".$qrPath."/".$fileQr.$fileFormatqr;
                File::makeDirectory($files, 0777, true, true);


                $png = QrCode::format('png')->size(500)
                    ->merge('../public/img/iconqr.png', .1, true)
                        ->generate(''.$kode.$user->id.'', '../public/'.$fullqr);
                
                DB::table('users')->where('id',$user->id)->update(['qrcode'=> $kode.$user->id,'img_qrcode'=> $this->url->to($fullqr) ]);


        }    
            if($request->identity_photo =="")
            {    
                $identity_photo = DB::table('users')->where('id',$id)->first()->identity_photo;
                $input['identity_photo'] = $identity_photo;     

            }

            if($request->img_sb ==""){

                 $img_sb = DB::table('users')->where('id',$id)->first()->img_sb;
                 $input['img_sb'] = $img_sb; 
            } 

            if($request->img_location ==""){

                 $img_location = DB::table('users')->where('id',$id)->first()->img_location;
                 $input['img_location'] = $img_location; 
            }   

            $user = $this->userRepository->update($input, $id);
        
        // $param = $request->id;
        // $namabank = DB::table('bank')->where(['id'=>$param])->first()->name;

        // $bank           = $namabank;
        $bank   = $request->bank;
        $account_name   = $request->account_name;
        $account_number  = $request->account_number;
        $primary = "primary";

        // $secure = sprintf("%06d", mt_rand(1, 999999));
        // $trask = $user->id.$secure;
        // $insert = DB::table('history')->insert(
        // [ 
        //     [
        //         'transaction_id' => $trask,
        //         'bank_id'     => '0',
        //         'tanggal'     => '0000-00-00',
        //         'bank_send'     => '0',
        //         'price'        =>   '0',
        //         'description' => 'Profile anda sudah terupdate ( Proses )',
        //         'user_id' => $user->id,
        //         'type' => 'profile',
        //          'param'=>'1',
        //          'admin' => '0',
        //         'status' => '0',
        //         'created_at' => DB::raw('now()')
        //     ],
        // ]);

        //Mail::to($user->email)->send(new Blokir($user->name,$user->updated_at)); 

        //cek type
        $typeRek = DB::table('bank')->where(['type'=>$primary,'user_id'=>$id])->get();
        if($typeRek==null)
        {

            DB::table('bank')->insert(['user_id' => $id, 'name' => $bank,'account_name'=>$account_name,'account_number'=>$account_number,'type'=>$primary,'status'=>'1','created_at'=> new \DateTime() ]);

        }

        $users = DB::table('users as a')->select('a.id','a.name','a.phone','a.saldo','a.email','a.gender','a.pob','a.dob','a.national','a.city','a.postal_code','a.education','a.job','a.type_id','a.identity_number','a.address','a.img_sb','a.identity_photo','a.photo','a.kyc','b.name as bank','b.account_name','b.account_number','b.type as type_bank')->where('a.id',$id)->leftJoin('bank as b', 'a.id', '=', 'b.user_id')->first(); 
        return $this->sendResponse($users, 'user updated successfully');

    }


    public function updatePassword(UpdatePasswordUserAPIRequest $request)
    {
        $input = $request->only('password', 'password_new');

        $id = Auth::user()->id;
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        if (!Auth::validate(array('email' => Auth::user()->email, 'password' => $input['password'], 'client_id' => $user->client_id)))
        {
            return Response::json(ResponseUtil::makeError('old password is incorrect'), 403);
        }

        $input['password'] = bcrypt($input['password_new']);

        $user = $this->userRepository->update($input, $id);
     
        return Response::json(ResponseUtil::makeError('password updated successfully'), 201);
        
    }

     public function myBank()
    {
        $id = Auth::user()->id;
        $bank = DB::table('bank')->select('id','name','account_name','account_number','type')->where('user_id',$id)->get();

        if (empty($bank)) {;
            return Response::json(ResponseUtil::makeError('Bank not found'), 404);
        }

        return $this->sendResponse($bank, 'bank retrieved successfully');
    }



     public function updateRekening(UpdateUserAPIRequest $request)
    {
       $id = Auth::user()->id;
       $no_rekening = $request->no_rekening;
       $atas_nama = $request->atas_nama;
       $bank = $request->bank;

        $us = User::where('id', $id)->update(['no_rekening' => $no_rekening,'atas_nama'=> $atas_nama,'bank'=> $bank]);
        $users = DB::table('users')->select('id','name','no_rekening','atas_nama','bank')->where('id',$id)->first();
        if (empty($users)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($users, 'user update successfully');
    }



    public function updatePin(UpdatePinUserAPIRequest $request)
    {
        $input = $request->only('pin', 'pin_new','password');

        $id = Auth::user()->id;
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        if (!Auth::validate(array('email' => Auth::user()->email, 'password'=> $input['password'], 'pin' => $input['pin'], 'client_id' => $user->client_id)))
        {
            return Response::json(ResponseUtil::makeError('old pin is incorrect'), 403);
        }

        //$secure = sprintf("%04d", mt_rand(1, 9999));

        $update['pin'] = $input['pin_new'];

        $user = $this->userRepository->update($update, $id);
        return Response::json(ResponseUtil::makeError('pin updated successfully'), 201);

    }


    public function updateFcm(UpdateUserAPIRequest $request)
    {

        $id = Auth::user()->id;
        $fcm = $request->fcm;

        if(empty($id))
        {
            return Response::json(ResponseUtil::makeError('user not found'), 404);

        }    

        $updateFcm = DB::table('users')->where('id', $id)->update(['fcm' => $fcm]);
        return Response::json(ResponseUtil::makeError('fcm updated successfully'), 200);

       

   

    }

}
