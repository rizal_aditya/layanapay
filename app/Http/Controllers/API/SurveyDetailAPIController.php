<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSurveyDetailAPIRequest;
use App\Http\Requests\API\UpdateSurveyDetailAPIRequest;
use App\Models\SurveyDetail;
use App\Repositories\SurveyDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SurveyDetailController
 * @package App\Http\Controllers\API
 */

class SurveyDetailAPIController extends AppBaseController
{
    /** @var  SurveyDetailRepository */
    private $surveyDetailRepository;

    public function __construct(SurveyDetailRepository $surveyDetailRepo)
    {
        $this->surveyDetailRepository = $surveyDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/surveyDetail",
     *      summary="Get a listing of the SurveyDetail.",
     *      tags={"SurveyDetail"},
     *      description="Get all SurveyDetail",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SurveyDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->surveyDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->surveyDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $surveyDetail = $this->surveyDetailRepository->all();

        return $this->sendResponse($surveyDetail->toArray(), 'SurveyDetail retrieved successfully');
    }

    /**
     * @param CreateSurveyDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/surveyDetail",
     *      summary="Store a newly created SurveyDetail in storage",
     *      tags={"SurveyDetail"},
     *      description="Store SurveyDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SurveyDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SurveyDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SurveyDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSurveyDetailAPIRequest $request)
    {
        $input = $request->all();

        $surveyDetail = $this->surveyDetailRepository->create($input);

        return $this->sendResponse($surveyDetail->toArray(), 'SurveyDetail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/surveyDetail/{id}",
     *      summary="Display the specified SurveyDetail",
     *      tags={"SurveyDetail"},
     *      description="Get SurveyDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SurveyDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SurveyDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var SurveyDetail $surveyDetail */
        $surveyDetail = $this->surveyDetailRepository->find($id);

        if (empty($surveyDetail)) {
            return Response::json(ResponseUtil::makeError('SurveyDetail not found'), 404);
        }

        return $this->sendResponse($surveyDetail->toArray(), 'SurveyDetail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSurveyDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/surveyDetail/{id}",
     *      summary="Update the specified SurveyDetail in storage",
     *      tags={"SurveyDetail"},
     *      description="Update SurveyDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SurveyDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SurveyDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SurveyDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SurveyDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSurveyDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var SurveyDetail $surveyDetail */
        $surveyDetail = $this->surveyDetailRepository->find($id);

        if (empty($surveyDetail)) {
            return Response::json(ResponseUtil::makeError('SurveyDetail not found'), 404);
        }

        $surveyDetail = $this->surveyDetailRepository->update($input, $id);

        return $this->sendResponse($surveyDetail->toArray(), 'SurveyDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/surveyDetail/{id}",
     *      summary="Remove the specified SurveyDetail from storage",
     *      tags={"SurveyDetail"},
     *      description="Delete SurveyDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SurveyDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var SurveyDetail $surveyDetail */
        $surveyDetail = $this->surveyDetailRepository->find($id);

        if (empty($surveyDetail)) {
            return Response::json(ResponseUtil::makeError('SurveyDetail not found'), 404);
        }

        $surveyDetail->delete();

        return $this->sendResponse($id, 'SurveyDetail deleted successfully');
    }
}
