<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBankAPIRequest;
use App\Http\Requests\API\UpdateBankAPIRequest;
use App\Repositories\BankRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;

/**
 * Class BankController
 * @package App\Http\Controllers\API
 */

class BankAPIController extends AppBaseController
{
    /** @var  BankRepository */
    private $bankRepository;

    public function __construct(BankRepository $bankRepo)
    {
        $this->bankRepository = $bankRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/bank",
     *      summary="Get a listing of the Bank.",
     *      tags={"Bank"},
     *      description="Get all Bank",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Bank")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
       
        
        $this->bankRepository->pushCriteria(new RequestCriteria($request));
        $this->bankRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bank = $this->bankRepository->all();

        return $this->sendResponse($bank->toArray(), 'Bank retrieved successfully');
    }

    public function bankServer(Request $request)
    {
        $type ="admin";
        $bank = DB::table('bank')->select('id','name','account_name','account_number','type')->where('type',$type)->get();

        if (empty($bank)) {;
            return Response::json(ResponseUtil::makeError('Bank not found'), 404);
        }

        return $this->sendResponse($bank, 'bank retrieved successfully');

    }

    public function listBank(Request $request)
    {
        $type ="bank";
        $bank = DB::table('bank')->select('id','name','account_name','image','param','type')->where(['type'=>$type,'param'=>'0'])->get();

        if (empty($bank)) {;
            return Response::json(ResponseUtil::makeError('Bank not found'), 404);
        }

        return $this->sendResponse($bank, 'bank retrieved successfully');

    }


  
    /**
     * @param CreateBankAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/bank",
     *      summary="Store a newly created Bank in storage",
     *      tags={"Bank"},
     *      description="Store Bank",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bank that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bank")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bank"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBankAPIRequest $request)
    {
        // $user = Auth::user()->id;
        // $param = $request->id;
        // $input = $request->all();
        // $namabank = DB::table('bank')->where(['id'=>$param])->first()->name;

      
        // $input['name'] = $namabank;
        // $input['param'] = $param;
        // $input['user_id'] = $user;
        // $input['type'] = 'secondary';
        // $input['status'] = '1';
        // $bank = $this->bankRepository->create($input);

        // return $this->sendResponse($bank->toArray(), 'Bank saved successfully');

        $user = Auth::user()->id;
        $input = $request->all();

        $input['user_id'] = $user;
        $input['type'] = 'secondary';
        $input['status'] = '1';
        $bank = $this->bankRepository->create($input);

        return $this->sendResponse($bank->toArray(), 'Bank saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/bank/{id}",
     *      summary="Display the specified Bank",
     *      tags={"Bank"},
     *      description="Get Bank",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bank",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bank"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Bank $bank */
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            return Response::json(ResponseUtil::makeError('Bank not found'), 404);
        }

        return $this->sendResponse($bank->toArray(), 'Bank retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBankAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bank/{id}",
     *      summary="Update the specified Bank in storage",
     *      tags={"Bank"},
     *      description="Update Bank",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bank",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bank that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bank")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bank"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBankAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bank $bank */
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            return Response::json(ResponseUtil::makeError('Bank not found'), 404);
        }

        $bank = $this->bankRepository->update($input, $id);

        return $this->sendResponse($bank->toArray(), 'Bank updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bank/{id}",
     *      summary="Remove the specified Bank from storage",
     *      tags={"Bank"},
     *      description="Delete Bank",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bank",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Bank $bank */
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            return Response::json(ResponseUtil::makeError('Bank not found'), 404);
        }

        $bank->delete();

        return $this->sendResponse($id, 'Bank deleted successfully');
    }
}
