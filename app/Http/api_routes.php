<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/

/* --- User API --- */

Route::get('user/myProfile', 'UserAPIController@myProfile');
Route::get('user/cekSaldo', 'UserAPIController@cekSaldo');
Route::post('user/myMerchant', 'UserAPIController@myMerchant');
Route::put('user/updateProfile', 'UserAPIController@updateProfile');
Route::put('user/updatePassword', 'UserAPIController@updatePassword');
Route::put('user/updatePin', 'UserAPIController@updatePin');
Route::get('user/myBank', 'UserAPIController@myBank');
Route::get('user/myQrcode', 'UserAPIController@myQrcode');
Route::put('user/updateRekening', 'UserAPIController@updateRekening');
Route::put('user/updateFcm', 'UserAPIController@updateFcm');

Route::get('user/listmerchant', 'UserAPIController@listmerchant');
Route::get('user/listissuer', 'UserAPIController@listissuer');
Route::get('user/listcobranding', 'UserAPIController@listcobranding');
Route::get('transaction/send', 'TransactionAPIController@sendemail');


Route::post('bank/store', 'BankAPIController@store');
Route::get('bank/bankServer', 'BankAPIController@bankServer');
Route::get('bank/listBank', 'BankAPIController@listBank');
/* --- Device API --- */

Route::post('device/check', 'DeviceAPIController@check');

Route::post('update/check', 'UpdateAPIController@check');


Route::get('transaction/historyBank', 'TransactionAPIController@historyBank');
Route::get('history/myHistory', 'HistoryAPIController@myHistory');

Route::resource('bank', 'BankAPIController');


// generate

Route::post('generate/store', 'GenerateAPIController@store');

/* --- Client API --- */

Route::get('client', 'ClientAPIController@index');

Route::group(['middleware' => ['role:admin']], function () {

	Route::resource('user', 'UserAPIController');
	
	

	
	Route::resource('device', 'DeviceAPIController');
	
	
	
	Route::resource('menu', 'MenuAPIController');
	

});


//transaksi 
Route::post('transaction/generate', 'TransactionAPIController@generate');
Route::post('transaction/nongenerate', 'TransactionAPIController@nongenerate');
Route::post('transaction/merchant', 'TransactionAPIController@merchant');
//deposite
Route::post('transaction/deposite', 'TransactionAPIController@deposite');
Route::post('transaction/konfirmasi', 'TransactionAPIController@konfirmasi');

//withdraw
Route::post('transaction/withdraw', 'TransactionAPIController@withdraw');

Route::post('transaction/store', 'TransactionAPIController@store');
Route::get('transaction/history', 'TransactionAPIController@history');
Route::post('transaction/batal', 'TransactionAPIController@batal');


Route::post('bank/store', 'BankAPIController@store');
Route::resource('purchase', 'PurchaseAPIController');

Route::resource('packet', 'PacketAPIController');







Route::resource('client', 'ClientAPIController');















Route::resource('transaction', 'TransactionAPIController');



Route::resource('history', 'HistoryAPIController');
Route::get('bank/myBank', 'BankAPIController@myBank');




Route::resource('generate', 'GenerateAPIController');



Route::resource('setting', 'SettingAPIController');
Route::post('setting/checkNominal', 'SettingAPIController@checkNominal');




Route::resource('activity', 'ActivityAPIController');

Route::resource('post', 'PostAPIController');

Route::resource('promo', 'PostAPIController');








