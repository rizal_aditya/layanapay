<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
use DB;

/**
 * Class PostUserCriteria
 * @package namespace App\Criteria;
 */

class TransactionCriteria implements CriteriaInterface
{

     
   

     public function __construct()
     {
       
     }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        
            
        $model = $model->orderBy('id','desc');  
        return $model;
    }
}
